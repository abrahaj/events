package com.shqiperia.entities
public enum DayOfWeek {
    MONDAY('http://purl.org/goodrelations/v1#Monday'),
    TUESDAY('http://purl.org/goodrelations/v1#Tuesday'),
    WEDNESDAY('http://purl.org/goodrelations/v1#Wednesday'),
    THURSDAY('http://purl.org/goodrelations/v1#Thursday'),
    FRIDAY('http://purl.org/goodrelations/v1#Friday'),
    SATURDAY('http://purl.org/goodrelations/v1#Saturday'),
    PUBLICHOLIDAYS('http://purl.org/goodrelations/v1#Sunday http://purl.org/goodrelations/v1#PublicHolidays') 
    String id

    DayOfWeek(String id) {
        this.id = id
    }
}