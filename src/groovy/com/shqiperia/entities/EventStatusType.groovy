package com.shqiperia.entities
public enum EventStatusType {

	EVENTCANCELLED('EventCancelled'),
	EVENTPOSTPONED('EventPostponed'),
	EVENTRESCHEDULED('EventRescheduled'),
	EVENTSCHEDULED('EventScheduled')
	String name

	EventStatusType(String name) {
		this.name = name
	}
}
