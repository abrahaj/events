package com.shqiperia.entities
public enum ItemAvailability {


	DISCONTINUED('Discontinued'),
	INSTOCK('InStock'),
	INSTOREONLY('InStoreOnly'),
	LIMITEDAVAILABILITY('LimitedAvailability'),
	ONLINEONLY('OnlineOnly'),
	OUTOFSTOCK('OutOfStock'),
	PREORDER('PreOrder'),
	SOLDOUT('SoldOut'),
	String id

	ItemAvailability(String id) {
		this.id = id
	}
}
