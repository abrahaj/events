<%@ page import="com.shqiperia.entities.Intangible" %>



<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="intangible.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${intangibleInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="intangible.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${intangibleInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="intangible.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${intangibleInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="intangible.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${intangibleInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="intangible.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${intangibleInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="intangible.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${intangibleInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: intangibleInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="intangible.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${intangibleInstance?.url}"/>
</div>

