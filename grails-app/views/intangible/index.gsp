
<%@ page import="com.shqiperia.entities.Intangible" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'intangible.label', default: 'Intangible')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-intangible" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-intangible" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'intangible.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'intangible.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'intangible.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'intangible.image.label', default: 'Image')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'intangible.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="sameAs" title="${message(code: 'intangible.sameAs.label', default: 'Same As')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${intangibleInstanceList}" status="i" var="intangibleInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${intangibleInstance.id}">${fieldValue(bean: intangibleInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: intangibleInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: intangibleInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: intangibleInstance, field: "image")}</td>
					
						<td>${fieldValue(bean: intangibleInstance, field: "name")}</td>
					
						<td>${fieldValue(bean: intangibleInstance, field: "sameAs")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${intangibleInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
