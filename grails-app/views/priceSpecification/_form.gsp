<%@ page import="com.shqiperia.entities.PriceSpecification" %>



<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="priceSpecification.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${priceSpecificationInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="priceSpecification.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${priceSpecificationInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="priceSpecification.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${priceSpecificationInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'eligibleQuantity', 'error')} ">
	<label for="eligibleQuantity">
		<g:message code="priceSpecification.eligibleQuantity.label" default="Eligible Quantity" />
		
	</label>
	<g:select id="eligibleQuantity" name="eligibleQuantity.id" from="${com.shqiperia.entities.QuantitativeValue.list()}" optionKey="id" value="${priceSpecificationInstance?.eligibleQuantity?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'eligibleTransactionVolume', 'error')} required">
	<label for="eligibleTransactionVolume">
		<g:message code="priceSpecification.eligibleTransactionVolume.label" default="Eligible Transaction Volume" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="eligibleTransactionVolume" name="eligibleTransactionVolume.id" from="${com.shqiperia.entities.PriceSpecification.list()}" optionKey="id" required="" value="${priceSpecificationInstance?.eligibleTransactionVolume?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="priceSpecification.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${priceSpecificationInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'maxPrice', 'error')} ">
	<label for="maxPrice">
		<g:message code="priceSpecification.maxPrice.label" default="Max Price" />
		
	</label>
	<g:field name="maxPrice" value="${fieldValue(bean: priceSpecificationInstance, field: 'maxPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'minPrice', 'error')} ">
	<label for="minPrice">
		<g:message code="priceSpecification.minPrice.label" default="Min Price" />
		
	</label>
	<g:field name="minPrice" value="${fieldValue(bean: priceSpecificationInstance, field: 'minPrice')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="priceSpecification.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${priceSpecificationInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'price', 'error')} ">
	<label for="price">
		<g:message code="priceSpecification.price.label" default="Price" />
		
	</label>
	<g:field name="price" value="${fieldValue(bean: priceSpecificationInstance, field: 'price')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'priceCurrency', 'error')} ">
	<label for="priceCurrency">
		<g:message code="priceSpecification.priceCurrency.label" default="Price Currency" />
		
	</label>
	<g:textField name="priceCurrency" maxlength="250" value="${priceSpecificationInstance?.priceCurrency}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="priceSpecification.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${priceSpecificationInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="priceSpecification.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${priceSpecificationInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'validFrom', 'error')} ">
	<label for="validFrom">
		<g:message code="priceSpecification.validFrom.label" default="Valid From" />
		
	</label>
	<g:datePicker name="validFrom" precision="day"  value="${priceSpecificationInstance?.validFrom}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'validThrough', 'error')} ">
	<label for="validThrough">
		<g:message code="priceSpecification.validThrough.label" default="Valid Through" />
		
	</label>
	<g:datePicker name="validThrough" precision="day"  value="${priceSpecificationInstance?.validThrough}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: priceSpecificationInstance, field: 'valueAddedTaxIncluded', 'error')} ">
	<label for="valueAddedTaxIncluded">
		<g:message code="priceSpecification.valueAddedTaxIncluded.label" default="Value Added Tax Included" />
		
	</label>
	<g:checkBox name="valueAddedTaxIncluded" value="${priceSpecificationInstance?.valueAddedTaxIncluded}" />
</div>

