
<%@ page import="com.shqiperia.entities.PriceSpecification" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'priceSpecification.label', default: 'PriceSpecification')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-priceSpecification" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-priceSpecification" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'priceSpecification.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'priceSpecification.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'priceSpecification.description.label', default: 'Description')}" />
					
						<th><g:message code="priceSpecification.eligibleQuantity.label" default="Eligible Quantity" /></th>
					
						<th><g:message code="priceSpecification.eligibleTransactionVolume.label" default="Eligible Transaction Volume" /></th>
					
						<g:sortableColumn property="image" title="${message(code: 'priceSpecification.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${priceSpecificationInstanceList}" status="i" var="priceSpecificationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${priceSpecificationInstance.id}">${fieldValue(bean: priceSpecificationInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: priceSpecificationInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: priceSpecificationInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: priceSpecificationInstance, field: "eligibleQuantity")}</td>
					
						<td>${fieldValue(bean: priceSpecificationInstance, field: "eligibleTransactionVolume")}</td>
					
						<td>${fieldValue(bean: priceSpecificationInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${priceSpecificationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
