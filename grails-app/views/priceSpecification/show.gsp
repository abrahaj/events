
<%@ page import="com.shqiperia.entities.PriceSpecification" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'priceSpecification.label', default: 'PriceSpecification')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-priceSpecification" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-priceSpecification" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list priceSpecification">
			
				<g:if test="${priceSpecificationInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="priceSpecification.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${priceSpecificationInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="priceSpecification.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${priceSpecificationInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="priceSpecification.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${priceSpecificationInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.eligibleQuantity}">
				<li class="fieldcontain">
					<span id="eligibleQuantity-label" class="property-label"><g:message code="priceSpecification.eligibleQuantity.label" default="Eligible Quantity" /></span>
					
						<span class="property-value" aria-labelledby="eligibleQuantity-label"><g:link controller="quantitativeValue" action="show" id="${priceSpecificationInstance?.eligibleQuantity?.id}">${priceSpecificationInstance?.eligibleQuantity?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.eligibleTransactionVolume}">
				<li class="fieldcontain">
					<span id="eligibleTransactionVolume-label" class="property-label"><g:message code="priceSpecification.eligibleTransactionVolume.label" default="Eligible Transaction Volume" /></span>
					
						<span class="property-value" aria-labelledby="eligibleTransactionVolume-label"><g:link controller="priceSpecification" action="show" id="${priceSpecificationInstance?.eligibleTransactionVolume?.id}">${priceSpecificationInstance?.eligibleTransactionVolume?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="priceSpecification.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${priceSpecificationInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.maxPrice}">
				<li class="fieldcontain">
					<span id="maxPrice-label" class="property-label"><g:message code="priceSpecification.maxPrice.label" default="Max Price" /></span>
					
						<span class="property-value" aria-labelledby="maxPrice-label"><g:fieldValue bean="${priceSpecificationInstance}" field="maxPrice"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.minPrice}">
				<li class="fieldcontain">
					<span id="minPrice-label" class="property-label"><g:message code="priceSpecification.minPrice.label" default="Min Price" /></span>
					
						<span class="property-value" aria-labelledby="minPrice-label"><g:fieldValue bean="${priceSpecificationInstance}" field="minPrice"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="priceSpecification.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${priceSpecificationInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="priceSpecification.price.label" default="Price" /></span>
					
						<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${priceSpecificationInstance}" field="price"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.priceCurrency}">
				<li class="fieldcontain">
					<span id="priceCurrency-label" class="property-label"><g:message code="priceSpecification.priceCurrency.label" default="Price Currency" /></span>
					
						<span class="property-value" aria-labelledby="priceCurrency-label"><g:fieldValue bean="${priceSpecificationInstance}" field="priceCurrency"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="priceSpecification.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${priceSpecificationInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="priceSpecification.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${priceSpecificationInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.validFrom}">
				<li class="fieldcontain">
					<span id="validFrom-label" class="property-label"><g:message code="priceSpecification.validFrom.label" default="Valid From" /></span>
					
						<span class="property-value" aria-labelledby="validFrom-label"><g:formatDate date="${priceSpecificationInstance?.validFrom}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.validThrough}">
				<li class="fieldcontain">
					<span id="validThrough-label" class="property-label"><g:message code="priceSpecification.validThrough.label" default="Valid Through" /></span>
					
						<span class="property-value" aria-labelledby="validThrough-label"><g:formatDate date="${priceSpecificationInstance?.validThrough}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${priceSpecificationInstance?.valueAddedTaxIncluded}">
				<li class="fieldcontain">
					<span id="valueAddedTaxIncluded-label" class="property-label"><g:message code="priceSpecification.valueAddedTaxIncluded.label" default="Value Added Tax Included" /></span>
					
						<span class="property-value" aria-labelledby="valueAddedTaxIncluded-label"><g:formatBoolean boolean="${priceSpecificationInstance?.valueAddedTaxIncluded}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:priceSpecificationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${priceSpecificationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
