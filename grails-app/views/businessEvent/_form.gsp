<%@ page import="com.shqiperia.entities.BusinessEvent" %>



<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="businessEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${businessEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="businessEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${businessEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="businessEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${businessEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="businessEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${businessEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="businessEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${businessEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="businessEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${businessEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="businessEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${businessEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="businessEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${businessEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="businessEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${businessEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="businessEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${businessEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="businessEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${businessEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="businessEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${businessEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="businessEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${businessEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="businessEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${businessEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="businessEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${businessEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="businessEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="businessEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="businessEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${businessEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: businessEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="businessEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${businessEventInstance?.url}"/>
</div>

