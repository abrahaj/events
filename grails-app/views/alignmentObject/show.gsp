
<%@ page import="com.shqiperia.entities.AlignmentObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'alignmentObject.label', default: 'AlignmentObject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-alignmentObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-alignmentObject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list alignmentObject">
			
				<g:if test="${alignmentObjectInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="alignmentObject.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${alignmentObjectInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.alignmentType}">
				<li class="fieldcontain">
					<span id="alignmentType-label" class="property-label"><g:message code="alignmentObject.alignmentType.label" default="Alignment Type" /></span>
					
						<span class="property-value" aria-labelledby="alignmentType-label"><g:fieldValue bean="${alignmentObjectInstance}" field="alignmentType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="alignmentObject.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${alignmentObjectInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="alignmentObject.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${alignmentObjectInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.educationalFramework}">
				<li class="fieldcontain">
					<span id="educationalFramework-label" class="property-label"><g:message code="alignmentObject.educationalFramework.label" default="Educational Framework" /></span>
					
						<span class="property-value" aria-labelledby="educationalFramework-label"><g:fieldValue bean="${alignmentObjectInstance}" field="educationalFramework"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="alignmentObject.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${alignmentObjectInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="alignmentObject.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${alignmentObjectInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="alignmentObject.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${alignmentObjectInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.targetDescription}">
				<li class="fieldcontain">
					<span id="targetDescription-label" class="property-label"><g:message code="alignmentObject.targetDescription.label" default="Target Description" /></span>
					
						<span class="property-value" aria-labelledby="targetDescription-label"><g:fieldValue bean="${alignmentObjectInstance}" field="targetDescription"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.targetName}">
				<li class="fieldcontain">
					<span id="targetName-label" class="property-label"><g:message code="alignmentObject.targetName.label" default="Target Name" /></span>
					
						<span class="property-value" aria-labelledby="targetName-label"><g:fieldValue bean="${alignmentObjectInstance}" field="targetName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.targetUrl}">
				<li class="fieldcontain">
					<span id="targetUrl-label" class="property-label"><g:message code="alignmentObject.targetUrl.label" default="Target Url" /></span>
					
						<span class="property-value" aria-labelledby="targetUrl-label"><g:fieldValue bean="${alignmentObjectInstance}" field="targetUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${alignmentObjectInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="alignmentObject.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${alignmentObjectInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:alignmentObjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${alignmentObjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
