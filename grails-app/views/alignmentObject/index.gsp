
<%@ page import="com.shqiperia.entities.AlignmentObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'alignmentObject.label', default: 'AlignmentObject')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-alignmentObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-alignmentObject" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'alignmentObject.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alignmentType" title="${message(code: 'alignmentObject.alignmentType.label', default: 'Alignment Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'alignmentObject.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'alignmentObject.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="educationalFramework" title="${message(code: 'alignmentObject.educationalFramework.label', default: 'Educational Framework')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'alignmentObject.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${alignmentObjectInstanceList}" status="i" var="alignmentObjectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${alignmentObjectInstance.id}">${fieldValue(bean: alignmentObjectInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: alignmentObjectInstance, field: "alignmentType")}</td>
					
						<td>${fieldValue(bean: alignmentObjectInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: alignmentObjectInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: alignmentObjectInstance, field: "educationalFramework")}</td>
					
						<td>${fieldValue(bean: alignmentObjectInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${alignmentObjectInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
