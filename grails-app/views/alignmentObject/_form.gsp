<%@ page import="com.shqiperia.entities.AlignmentObject" %>



<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="alignmentObject.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${alignmentObjectInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'alignmentType', 'error')} ">
	<label for="alignmentType">
		<g:message code="alignmentObject.alignmentType.label" default="Alignment Type" />
		
	</label>
	<g:textField name="alignmentType" maxlength="250" value="${alignmentObjectInstance?.alignmentType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="alignmentObject.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${alignmentObjectInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="alignmentObject.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${alignmentObjectInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'educationalFramework', 'error')} ">
	<label for="educationalFramework">
		<g:message code="alignmentObject.educationalFramework.label" default="Educational Framework" />
		
	</label>
	<g:textField name="educationalFramework" maxlength="250" value="${alignmentObjectInstance?.educationalFramework}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="alignmentObject.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${alignmentObjectInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="alignmentObject.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${alignmentObjectInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="alignmentObject.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${alignmentObjectInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'targetDescription', 'error')} ">
	<label for="targetDescription">
		<g:message code="alignmentObject.targetDescription.label" default="Target Description" />
		
	</label>
	<g:textField name="targetDescription" maxlength="250" value="${alignmentObjectInstance?.targetDescription}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'targetName', 'error')} ">
	<label for="targetName">
		<g:message code="alignmentObject.targetName.label" default="Target Name" />
		
	</label>
	<g:textField name="targetName" maxlength="250" value="${alignmentObjectInstance?.targetName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'targetUrl', 'error')} ">
	<label for="targetUrl">
		<g:message code="alignmentObject.targetUrl.label" default="Target Url" />
		
	</label>
	<g:textField name="targetUrl" maxlength="250" value="${alignmentObjectInstance?.targetUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: alignmentObjectInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="alignmentObject.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${alignmentObjectInstance?.url}"/>
</div>

