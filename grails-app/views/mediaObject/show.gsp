
<%@ page import="com.shqiperia.entities.MediaObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'mediaObject.label', default: 'MediaObject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-mediaObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-mediaObject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list mediaObject">
			
				<g:if test="${mediaObjectInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="mediaObject.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${mediaObjectInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="mediaObject.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${mediaObjectInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="mediaObject.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${mediaObjectInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="mediaObject.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${mediaObjectInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="mediaObject.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${mediaObjectInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="mediaObject.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${mediaObjectInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="mediaObject.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${mediaObjectInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="mediaObject.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${mediaObjectInstance?.aggregateRating?.id}">${mediaObjectInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="mediaObject.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${mediaObjectInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="mediaObject.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${mediaObjectInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="mediaObject.author.label" default="Author" /></span>
					
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.author?.id}">${mediaObjectInstance?.author?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="mediaObject.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${mediaObjectInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.bitrate}">
				<li class="fieldcontain">
					<span id="bitrate-label" class="property-label"><g:message code="mediaObject.bitrate.label" default="Bitrate" /></span>
					
						<span class="property-value" aria-labelledby="bitrate-label"><g:fieldValue bean="${mediaObjectInstance}" field="bitrate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="mediaObject.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${mediaObjectInstance?.citation?.id}">${mediaObjectInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="mediaObject.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${mediaObjectInstance?.contentLocation?.id}">${mediaObjectInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="mediaObject.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${mediaObjectInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.contentSize}">
				<li class="fieldcontain">
					<span id="contentSize-label" class="property-label"><g:message code="mediaObject.contentSize.label" default="Content Size" /></span>
					
						<span class="property-value" aria-labelledby="contentSize-label"><g:fieldValue bean="${mediaObjectInstance}" field="contentSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.contentUrl}">
				<li class="fieldcontain">
					<span id="contentUrl-label" class="property-label"><g:message code="mediaObject.contentUrl.label" default="Content Url" /></span>
					
						<span class="property-value" aria-labelledby="contentUrl-label"><g:fieldValue bean="${mediaObjectInstance}" field="contentUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="mediaObject.contributor.label" default="Contributor" /></span>
					
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.contributor?.id}">${mediaObjectInstance?.contributor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="mediaObject.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.copyrightHolder?.id}">${mediaObjectInstance?.copyrightHolder?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="mediaObject.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${mediaObjectInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="mediaObject.creator.label" default="Creator" /></span>
					
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.creator?.id}">${mediaObjectInstance?.creator?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="mediaObject.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${mediaObjectInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="mediaObject.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${mediaObjectInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="mediaObject.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${mediaObjectInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="mediaObject.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${mediaObjectInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="mediaObject.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${mediaObjectInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="mediaObject.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:fieldValue bean="${mediaObjectInstance}" field="duration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="mediaObject.editor.label" default="Editor" /></span>
					
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.editor?.id}">${mediaObjectInstance?.editor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="mediaObject.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${mediaObjectInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.embedUrl}">
				<li class="fieldcontain">
					<span id="embedUrl-label" class="property-label"><g:message code="mediaObject.embedUrl.label" default="Embed Url" /></span>
					
						<span class="property-value" aria-labelledby="embedUrl-label"><g:fieldValue bean="${mediaObjectInstance}" field="embedUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.encodesCreativeWork}">
				<li class="fieldcontain">
					<span id="encodesCreativeWork-label" class="property-label"><g:message code="mediaObject.encodesCreativeWork.label" default="Encodes Creative Work" /></span>
					
						<span class="property-value" aria-labelledby="encodesCreativeWork-label"><g:link controller="creativeWork" action="show" id="${mediaObjectInstance?.encodesCreativeWork?.id}">${mediaObjectInstance?.encodesCreativeWork?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.encodingFormat}">
				<li class="fieldcontain">
					<span id="encodingFormat-label" class="property-label"><g:message code="mediaObject.encodingFormat.label" default="Encoding Format" /></span>
					
						<span class="property-value" aria-labelledby="encodingFormat-label"><g:fieldValue bean="${mediaObjectInstance}" field="encodingFormat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.expires}">
				<li class="fieldcontain">
					<span id="expires-label" class="property-label"><g:message code="mediaObject.expires.label" default="Expires" /></span>
					
						<span class="property-value" aria-labelledby="expires-label"><g:formatDate date="${mediaObjectInstance?.expires}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="mediaObject.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${mediaObjectInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="mediaObject.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${mediaObjectInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="mediaObject.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${mediaObjectInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="mediaObject.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${mediaObjectInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="mediaObject.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${mediaObjectInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="mediaObject.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${mediaObjectInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="mediaObject.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${mediaObjectInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="mediaObject.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${mediaObjectInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="mediaObject.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${mediaObjectInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="mediaObject.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${mediaObjectInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="mediaObject.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${mediaObjectInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="mediaObject.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${mediaObjectInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="mediaObject.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${mediaObjectInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="mediaObject.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${mediaObjectInstance?.offers?.id}">${mediaObjectInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.playerType}">
				<li class="fieldcontain">
					<span id="playerType-label" class="property-label"><g:message code="mediaObject.playerType.label" default="Player Type" /></span>
					
						<span class="property-value" aria-labelledby="playerType-label"><g:fieldValue bean="${mediaObjectInstance}" field="playerType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.productionCompany}">
				<li class="fieldcontain">
					<span id="productionCompany-label" class="property-label"><g:message code="mediaObject.productionCompany.label" default="Production Company" /></span>
					
						<span class="property-value" aria-labelledby="productionCompany-label"><g:link controller="organization" action="show" id="${mediaObjectInstance?.productionCompany?.id}">${mediaObjectInstance?.productionCompany?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="mediaObject.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${mediaObjectInstance?.provider?.id}">${mediaObjectInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.publication}">
				<li class="fieldcontain">
					<span id="publication-label" class="property-label"><g:message code="mediaObject.publication.label" default="Publication" /></span>
					
						<span class="property-value" aria-labelledby="publication-label"><g:link controller="publicationEvent" action="show" id="${mediaObjectInstance?.publication?.id}">${mediaObjectInstance?.publication?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="mediaObject.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${mediaObjectInstance?.publisher?.id}">${mediaObjectInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="mediaObject.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${mediaObjectInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.regionsAllowed}">
				<li class="fieldcontain">
					<span id="regionsAllowed-label" class="property-label"><g:message code="mediaObject.regionsAllowed.label" default="Regions Allowed" /></span>
					
						<span class="property-value" aria-labelledby="regionsAllowed-label"><g:link controller="place" action="show" id="${mediaObjectInstance?.regionsAllowed?.id}">${mediaObjectInstance?.regionsAllowed?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.requiresSubscription}">
				<li class="fieldcontain">
					<span id="requiresSubscription-label" class="property-label"><g:message code="mediaObject.requiresSubscription.label" default="Requires Subscription" /></span>
					
						<span class="property-value" aria-labelledby="requiresSubscription-label"><g:formatBoolean boolean="${mediaObjectInstance?.requiresSubscription}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="mediaObject.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${mediaObjectInstance?.review?.id}">${mediaObjectInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="mediaObject.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${mediaObjectInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="mediaObject.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${mediaObjectInstance?.sourceOrganization?.id}">${mediaObjectInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="mediaObject.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${mediaObjectInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="mediaObject.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${mediaObjectInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="mediaObject.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${mediaObjectInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.uploadDate}">
				<li class="fieldcontain">
					<span id="uploadDate-label" class="property-label"><g:message code="mediaObject.uploadDate.label" default="Upload Date" /></span>
					
						<span class="property-value" aria-labelledby="uploadDate-label"><g:formatDate date="${mediaObjectInstance?.uploadDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="mediaObject.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${mediaObjectInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${mediaObjectInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="mediaObject.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${mediaObjectInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:mediaObjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${mediaObjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
