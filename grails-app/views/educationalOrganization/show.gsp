
<%@ page import="com.shqiperia.entities.EducationalOrganization" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'educationalOrganization.label', default: 'EducationalOrganization')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-educationalOrganization" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-educationalOrganization" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list educationalOrganization">
			
				<g:if test="${educationalOrganizationInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="educationalOrganization.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.legalName}">
				<li class="fieldcontain">
					<span id="legalName-label" class="property-label"><g:message code="educationalOrganization.legalName.label" default="Legal Name" /></span>
					
						<span class="property-value" aria-labelledby="legalName-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="legalName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="educationalOrganization.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="educationalOrganization.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="educationalOrganization.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="postaladdress" action="show" id="${educationalOrganizationInstance?.address?.id}">${educationalOrganizationInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.contactPoint}">
				<li class="fieldcontain">
					<span id="contactPoint-label" class="property-label"><g:message code="educationalOrganization.contactPoint.label" default="Contact Point" /></span>
					
						<span class="property-value" aria-labelledby="contactPoint-label"><g:link controller="contactpoint" action="show" id="${educationalOrganizationInstance?.contactPoint?.id}">${educationalOrganizationInstance?.contactPoint?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="educationalOrganization.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.duns}">
				<li class="fieldcontain">
					<span id="duns-label" class="property-label"><g:message code="educationalOrganization.duns.label" default="Duns" /></span>
					
						<span class="property-value" aria-labelledby="duns-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="duns"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="educationalOrganization.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.foundingDate}">
				<li class="fieldcontain">
					<span id="foundingDate-label" class="property-label"><g:message code="educationalOrganization.foundingDate.label" default="Founding Date" /></span>
					
						<span class="property-value" aria-labelledby="foundingDate-label"><g:formatDate date="${educationalOrganizationInstance?.foundingDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.globalLocationNumber}">
				<li class="fieldcontain">
					<span id="globalLocationNumber-label" class="property-label"><g:message code="educationalOrganization.globalLocationNumber.label" default="Global Location Number" /></span>
					
						<span class="property-value" aria-labelledby="globalLocationNumber-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="globalLocationNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="educationalOrganization.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="educationalOrganization.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.isicV4}">
				<li class="fieldcontain">
					<span id="isicV4-label" class="property-label"><g:message code="educationalOrganization.isicV4.label" default="Isic V4" /></span>
					
						<span class="property-value" aria-labelledby="isicV4-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="isicV4"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.logo}">
				<li class="fieldcontain">
					<span id="logo-label" class="property-label"><g:message code="educationalOrganization.logo.label" default="Logo" /></span>
					
						<span class="property-value" aria-labelledby="logo-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="logo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="educationalOrganization.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.taxID}">
				<li class="fieldcontain">
					<span id="taxID-label" class="property-label"><g:message code="educationalOrganization.taxID.label" default="Tax ID" /></span>
					
						<span class="property-value" aria-labelledby="taxID-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="taxID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="educationalOrganization.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="educationalOrganization.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${educationalOrganizationInstance?.vatID}">
				<li class="fieldcontain">
					<span id="vatID-label" class="property-label"><g:message code="educationalOrganization.vatID.label" default="Vat ID" /></span>
					
						<span class="property-value" aria-labelledby="vatID-label"><g:fieldValue bean="${educationalOrganizationInstance}" field="vatID"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:educationalOrganizationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${educationalOrganizationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
