
<%@ page import="com.shqiperia.entities.EducationalOrganization" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'educationalOrganization.label', default: 'EducationalOrganization')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-educationalOrganization" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-educationalOrganization" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'educationalOrganization.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="legalName" title="${message(code: 'educationalOrganization.legalName.label', default: 'Legal Name')}" />
					
						<g:sortableColumn property="email" title="${message(code: 'educationalOrganization.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'educationalOrganization.additionalType.label', default: 'Additional Type')}" />
					
						<th><g:message code="educationalOrganization.address.label" default="Address" /></th>
					
						<th><g:message code="educationalOrganization.contactPoint.label" default="Contact Point" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${educationalOrganizationInstanceList}" status="i" var="educationalOrganizationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${educationalOrganizationInstance.id}">${fieldValue(bean: educationalOrganizationInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: educationalOrganizationInstance, field: "legalName")}</td>
					
						<td>${fieldValue(bean: educationalOrganizationInstance, field: "email")}</td>
					
						<td>${fieldValue(bean: educationalOrganizationInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: educationalOrganizationInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: educationalOrganizationInstance, field: "contactPoint")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${educationalOrganizationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
