<%@ page import="com.shqiperia.entities.EducationalOrganization" %>



<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="educationalOrganization.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${educationalOrganizationInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'legalName', 'error')} ">
	<label for="legalName">
		<g:message code="educationalOrganization.legalName.label" default="Legal Name" />
		
	</label>
	<g:textField name="legalName" maxlength="250" value="${educationalOrganizationInstance?.legalName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="educationalOrganization.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" maxlength="250" value="${educationalOrganizationInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="educationalOrganization.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${educationalOrganizationInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="educationalOrganization.address.label" default="Address" />
		
	</label>
	<g:select id="address" name="address.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${educationalOrganizationInstance?.address?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'contactPoint', 'error')} ">
	<label for="contactPoint">
		<g:message code="educationalOrganization.contactPoint.label" default="Contact Point" />
		
	</label>
	<g:select id="contactPoint" name="contactPoint.id" from="${com.shqiperia.entities.Contactpoint.list()}" optionKey="id" value="${educationalOrganizationInstance?.contactPoint?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="educationalOrganization.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${educationalOrganizationInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'duns', 'error')} ">
	<label for="duns">
		<g:message code="educationalOrganization.duns.label" default="Duns" />
		
	</label>
	<g:textField name="duns" maxlength="250" value="${educationalOrganizationInstance?.duns}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="educationalOrganization.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${educationalOrganizationInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'foundingDate', 'error')} ">
	<label for="foundingDate">
		<g:message code="educationalOrganization.foundingDate.label" default="Founding Date" />
		
	</label>
	<g:datePicker name="foundingDate" precision="day"  value="${educationalOrganizationInstance?.foundingDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'globalLocationNumber', 'error')} ">
	<label for="globalLocationNumber">
		<g:message code="educationalOrganization.globalLocationNumber.label" default="Global Location Number" />
		
	</label>
	<g:textField name="globalLocationNumber" maxlength="250" value="${educationalOrganizationInstance?.globalLocationNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="educationalOrganization.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${educationalOrganizationInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="educationalOrganization.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${educationalOrganizationInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'isicV4', 'error')} ">
	<label for="isicV4">
		<g:message code="educationalOrganization.isicV4.label" default="Isic V4" />
		
	</label>
	<g:textField name="isicV4" maxlength="250" value="${educationalOrganizationInstance?.isicV4}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'logo', 'error')} ">
	<label for="logo">
		<g:message code="educationalOrganization.logo.label" default="Logo" />
		
	</label>
	<g:textField name="logo" maxlength="250" value="${educationalOrganizationInstance?.logo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="educationalOrganization.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${educationalOrganizationInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'taxID', 'error')} ">
	<label for="taxID">
		<g:message code="educationalOrganization.taxID.label" default="Tax ID" />
		
	</label>
	<g:textField name="taxID" maxlength="250" value="${educationalOrganizationInstance?.taxID}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="educationalOrganization.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${educationalOrganizationInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="educationalOrganization.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${educationalOrganizationInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationalOrganizationInstance, field: 'vatID', 'error')} ">
	<label for="vatID">
		<g:message code="educationalOrganization.vatID.label" default="Vat ID" />
		
	</label>
	<g:textField name="vatID" maxlength="250" value="${educationalOrganizationInstance?.vatID}"/>
</div>

