
<%@ page import="com.shqiperia.entities.WarrantyPromise" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'warrantyPromise.label', default: 'WarrantyPromise')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-warrantyPromise" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-warrantyPromise" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list warrantyPromise">
			
				<g:if test="${warrantyPromiseInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="warrantyPromise.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="warrantyPromise.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="warrantyPromise.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.durationOfWarranty}">
				<li class="fieldcontain">
					<span id="durationOfWarranty-label" class="property-label"><g:message code="warrantyPromise.durationOfWarranty.label" default="Duration Of Warranty" /></span>
					
						<span class="property-value" aria-labelledby="durationOfWarranty-label"><g:link controller="quantitativeValue" action="show" id="${warrantyPromiseInstance?.durationOfWarranty?.id}">${warrantyPromiseInstance?.durationOfWarranty?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="warrantyPromise.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="warrantyPromise.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="warrantyPromise.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="warrantyPromise.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${warrantyPromiseInstance?.warrantyScope}">
				<li class="fieldcontain">
					<span id="warrantyScope-label" class="property-label"><g:message code="warrantyPromise.warrantyScope.label" default="Warranty Scope" /></span>
					
						<span class="property-value" aria-labelledby="warrantyScope-label"><g:fieldValue bean="${warrantyPromiseInstance}" field="warrantyScope"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:warrantyPromiseInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${warrantyPromiseInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
