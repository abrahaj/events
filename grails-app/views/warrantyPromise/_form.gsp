<%@ page import="com.shqiperia.entities.WarrantyPromise" %>



<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="warrantyPromise.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${warrantyPromiseInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="warrantyPromise.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${warrantyPromiseInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="warrantyPromise.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${warrantyPromiseInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'durationOfWarranty', 'error')} ">
	<label for="durationOfWarranty">
		<g:message code="warrantyPromise.durationOfWarranty.label" default="Duration Of Warranty" />
		
	</label>
	<g:select id="durationOfWarranty" name="durationOfWarranty.id" from="${com.shqiperia.entities.QuantitativeValue.list()}" optionKey="id" value="${warrantyPromiseInstance?.durationOfWarranty?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="warrantyPromise.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${warrantyPromiseInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="warrantyPromise.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${warrantyPromiseInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="warrantyPromise.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${warrantyPromiseInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="warrantyPromise.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${warrantyPromiseInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: warrantyPromiseInstance, field: 'warrantyScope', 'error')} ">
	<label for="warrantyScope">
		<g:message code="warrantyPromise.warrantyScope.label" default="Warranty Scope" />
		
	</label>
	<g:select name="warrantyScope" from="${com.shqiperia.entities.WarrantyPromise$WarrantyScope?.values()}" keys="${com.shqiperia.entities.WarrantyPromise$WarrantyScope.values()*.name()}" value="${warrantyPromiseInstance?.warrantyScope?.name()}" noSelection="['': '']"/>
</div>

