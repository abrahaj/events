
<%@ page import="com.shqiperia.entities.WarrantyPromise" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'warrantyPromise.label', default: 'WarrantyPromise')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-warrantyPromise" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-warrantyPromise" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'warrantyPromise.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'warrantyPromise.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'warrantyPromise.description.label', default: 'Description')}" />
					
						<th><g:message code="warrantyPromise.durationOfWarranty.label" default="Duration Of Warranty" /></th>
					
						<g:sortableColumn property="image" title="${message(code: 'warrantyPromise.image.label', default: 'Image')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'warrantyPromise.name.label', default: 'Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${warrantyPromiseInstanceList}" status="i" var="warrantyPromiseInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${warrantyPromiseInstance.id}">${fieldValue(bean: warrantyPromiseInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: warrantyPromiseInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: warrantyPromiseInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: warrantyPromiseInstance, field: "durationOfWarranty")}</td>
					
						<td>${fieldValue(bean: warrantyPromiseInstance, field: "image")}</td>
					
						<td>${fieldValue(bean: warrantyPromiseInstance, field: "name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${warrantyPromiseInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
