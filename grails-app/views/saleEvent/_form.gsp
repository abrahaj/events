<%@ page import="com.shqiperia.entities.SaleEvent" %>



<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="saleEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${saleEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="saleEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${saleEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="saleEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${saleEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="saleEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${saleEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="saleEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${saleEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="saleEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${saleEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="saleEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${saleEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="saleEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${saleEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="saleEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${saleEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="saleEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${saleEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="saleEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${saleEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="saleEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${saleEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="saleEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${saleEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="saleEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${saleEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="saleEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${saleEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="saleEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="saleEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="saleEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${saleEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: saleEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="saleEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${saleEventInstance?.url}"/>
</div>

