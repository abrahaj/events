
<%@ page import="com.shqiperia.entities.SaleEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'saleEvent.label', default: 'SaleEvent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-saleEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-saleEvent" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'saleEvent.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'saleEvent.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'saleEvent.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'saleEvent.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="doorTime" title="${message(code: 'saleEvent.doorTime.label', default: 'Door Time')}" />
					
						<g:sortableColumn property="duration" title="${message(code: 'saleEvent.duration.label', default: 'Duration')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${saleEventInstanceList}" status="i" var="saleEventInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${saleEventInstance.id}">${fieldValue(bean: saleEventInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: saleEventInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: saleEventInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: saleEventInstance, field: "description")}</td>
					
						<td><g:formatDate date="${saleEventInstance.doorTime}" /></td>
					
						<td><g:formatDate date="${saleEventInstance.duration}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${saleEventInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
