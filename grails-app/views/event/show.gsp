
<%@ page import="com.shqiperia.entities.Event" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-event" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-event" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list event">
			
				<g:if test="${eventInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="event.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${eventInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="event.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${eventInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="event.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${eventInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.attendee}">
				<li class="fieldcontain">
					<span id="attendee-label" class="property-label"><g:message code="event.attendee.label" default="Attendee" /></span>
					
						<g:each in="${eventInstance.attendee}" var="a">
						<span class="property-value" aria-labelledby="attendee-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="event.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${eventInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.doorTime}">
				<li class="fieldcontain">
					<span id="doorTime-label" class="property-label"><g:message code="event.doorTime.label" default="Door Time" /></span>
					
						<span class="property-value" aria-labelledby="doorTime-label"><g:formatDate date="${eventInstance?.doorTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="event.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:formatDate date="${eventInstance?.duration}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="event.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${eventInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.eventStatus}">
				<li class="fieldcontain">
					<span id="eventStatus-label" class="property-label"><g:message code="event.eventStatus.label" default="Event Status" /></span>
					
						<span class="property-value" aria-labelledby="eventStatus-label"><g:fieldValue bean="${eventInstance}" field="eventStatus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="event.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${eventInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="event.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:link controller="postaladdress" action="show" id="${eventInstance?.location?.id}">${eventInstance?.location?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.performer}">
				<li class="fieldcontain">
					<span id="performer-label" class="property-label"><g:message code="event.performer.label" default="Performer" /></span>
					
						<g:each in="${eventInstance.performer}" var="p">
						<span class="property-value" aria-labelledby="performer-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.previousStartDate}">
				<li class="fieldcontain">
					<span id="previousStartDate-label" class="property-label"><g:message code="event.previousStartDate.label" default="Previous Start Date" /></span>
					
						<span class="property-value" aria-labelledby="previousStartDate-label"><g:formatDate date="${eventInstance?.previousStartDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="event.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${eventInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="event.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${eventInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.subEvent}">
				<li class="fieldcontain">
					<span id="subEvent-label" class="property-label"><g:message code="event.subEvent.label" default="Sub Event" /></span>
					
						<g:each in="${eventInstance.subEvent}" var="s">
						<span class="property-value" aria-labelledby="subEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.superEvent}">
				<li class="fieldcontain">
					<span id="superEvent-label" class="property-label"><g:message code="event.superEvent.label" default="Super Event" /></span>
					
						<g:each in="${eventInstance.superEvent}" var="s">
						<span class="property-value" aria-labelledby="superEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="event.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${eventInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${eventInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="event.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${eventInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:eventInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${eventInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
