<%@ page import="com.shqiperia.entities.Event" %>



<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="event.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${eventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="event.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${eventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="event.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${eventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="event.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${eventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="event.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${eventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="event.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${eventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="event.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${eventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="event.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${eventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="event.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${eventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="event.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${eventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="event.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${eventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="event.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${eventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="event.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${eventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="event.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${eventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="event.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${eventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="event.subEvent.label" default="Sub Event" />
		
	</label>
	<g:select name="subEvent" from="${com.shqiperia.entities.Event.list()}" multiple="multiple" optionKey="id" size="5" value="${eventInstance?.subEvent*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="event.superEvent.label" default="Super Event" />
		
	</label>
	<g:select name="superEvent" from="${com.shqiperia.entities.Event.list()}" multiple="multiple" optionKey="id" size="5" value="${eventInstance?.superEvent*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="event.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${eventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: eventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="event.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${eventInstance?.url}"/>
</div>

