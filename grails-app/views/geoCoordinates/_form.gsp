<%@ page import="com.shqiperia.entities.GeoCoordinates" %>



<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="geoCoordinates.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${geoCoordinatesInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="geoCoordinates.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${geoCoordinatesInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="geoCoordinates.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${geoCoordinatesInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'elevation', 'error')} ">
	<label for="elevation">
		<g:message code="geoCoordinates.elevation.label" default="Elevation" />
		
	</label>
	<g:textField name="elevation" maxlength="250" value="${geoCoordinatesInstance?.elevation}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="geoCoordinates.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${geoCoordinatesInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'latitude', 'error')} ">
	<label for="latitude">
		<g:message code="geoCoordinates.latitude.label" default="Latitude" />
		
	</label>
	<g:textField name="latitude" maxlength="250" value="${geoCoordinatesInstance?.latitude}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'longitude', 'error')} ">
	<label for="longitude">
		<g:message code="geoCoordinates.longitude.label" default="Longitude" />
		
	</label>
	<g:textField name="longitude" maxlength="250" value="${geoCoordinatesInstance?.longitude}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="geoCoordinates.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${geoCoordinatesInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="geoCoordinates.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${geoCoordinatesInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: geoCoordinatesInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="geoCoordinates.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${geoCoordinatesInstance?.url}"/>
</div>

