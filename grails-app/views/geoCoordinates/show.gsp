
<%@ page import="com.shqiperia.entities.GeoCoordinates" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'geoCoordinates.label', default: 'GeoCoordinates')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-geoCoordinates" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-geoCoordinates" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list geoCoordinates">
			
				<g:if test="${geoCoordinatesInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="geoCoordinates.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="geoCoordinates.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="geoCoordinates.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.elevation}">
				<li class="fieldcontain">
					<span id="elevation-label" class="property-label"><g:message code="geoCoordinates.elevation.label" default="Elevation" /></span>
					
						<span class="property-value" aria-labelledby="elevation-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="elevation"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="geoCoordinates.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.latitude}">
				<li class="fieldcontain">
					<span id="latitude-label" class="property-label"><g:message code="geoCoordinates.latitude.label" default="Latitude" /></span>
					
						<span class="property-value" aria-labelledby="latitude-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="latitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.longitude}">
				<li class="fieldcontain">
					<span id="longitude-label" class="property-label"><g:message code="geoCoordinates.longitude.label" default="Longitude" /></span>
					
						<span class="property-value" aria-labelledby="longitude-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="longitude"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="geoCoordinates.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="geoCoordinates.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${geoCoordinatesInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="geoCoordinates.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${geoCoordinatesInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:geoCoordinatesInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${geoCoordinatesInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
