
<%@ page import="com.shqiperia.entities.QuantitativeValue" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quantitativeValue.label', default: 'QuantitativeValue')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-quantitativeValue" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-quantitativeValue" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'quantitativeValue.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'quantitativeValue.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'quantitativeValue.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'quantitativeValue.image.label', default: 'Image')}" />
					
						<g:sortableColumn property="maxValue" title="${message(code: 'quantitativeValue.maxValue.label', default: 'Max Value')}" />
					
						<g:sortableColumn property="minValue" title="${message(code: 'quantitativeValue.minValue.label', default: 'Min Value')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${quantitativeValueInstanceList}" status="i" var="quantitativeValueInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${quantitativeValueInstance.id}">${fieldValue(bean: quantitativeValueInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: quantitativeValueInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: quantitativeValueInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: quantitativeValueInstance, field: "image")}</td>
					
						<td>${fieldValue(bean: quantitativeValueInstance, field: "maxValue")}</td>
					
						<td>${fieldValue(bean: quantitativeValueInstance, field: "minValue")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${quantitativeValueInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
