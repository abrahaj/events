
<%@ page import="com.shqiperia.entities.QuantitativeValue" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quantitativeValue.label', default: 'QuantitativeValue')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-quantitativeValue" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-quantitativeValue" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list quantitativeValue">
			
				<g:if test="${quantitativeValueInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="quantitativeValue.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${quantitativeValueInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="quantitativeValue.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${quantitativeValueInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="quantitativeValue.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${quantitativeValueInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="quantitativeValue.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${quantitativeValueInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.maxValue}">
				<li class="fieldcontain">
					<span id="maxValue-label" class="property-label"><g:message code="quantitativeValue.maxValue.label" default="Max Value" /></span>
					
						<span class="property-value" aria-labelledby="maxValue-label"><g:fieldValue bean="${quantitativeValueInstance}" field="maxValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.minValue}">
				<li class="fieldcontain">
					<span id="minValue-label" class="property-label"><g:message code="quantitativeValue.minValue.label" default="Min Value" /></span>
					
						<span class="property-value" aria-labelledby="minValue-label"><g:fieldValue bean="${quantitativeValueInstance}" field="minValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="quantitativeValue.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${quantitativeValueInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="quantitativeValue.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${quantitativeValueInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.unitCode}">
				<li class="fieldcontain">
					<span id="unitCode-label" class="property-label"><g:message code="quantitativeValue.unitCode.label" default="Unit Code" /></span>
					
						<span class="property-value" aria-labelledby="unitCode-label"><g:fieldValue bean="${quantitativeValueInstance}" field="unitCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="quantitativeValue.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${quantitativeValueInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.value}">
				<li class="fieldcontain">
					<span id="value-label" class="property-label"><g:message code="quantitativeValue.value.label" default="Value" /></span>
					
						<span class="property-value" aria-labelledby="value-label"><g:fieldValue bean="${quantitativeValueInstance}" field="value"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantitativeValueInstance?.valueReference}">
				<li class="fieldcontain">
					<span id="valueReference-label" class="property-label"><g:message code="quantitativeValue.valueReference.label" default="Value Reference" /></span>
					
						<span class="property-value" aria-labelledby="valueReference-label"><g:link controller="structuredValue" action="show" id="${quantitativeValueInstance?.valueReference?.id}">${quantitativeValueInstance?.valueReference?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:quantitativeValueInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${quantitativeValueInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
