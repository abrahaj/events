<%@ page import="com.shqiperia.entities.QuantitativeValue" %>



<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="quantitativeValue.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${quantitativeValueInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="quantitativeValue.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${quantitativeValueInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="quantitativeValue.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${quantitativeValueInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="quantitativeValue.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${quantitativeValueInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'maxValue', 'error')} ">
	<label for="maxValue">
		<g:message code="quantitativeValue.maxValue.label" default="Max Value" />
		
	</label>
	<g:field name="maxValue" value="${fieldValue(bean: quantitativeValueInstance, field: 'maxValue')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'minValue', 'error')} ">
	<label for="minValue">
		<g:message code="quantitativeValue.minValue.label" default="Min Value" />
		
	</label>
	<g:field name="minValue" value="${fieldValue(bean: quantitativeValueInstance, field: 'minValue')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="quantitativeValue.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${quantitativeValueInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="quantitativeValue.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${quantitativeValueInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'unitCode', 'error')} ">
	<label for="unitCode">
		<g:message code="quantitativeValue.unitCode.label" default="Unit Code" />
		
	</label>
	<g:textField name="unitCode" maxlength="250" value="${quantitativeValueInstance?.unitCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="quantitativeValue.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${quantitativeValueInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'value', 'error')} ">
	<label for="value">
		<g:message code="quantitativeValue.value.label" default="Value" />
		
	</label>
	<g:field name="value" value="${fieldValue(bean: quantitativeValueInstance, field: 'value')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: quantitativeValueInstance, field: 'valueReference', 'error')} required">
	<label for="valueReference">
		<g:message code="quantitativeValue.valueReference.label" default="Value Reference" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="valueReference" name="valueReference.id" from="${com.shqiperia.entities.StructuredValue.list()}" optionKey="id" required="" value="${quantitativeValueInstance?.valueReference?.id}" class="many-to-one"/>
</div>

