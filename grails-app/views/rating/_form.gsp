<%@ page import="com.shqiperia.entities.Rating" %>



<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="rating.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${ratingInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="rating.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${ratingInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'bestRating', 'error')} ">
	<label for="bestRating">
		<g:message code="rating.bestRating.label" default="Best Rating" />
		
	</label>
	<g:field name="bestRating" value="${fieldValue(bean: ratingInstance, field: 'bestRating')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="rating.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${ratingInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="rating.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${ratingInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="rating.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${ratingInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'ratingValue', 'error')} ">
	<label for="ratingValue">
		<g:message code="rating.ratingValue.label" default="Rating Value" />
		
	</label>
	<g:textField name="ratingValue" maxlength="250" value="${ratingInstance?.ratingValue}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="rating.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${ratingInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="rating.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${ratingInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: ratingInstance, field: 'worstRating', 'error')} ">
	<label for="worstRating">
		<g:message code="rating.worstRating.label" default="Worst Rating" />
		
	</label>
	<g:field name="worstRating" value="${fieldValue(bean: ratingInstance, field: 'worstRating')}"/>
</div>

