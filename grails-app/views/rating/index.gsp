
<%@ page import="com.shqiperia.entities.Rating" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rating.label', default: 'Rating')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-rating" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-rating" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'rating.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'rating.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="bestRating" title="${message(code: 'rating.bestRating.label', default: 'Best Rating')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'rating.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'rating.image.label', default: 'Image')}" />
					
						<g:sortableColumn property="name" title="${message(code: 'rating.name.label', default: 'Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${ratingInstanceList}" status="i" var="ratingInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${ratingInstance.id}">${fieldValue(bean: ratingInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: ratingInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: ratingInstance, field: "bestRating")}</td>
					
						<td>${fieldValue(bean: ratingInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: ratingInstance, field: "image")}</td>
					
						<td>${fieldValue(bean: ratingInstance, field: "name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${ratingInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
