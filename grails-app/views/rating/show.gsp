
<%@ page import="com.shqiperia.entities.Rating" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rating.label', default: 'Rating')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-rating" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-rating" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list rating">
			
				<g:if test="${ratingInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="rating.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${ratingInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="rating.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${ratingInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.bestRating}">
				<li class="fieldcontain">
					<span id="bestRating-label" class="property-label"><g:message code="rating.bestRating.label" default="Best Rating" /></span>
					
						<span class="property-value" aria-labelledby="bestRating-label"><g:fieldValue bean="${ratingInstance}" field="bestRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="rating.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${ratingInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="rating.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${ratingInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="rating.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${ratingInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.ratingValue}">
				<li class="fieldcontain">
					<span id="ratingValue-label" class="property-label"><g:message code="rating.ratingValue.label" default="Rating Value" /></span>
					
						<span class="property-value" aria-labelledby="ratingValue-label"><g:fieldValue bean="${ratingInstance}" field="ratingValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="rating.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${ratingInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="rating.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${ratingInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${ratingInstance?.worstRating}">
				<li class="fieldcontain">
					<span id="worstRating-label" class="property-label"><g:message code="rating.worstRating.label" default="Worst Rating" /></span>
					
						<span class="property-value" aria-labelledby="worstRating-label"><g:fieldValue bean="${ratingInstance}" field="worstRating"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:ratingInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${ratingInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
