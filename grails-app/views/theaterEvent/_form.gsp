<%@ page import="com.shqiperia.entities.TheaterEvent" %>



<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="theaterEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${theaterEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="theaterEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${theaterEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="theaterEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${theaterEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="theaterEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${theaterEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="theaterEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${theaterEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="theaterEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${theaterEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="theaterEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${theaterEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="theaterEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${theaterEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="theaterEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${theaterEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="theaterEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${theaterEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="theaterEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${theaterEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="theaterEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${theaterEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="theaterEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${theaterEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="theaterEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${theaterEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="theaterEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${theaterEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="theaterEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="theaterEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="theaterEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${theaterEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: theaterEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="theaterEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${theaterEventInstance?.url}"/>
</div>

