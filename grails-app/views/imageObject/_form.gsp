<%@ page import="com.shqiperia.entities.ImageObject" %>



<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'about', 'error')} ">
	<label for="about">
		<g:message code="imageObject.about.label" default="About" />
		
	</label>
	<g:textField name="about" maxlength="250" value="${imageObjectInstance?.about}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'accessibilityAPI', 'error')} ">
	<label for="accessibilityAPI">
		<g:message code="imageObject.accessibilityAPI.label" default="Accessibility API" />
		
	</label>
	<g:textField name="accessibilityAPI" maxlength="250" value="${imageObjectInstance?.accessibilityAPI}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'accessibilityControl', 'error')} ">
	<label for="accessibilityControl">
		<g:message code="imageObject.accessibilityControl.label" default="Accessibility Control" />
		
	</label>
	<g:textField name="accessibilityControl" maxlength="250" value="${imageObjectInstance?.accessibilityControl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'accessibilityFeature', 'error')} ">
	<label for="accessibilityFeature">
		<g:message code="imageObject.accessibilityFeature.label" default="Accessibility Feature" />
		
	</label>
	<g:textField name="accessibilityFeature" maxlength="250" value="${imageObjectInstance?.accessibilityFeature}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'accessibilityHazard', 'error')} ">
	<label for="accessibilityHazard">
		<g:message code="imageObject.accessibilityHazard.label" default="Accessibility Hazard" />
		
	</label>
	<g:textField name="accessibilityHazard" maxlength="250" value="${imageObjectInstance?.accessibilityHazard}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'accountablePerson', 'error')} ">
	<label for="accountablePerson">
		<g:message code="imageObject.accountablePerson.label" default="Accountable Person" />
		
	</label>
	<g:select name="accountablePerson" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.accountablePerson*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="imageObject.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${imageObjectInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="imageObject.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${imageObjectInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="imageObject.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${imageObjectInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'alternativeHeadline', 'error')} ">
	<label for="alternativeHeadline">
		<g:message code="imageObject.alternativeHeadline.label" default="Alternative Headline" />
		
	</label>
	<g:textField name="alternativeHeadline" maxlength="250" value="${imageObjectInstance?.alternativeHeadline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'associatedMedia', 'error')} required">
	<label for="associatedMedia">
		<g:message code="imageObject.associatedMedia.label" default="Associated Media" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="associatedMedia" name="associatedMedia.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" required="" value="${imageObjectInstance?.associatedMedia?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'audience', 'error')} ">
	<label for="audience">
		<g:message code="imageObject.audience.label" default="Audience" />
		
	</label>
	<g:select id="audience" name="audience.id" from="${com.shqiperia.entities.Audience.list()}" optionKey="id" value="${imageObjectInstance?.audience?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'audio', 'error')} ">
	<label for="audio">
		<g:message code="imageObject.audio.label" default="Audio" />
		
	</label>
	<g:select id="audio" name="audio.id" from="${com.shqiperia.entities.AudioObject.list()}" optionKey="id" value="${imageObjectInstance?.audio?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="imageObject.author.label" default="Author" />
		
	</label>
	<g:select name="author" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.author*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="imageObject.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${imageObjectInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'bitrate', 'error')} ">
	<label for="bitrate">
		<g:message code="imageObject.bitrate.label" default="Bitrate" />
		
	</label>
	<g:textField name="bitrate" maxlength="250" value="${imageObjectInstance?.bitrate}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'caption', 'error')} ">
	<label for="caption">
		<g:message code="imageObject.caption.label" default="Caption" />
		
	</label>
	<g:textField name="caption" maxlength="250" value="${imageObjectInstance?.caption}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'citation', 'error')} required">
	<label for="citation">
		<g:message code="imageObject.citation.label" default="Citation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="citation" name="citation.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${imageObjectInstance?.citation?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'comment', 'error')} ">
	<label for="comment">
		<g:message code="imageObject.comment.label" default="Comment" />
		
	</label>
	<g:select id="comment" name="comment.id" from="${com.shqiperia.entities.UserComments.list()}" optionKey="id" value="${imageObjectInstance?.comment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'contentLocation', 'error')} ">
	<label for="contentLocation">
		<g:message code="imageObject.contentLocation.label" default="Content Location" />
		
	</label>
	<g:select id="contentLocation" name="contentLocation.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${imageObjectInstance?.contentLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'contentRating', 'error')} ">
	<label for="contentRating">
		<g:message code="imageObject.contentRating.label" default="Content Rating" />
		
	</label>
	<g:textField name="contentRating" maxlength="250" value="${imageObjectInstance?.contentRating}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'contentSize', 'error')} ">
	<label for="contentSize">
		<g:message code="imageObject.contentSize.label" default="Content Size" />
		
	</label>
	<g:textField name="contentSize" maxlength="250" value="${imageObjectInstance?.contentSize}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'contentUrl', 'error')} ">
	<label for="contentUrl">
		<g:message code="imageObject.contentUrl.label" default="Content Url" />
		
	</label>
	<g:textField name="contentUrl" maxlength="250" value="${imageObjectInstance?.contentUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'contributor', 'error')} ">
	<label for="contributor">
		<g:message code="imageObject.contributor.label" default="Contributor" />
		
	</label>
	<g:select name="contributor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.contributor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'copyrightHolder', 'error')} ">
	<label for="copyrightHolder">
		<g:message code="imageObject.copyrightHolder.label" default="Copyright Holder" />
		
	</label>
	<g:select name="copyrightHolder" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.copyrightHolder*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'copyrightYear', 'error')} ">
	<label for="copyrightYear">
		<g:message code="imageObject.copyrightYear.label" default="Copyright Year" />
		
	</label>
	<g:field name="copyrightYear" type="number" value="${imageObjectInstance.copyrightYear}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="imageObject.creator.label" default="Creator" />
		
	</label>
	<g:select name="creator" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.creator*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'dateModified', 'error')} ">
	<label for="dateModified">
		<g:message code="imageObject.dateModified.label" default="Date Modified" />
		
	</label>
	<g:datePicker name="dateModified" precision="day"  value="${imageObjectInstance?.dateModified}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'datePublished', 'error')} ">
	<label for="datePublished">
		<g:message code="imageObject.datePublished.label" default="Date Published" />
		
	</label>
	<g:datePicker name="datePublished" precision="day"  value="${imageObjectInstance?.datePublished}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="imageObject.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${imageObjectInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'discussionUrl', 'error')} ">
	<label for="discussionUrl">
		<g:message code="imageObject.discussionUrl.label" default="Discussion Url" />
		
	</label>
	<g:textField name="discussionUrl" maxlength="250" value="${imageObjectInstance?.discussionUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="imageObject.duration.label" default="Duration" />
		
	</label>
	<g:textField name="duration" maxlength="250" value="${imageObjectInstance?.duration}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'editor', 'error')} ">
	<label for="editor">
		<g:message code="imageObject.editor.label" default="Editor" />
		
	</label>
	<g:select name="editor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${imageObjectInstance?.editor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'educationalAlignment', 'error')} ">
	<label for="educationalAlignment">
		<g:message code="imageObject.educationalAlignment.label" default="Educational Alignment" />
		
	</label>
	<g:select id="educationalAlignment" name="educationalAlignment.id" from="${com.shqiperia.entities.AlignmentObject.list()}" optionKey="id" value="${imageObjectInstance?.educationalAlignment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'educationalUse', 'error')} ">
	<label for="educationalUse">
		<g:message code="imageObject.educationalUse.label" default="Educational Use" />
		
	</label>
	<g:textField name="educationalUse" maxlength="250" value="${imageObjectInstance?.educationalUse}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'embedUrl', 'error')} ">
	<label for="embedUrl">
		<g:message code="imageObject.embedUrl.label" default="Embed Url" />
		
	</label>
	<g:textField name="embedUrl" maxlength="250" value="${imageObjectInstance?.embedUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'encodesCreativeWork', 'error')} required">
	<label for="encodesCreativeWork">
		<g:message code="imageObject.encodesCreativeWork.label" default="Encodes Creative Work" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="encodesCreativeWork" name="encodesCreativeWork.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${imageObjectInstance?.encodesCreativeWork?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'encoding', 'error')} required">
	<label for="encoding">
		<g:message code="imageObject.encoding.label" default="Encoding" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="encoding" name="encoding.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" required="" value="${imageObjectInstance?.encoding?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'encodingFormat', 'error')} ">
	<label for="encodingFormat">
		<g:message code="imageObject.encodingFormat.label" default="Encoding Format" />
		
	</label>
	<g:textField name="encodingFormat" maxlength="250" value="${imageObjectInstance?.encodingFormat}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'exifData', 'error')} ">
	<label for="exifData">
		<g:message code="imageObject.exifData.label" default="Exif Data" />
		
	</label>
	<g:textField name="exifData" maxlength="250" value="${imageObjectInstance?.exifData}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'expires', 'error')} ">
	<label for="expires">
		<g:message code="imageObject.expires.label" default="Expires" />
		
	</label>
	<g:datePicker name="expires" precision="day"  value="${imageObjectInstance?.expires}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'genre', 'error')} ">
	<label for="genre">
		<g:message code="imageObject.genre.label" default="Genre" />
		
	</label>
	<g:textField name="genre" maxlength="250" value="${imageObjectInstance?.genre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'headline', 'error')} ">
	<label for="headline">
		<g:message code="imageObject.headline.label" default="Headline" />
		
	</label>
	<g:textField name="headline" maxlength="250" value="${imageObjectInstance?.headline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="imageObject.height.label" default="Height" />
		
	</label>
	<g:textField name="height" maxlength="250" value="${imageObjectInstance?.height}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="imageObject.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${imageObjectInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'inLanguage', 'error')} ">
	<label for="inLanguage">
		<g:message code="imageObject.inLanguage.label" default="In Language" />
		
	</label>
	<g:textField name="inLanguage" maxlength="250" value="${imageObjectInstance?.inLanguage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="imageObject.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${imageObjectInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'interactivityType', 'error')} ">
	<label for="interactivityType">
		<g:message code="imageObject.interactivityType.label" default="Interactivity Type" />
		
	</label>
	<g:textField name="interactivityType" maxlength="250" value="${imageObjectInstance?.interactivityType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'isBasedOnUrl', 'error')} ">
	<label for="isBasedOnUrl">
		<g:message code="imageObject.isBasedOnUrl.label" default="Is Based On Url" />
		
	</label>
	<g:textField name="isBasedOnUrl" maxlength="250" value="${imageObjectInstance?.isBasedOnUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'isFamilyFriendly', 'error')} ">
	<label for="isFamilyFriendly">
		<g:message code="imageObject.isFamilyFriendly.label" default="Is Family Friendly" />
		
	</label>
	<g:checkBox name="isFamilyFriendly" value="${imageObjectInstance?.isFamilyFriendly}" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'keywords', 'error')} ">
	<label for="keywords">
		<g:message code="imageObject.keywords.label" default="Keywords" />
		
	</label>
	<g:textField name="keywords" maxlength="250" value="${imageObjectInstance?.keywords}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'learningResourceType', 'error')} ">
	<label for="learningResourceType">
		<g:message code="imageObject.learningResourceType.label" default="Learning Resource Type" />
		
	</label>
	<g:textField name="learningResourceType" maxlength="250" value="${imageObjectInstance?.learningResourceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'mentions', 'error')} ">
	<label for="mentions">
		<g:message code="imageObject.mentions.label" default="Mentions" />
		
	</label>
	<g:textField name="mentions" maxlength="250" value="${imageObjectInstance?.mentions}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="imageObject.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${imageObjectInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'offers', 'error')} ">
	<label for="offers">
		<g:message code="imageObject.offers.label" default="Offers" />
		
	</label>
	<g:select id="offers" name="offers.id" from="${com.shqiperia.entities.Offer.list()}" optionKey="id" value="${imageObjectInstance?.offers?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'playerType', 'error')} ">
	<label for="playerType">
		<g:message code="imageObject.playerType.label" default="Player Type" />
		
	</label>
	<g:textField name="playerType" maxlength="250" value="${imageObjectInstance?.playerType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'productionCompany', 'error')} ">
	<label for="productionCompany">
		<g:message code="imageObject.productionCompany.label" default="Production Company" />
		
	</label>
	<g:select id="productionCompany" name="productionCompany.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${imageObjectInstance?.productionCompany?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'provider', 'error')} ">
	<label for="provider">
		<g:message code="imageObject.provider.label" default="Provider" />
		
	</label>
	<g:select id="provider" name="provider.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${imageObjectInstance?.provider?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'publication', 'error')} ">
	<label for="publication">
		<g:message code="imageObject.publication.label" default="Publication" />
		
	</label>
	<g:select id="publication" name="publication.id" from="${com.shqiperia.entities.PublicationEvent.list()}" optionKey="id" value="${imageObjectInstance?.publication?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'publisher', 'error')} ">
	<label for="publisher">
		<g:message code="imageObject.publisher.label" default="Publisher" />
		
	</label>
	<g:select id="publisher" name="publisher.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${imageObjectInstance?.publisher?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'publishingPrinciples', 'error')} ">
	<label for="publishingPrinciples">
		<g:message code="imageObject.publishingPrinciples.label" default="Publishing Principles" />
		
	</label>
	<g:textField name="publishingPrinciples" maxlength="250" value="${imageObjectInstance?.publishingPrinciples}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'regionsAllowed', 'error')} ">
	<label for="regionsAllowed">
		<g:message code="imageObject.regionsAllowed.label" default="Regions Allowed" />
		
	</label>
	<g:select id="regionsAllowed" name="regionsAllowed.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${imageObjectInstance?.regionsAllowed?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'representativeOfPage', 'error')} ">
	<label for="representativeOfPage">
		<g:message code="imageObject.representativeOfPage.label" default="Representative Of Page" />
		
	</label>
	<g:checkBox name="representativeOfPage" value="${imageObjectInstance?.representativeOfPage}" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'requiresSubscription', 'error')} ">
	<label for="requiresSubscription">
		<g:message code="imageObject.requiresSubscription.label" default="Requires Subscription" />
		
	</label>
	<g:checkBox name="requiresSubscription" value="${imageObjectInstance?.requiresSubscription}" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'review', 'error')} ">
	<label for="review">
		<g:message code="imageObject.review.label" default="Review" />
		
	</label>
	<g:select id="review" name="review.id" from="${com.shqiperia.entities.Review.list()}" optionKey="id" value="${imageObjectInstance?.review?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="imageObject.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${imageObjectInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'sourceOrganization', 'error')} ">
	<label for="sourceOrganization">
		<g:message code="imageObject.sourceOrganization.label" default="Source Organization" />
		
	</label>
	<g:select id="sourceOrganization" name="sourceOrganization.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${imageObjectInstance?.sourceOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="imageObject.text.label" default="Text" />
		
	</label>
	<g:textField name="text" maxlength="250" value="${imageObjectInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'thumbnail', 'error')} ">
	<label for="thumbnail">
		<g:message code="imageObject.thumbnail.label" default="Thumbnail" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'thumbnailUrl', 'error')} ">
	<label for="thumbnailUrl">
		<g:message code="imageObject.thumbnailUrl.label" default="Thumbnail Url" />
		
	</label>
	<g:textField name="thumbnailUrl" maxlength="250" value="${imageObjectInstance?.thumbnailUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'timeRequired', 'error')} ">
	<label for="timeRequired">
		<g:message code="imageObject.timeRequired.label" default="Time Required" />
		
	</label>
	<g:textField name="timeRequired" maxlength="250" value="${imageObjectInstance?.timeRequired}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="imageObject.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${imageObjectInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'uploadDate', 'error')} ">
	<label for="uploadDate">
		<g:message code="imageObject.uploadDate.label" default="Upload Date" />
		
	</label>
	<g:datePicker name="uploadDate" precision="day"  value="${imageObjectInstance?.uploadDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="imageObject.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${imageObjectInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: imageObjectInstance, field: 'width', 'error')} ">
	<label for="width">
		<g:message code="imageObject.width.label" default="Width" />
		
	</label>
	<g:textField name="width" maxlength="250" value="${imageObjectInstance?.width}"/>
</div>

