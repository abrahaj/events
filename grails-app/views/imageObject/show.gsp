
<%@ page import="com.shqiperia.entities.ImageObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'imageObject.label', default: 'ImageObject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-imageObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-imageObject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list imageObject">
			
				<g:if test="${imageObjectInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="imageObject.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${imageObjectInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="imageObject.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${imageObjectInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="imageObject.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${imageObjectInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="imageObject.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${imageObjectInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="imageObject.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${imageObjectInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="imageObject.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${imageObjectInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="imageObject.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${imageObjectInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="imageObject.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${imageObjectInstance?.aggregateRating?.id}">${imageObjectInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="imageObject.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${imageObjectInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="imageObject.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${imageObjectInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.associatedMedia}">
				<li class="fieldcontain">
					<span id="associatedMedia-label" class="property-label"><g:message code="imageObject.associatedMedia.label" default="Associated Media" /></span>
					
						<span class="property-value" aria-labelledby="associatedMedia-label"><g:link controller="mediaObject" action="show" id="${imageObjectInstance?.associatedMedia?.id}">${imageObjectInstance?.associatedMedia?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.audience}">
				<li class="fieldcontain">
					<span id="audience-label" class="property-label"><g:message code="imageObject.audience.label" default="Audience" /></span>
					
						<span class="property-value" aria-labelledby="audience-label"><g:link controller="audience" action="show" id="${imageObjectInstance?.audience?.id}">${imageObjectInstance?.audience?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.audio}">
				<li class="fieldcontain">
					<span id="audio-label" class="property-label"><g:message code="imageObject.audio.label" default="Audio" /></span>
					
						<span class="property-value" aria-labelledby="audio-label"><g:link controller="audioObject" action="show" id="${imageObjectInstance?.audio?.id}">${imageObjectInstance?.audio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="imageObject.author.label" default="Author" /></span>
					
						<g:each in="${imageObjectInstance.author}" var="a">
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="imageObject.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${imageObjectInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.bitrate}">
				<li class="fieldcontain">
					<span id="bitrate-label" class="property-label"><g:message code="imageObject.bitrate.label" default="Bitrate" /></span>
					
						<span class="property-value" aria-labelledby="bitrate-label"><g:fieldValue bean="${imageObjectInstance}" field="bitrate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.caption}">
				<li class="fieldcontain">
					<span id="caption-label" class="property-label"><g:message code="imageObject.caption.label" default="Caption" /></span>
					
						<span class="property-value" aria-labelledby="caption-label"><g:fieldValue bean="${imageObjectInstance}" field="caption"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="imageObject.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${imageObjectInstance?.citation?.id}">${imageObjectInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.comment}">
				<li class="fieldcontain">
					<span id="comment-label" class="property-label"><g:message code="imageObject.comment.label" default="Comment" /></span>
					
						<span class="property-value" aria-labelledby="comment-label"><g:link controller="userComments" action="show" id="${imageObjectInstance?.comment?.id}">${imageObjectInstance?.comment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="imageObject.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${imageObjectInstance?.contentLocation?.id}">${imageObjectInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="imageObject.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${imageObjectInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.contentSize}">
				<li class="fieldcontain">
					<span id="contentSize-label" class="property-label"><g:message code="imageObject.contentSize.label" default="Content Size" /></span>
					
						<span class="property-value" aria-labelledby="contentSize-label"><g:fieldValue bean="${imageObjectInstance}" field="contentSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.contentUrl}">
				<li class="fieldcontain">
					<span id="contentUrl-label" class="property-label"><g:message code="imageObject.contentUrl.label" default="Content Url" /></span>
					
						<span class="property-value" aria-labelledby="contentUrl-label"><g:fieldValue bean="${imageObjectInstance}" field="contentUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="imageObject.contributor.label" default="Contributor" /></span>
					
						<g:each in="${imageObjectInstance.contributor}" var="c">
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="imageObject.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<g:each in="${imageObjectInstance.copyrightHolder}" var="c">
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="imageObject.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${imageObjectInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="imageObject.creator.label" default="Creator" /></span>
					
						<g:each in="${imageObjectInstance.creator}" var="c">
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="imageObject.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${imageObjectInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="imageObject.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${imageObjectInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="imageObject.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${imageObjectInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="imageObject.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${imageObjectInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="imageObject.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${imageObjectInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="imageObject.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:fieldValue bean="${imageObjectInstance}" field="duration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="imageObject.editor.label" default="Editor" /></span>
					
						<g:each in="${imageObjectInstance.editor}" var="e">
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.educationalAlignment}">
				<li class="fieldcontain">
					<span id="educationalAlignment-label" class="property-label"><g:message code="imageObject.educationalAlignment.label" default="Educational Alignment" /></span>
					
						<span class="property-value" aria-labelledby="educationalAlignment-label"><g:link controller="alignmentObject" action="show" id="${imageObjectInstance?.educationalAlignment?.id}">${imageObjectInstance?.educationalAlignment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="imageObject.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${imageObjectInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.embedUrl}">
				<li class="fieldcontain">
					<span id="embedUrl-label" class="property-label"><g:message code="imageObject.embedUrl.label" default="Embed Url" /></span>
					
						<span class="property-value" aria-labelledby="embedUrl-label"><g:fieldValue bean="${imageObjectInstance}" field="embedUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.encodesCreativeWork}">
				<li class="fieldcontain">
					<span id="encodesCreativeWork-label" class="property-label"><g:message code="imageObject.encodesCreativeWork.label" default="Encodes Creative Work" /></span>
					
						<span class="property-value" aria-labelledby="encodesCreativeWork-label"><g:link controller="creativeWork" action="show" id="${imageObjectInstance?.encodesCreativeWork?.id}">${imageObjectInstance?.encodesCreativeWork?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.encoding}">
				<li class="fieldcontain">
					<span id="encoding-label" class="property-label"><g:message code="imageObject.encoding.label" default="Encoding" /></span>
					
						<span class="property-value" aria-labelledby="encoding-label"><g:link controller="mediaObject" action="show" id="${imageObjectInstance?.encoding?.id}">${imageObjectInstance?.encoding?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.encodingFormat}">
				<li class="fieldcontain">
					<span id="encodingFormat-label" class="property-label"><g:message code="imageObject.encodingFormat.label" default="Encoding Format" /></span>
					
						<span class="property-value" aria-labelledby="encodingFormat-label"><g:fieldValue bean="${imageObjectInstance}" field="encodingFormat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.exifData}">
				<li class="fieldcontain">
					<span id="exifData-label" class="property-label"><g:message code="imageObject.exifData.label" default="Exif Data" /></span>
					
						<span class="property-value" aria-labelledby="exifData-label"><g:fieldValue bean="${imageObjectInstance}" field="exifData"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.expires}">
				<li class="fieldcontain">
					<span id="expires-label" class="property-label"><g:message code="imageObject.expires.label" default="Expires" /></span>
					
						<span class="property-value" aria-labelledby="expires-label"><g:formatDate date="${imageObjectInstance?.expires}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="imageObject.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${imageObjectInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="imageObject.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${imageObjectInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="imageObject.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${imageObjectInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="imageObject.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${imageObjectInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="imageObject.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${imageObjectInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="imageObject.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${imageObjectInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="imageObject.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${imageObjectInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="imageObject.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${imageObjectInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="imageObject.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${imageObjectInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="imageObject.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${imageObjectInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="imageObject.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${imageObjectInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="imageObject.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${imageObjectInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="imageObject.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${imageObjectInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="imageObject.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${imageObjectInstance?.offers?.id}">${imageObjectInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.playerType}">
				<li class="fieldcontain">
					<span id="playerType-label" class="property-label"><g:message code="imageObject.playerType.label" default="Player Type" /></span>
					
						<span class="property-value" aria-labelledby="playerType-label"><g:fieldValue bean="${imageObjectInstance}" field="playerType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.productionCompany}">
				<li class="fieldcontain">
					<span id="productionCompany-label" class="property-label"><g:message code="imageObject.productionCompany.label" default="Production Company" /></span>
					
						<span class="property-value" aria-labelledby="productionCompany-label"><g:link controller="organization" action="show" id="${imageObjectInstance?.productionCompany?.id}">${imageObjectInstance?.productionCompany?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="imageObject.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${imageObjectInstance?.provider?.id}">${imageObjectInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.publication}">
				<li class="fieldcontain">
					<span id="publication-label" class="property-label"><g:message code="imageObject.publication.label" default="Publication" /></span>
					
						<span class="property-value" aria-labelledby="publication-label"><g:link controller="publicationEvent" action="show" id="${imageObjectInstance?.publication?.id}">${imageObjectInstance?.publication?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="imageObject.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${imageObjectInstance?.publisher?.id}">${imageObjectInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="imageObject.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${imageObjectInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.regionsAllowed}">
				<li class="fieldcontain">
					<span id="regionsAllowed-label" class="property-label"><g:message code="imageObject.regionsAllowed.label" default="Regions Allowed" /></span>
					
						<span class="property-value" aria-labelledby="regionsAllowed-label"><g:link controller="place" action="show" id="${imageObjectInstance?.regionsAllowed?.id}">${imageObjectInstance?.regionsAllowed?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.representativeOfPage}">
				<li class="fieldcontain">
					<span id="representativeOfPage-label" class="property-label"><g:message code="imageObject.representativeOfPage.label" default="Representative Of Page" /></span>
					
						<span class="property-value" aria-labelledby="representativeOfPage-label"><g:formatBoolean boolean="${imageObjectInstance?.representativeOfPage}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.requiresSubscription}">
				<li class="fieldcontain">
					<span id="requiresSubscription-label" class="property-label"><g:message code="imageObject.requiresSubscription.label" default="Requires Subscription" /></span>
					
						<span class="property-value" aria-labelledby="requiresSubscription-label"><g:formatBoolean boolean="${imageObjectInstance?.requiresSubscription}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="imageObject.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${imageObjectInstance?.review?.id}">${imageObjectInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="imageObject.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${imageObjectInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="imageObject.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${imageObjectInstance?.sourceOrganization?.id}">${imageObjectInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="imageObject.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${imageObjectInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.thumbnail}">
				<li class="fieldcontain">
					<span id="thumbnail-label" class="property-label"><g:message code="imageObject.thumbnail.label" default="Thumbnail" /></span>
					
						<g:each in="${imageObjectInstance.thumbnail}" var="t">
						<span class="property-value" aria-labelledby="thumbnail-label"><g:link controller="imageObject" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="imageObject.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${imageObjectInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.timeRequired}">
				<li class="fieldcontain">
					<span id="timeRequired-label" class="property-label"><g:message code="imageObject.timeRequired.label" default="Time Required" /></span>
					
						<span class="property-value" aria-labelledby="timeRequired-label"><g:fieldValue bean="${imageObjectInstance}" field="timeRequired"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="imageObject.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${imageObjectInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.uploadDate}">
				<li class="fieldcontain">
					<span id="uploadDate-label" class="property-label"><g:message code="imageObject.uploadDate.label" default="Upload Date" /></span>
					
						<span class="property-value" aria-labelledby="uploadDate-label"><g:formatDate date="${imageObjectInstance?.uploadDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="imageObject.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${imageObjectInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${imageObjectInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="imageObject.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${imageObjectInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:imageObjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${imageObjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
