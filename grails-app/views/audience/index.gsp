
<%@ page import="com.shqiperia.entities.Audience" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'audience.label', default: 'Audience')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-audience" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-audience" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'audience.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'audience.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="audienceType" title="${message(code: 'audience.audienceType.label', default: 'Audience Type')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'audience.description.label', default: 'Description')}" />
					
						<th><g:message code="audience.geographicArea.label" default="Geographic Area" /></th>
					
						<g:sortableColumn property="image" title="${message(code: 'audience.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${audienceInstanceList}" status="i" var="audienceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${audienceInstance.id}">${fieldValue(bean: audienceInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: audienceInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: audienceInstance, field: "audienceType")}</td>
					
						<td>${fieldValue(bean: audienceInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: audienceInstance, field: "geographicArea")}</td>
					
						<td>${fieldValue(bean: audienceInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${audienceInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
