<%@ page import="com.shqiperia.entities.Audience" %>



<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="audience.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${audienceInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="audience.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${audienceInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'audienceType', 'error')} ">
	<label for="audienceType">
		<g:message code="audience.audienceType.label" default="Audience Type" />
		
	</label>
	<g:textField name="audienceType" maxlength="250" value="${audienceInstance?.audienceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="audience.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${audienceInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'geographicArea', 'error')} ">
	<label for="geographicArea">
		<g:message code="audience.geographicArea.label" default="Geographic Area" />
		
	</label>
	<g:select id="geographicArea" name="geographicArea.id" from="${com.shqiperia.entities.AdministrativeArea.list()}" optionKey="id" value="${audienceInstance?.geographicArea?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="audience.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${audienceInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="audience.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${audienceInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="audience.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${audienceInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audienceInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="audience.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${audienceInstance?.url}"/>
</div>

