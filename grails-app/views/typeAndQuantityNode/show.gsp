
<%@ page import="com.shqiperia.entities.TypeAndQuantityNode" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'typeAndQuantityNode.label', default: 'TypeAndQuantityNode')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-typeAndQuantityNode" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-typeAndQuantityNode" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list typeAndQuantityNode">
			
				<g:if test="${typeAndQuantityNodeInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="typeAndQuantityNode.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="typeAndQuantityNode.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.amountOfThisGood}">
				<li class="fieldcontain">
					<span id="amountOfThisGood-label" class="property-label"><g:message code="typeAndQuantityNode.amountOfThisGood.label" default="Amount Of This Good" /></span>
					
						<span class="property-value" aria-labelledby="amountOfThisGood-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="amountOfThisGood"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.businessFunction}">
				<li class="fieldcontain">
					<span id="businessFunction-label" class="property-label"><g:message code="typeAndQuantityNode.businessFunction.label" default="Business Function" /></span>
					
						<span class="property-value" aria-labelledby="businessFunction-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="businessFunction"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="typeAndQuantityNode.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="typeAndQuantityNode.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="typeAndQuantityNode.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="typeAndQuantityNode.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.typeOfGood}">
				<li class="fieldcontain">
					<span id="typeOfGood-label" class="property-label"><g:message code="typeAndQuantityNode.typeOfGood.label" default="Type Of Good" /></span>
					
						<span class="property-value" aria-labelledby="typeOfGood-label"><g:link controller="product" action="show" id="${typeAndQuantityNodeInstance?.typeOfGood?.id}">${typeAndQuantityNodeInstance?.typeOfGood?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.unitCode}">
				<li class="fieldcontain">
					<span id="unitCode-label" class="property-label"><g:message code="typeAndQuantityNode.unitCode.label" default="Unit Code" /></span>
					
						<span class="property-value" aria-labelledby="unitCode-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="unitCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${typeAndQuantityNodeInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="typeAndQuantityNode.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${typeAndQuantityNodeInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:typeAndQuantityNodeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${typeAndQuantityNodeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
