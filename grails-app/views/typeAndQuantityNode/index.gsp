
<%@ page import="com.shqiperia.entities.TypeAndQuantityNode" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'typeAndQuantityNode.label', default: 'TypeAndQuantityNode')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-typeAndQuantityNode" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-typeAndQuantityNode" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'typeAndQuantityNode.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'typeAndQuantityNode.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="amountOfThisGood" title="${message(code: 'typeAndQuantityNode.amountOfThisGood.label', default: 'Amount Of This Good')}" />
					
						<g:sortableColumn property="businessFunction" title="${message(code: 'typeAndQuantityNode.businessFunction.label', default: 'Business Function')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'typeAndQuantityNode.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'typeAndQuantityNode.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${typeAndQuantityNodeInstanceList}" status="i" var="typeAndQuantityNodeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${typeAndQuantityNodeInstance.id}">${fieldValue(bean: typeAndQuantityNodeInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: typeAndQuantityNodeInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: typeAndQuantityNodeInstance, field: "amountOfThisGood")}</td>
					
						<td>${fieldValue(bean: typeAndQuantityNodeInstance, field: "businessFunction")}</td>
					
						<td>${fieldValue(bean: typeAndQuantityNodeInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: typeAndQuantityNodeInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${typeAndQuantityNodeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
