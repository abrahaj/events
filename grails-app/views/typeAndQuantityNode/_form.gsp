<%@ page import="com.shqiperia.entities.TypeAndQuantityNode" %>



<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="typeAndQuantityNode.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${typeAndQuantityNodeInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="typeAndQuantityNode.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${typeAndQuantityNodeInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'amountOfThisGood', 'error')} ">
	<label for="amountOfThisGood">
		<g:message code="typeAndQuantityNode.amountOfThisGood.label" default="Amount Of This Good" />
		
	</label>
	<g:field name="amountOfThisGood" value="${fieldValue(bean: typeAndQuantityNodeInstance, field: 'amountOfThisGood')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'businessFunction', 'error')} ">
	<label for="businessFunction">
		<g:message code="typeAndQuantityNode.businessFunction.label" default="Business Function" />
		
	</label>
	<g:select name="businessFunction" from="${com.shqiperia.entities.TypeAndQuantityNode$BusinessFunction?.values()}" keys="${com.shqiperia.entities.TypeAndQuantityNode$BusinessFunction.values()*.name()}" value="${typeAndQuantityNodeInstance?.businessFunction?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="typeAndQuantityNode.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${typeAndQuantityNodeInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="typeAndQuantityNode.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${typeAndQuantityNodeInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="typeAndQuantityNode.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${typeAndQuantityNodeInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="typeAndQuantityNode.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${typeAndQuantityNodeInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'typeOfGood', 'error')} ">
	<label for="typeOfGood">
		<g:message code="typeAndQuantityNode.typeOfGood.label" default="Type Of Good" />
		
	</label>
	<g:select id="typeOfGood" name="typeOfGood.id" from="${com.shqiperia.entities.Product.list()}" optionKey="id" value="${typeAndQuantityNodeInstance?.typeOfGood?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'unitCode', 'error')} ">
	<label for="unitCode">
		<g:message code="typeAndQuantityNode.unitCode.label" default="Unit Code" />
		
	</label>
	<g:textField name="unitCode" maxlength="250" value="${typeAndQuantityNodeInstance?.unitCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: typeAndQuantityNodeInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="typeAndQuantityNode.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${typeAndQuantityNodeInstance?.url}"/>
</div>

