
<%@ page import="com.shqiperia.entities.Organization" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'organization.label', default: 'Organization')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-organization" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-organization" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list organization">
			
				<g:if test="${organizationInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="organization.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${organizationInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.legalName}">
				<li class="fieldcontain">
					<span id="legalName-label" class="property-label"><g:message code="organization.legalName.label" default="Legal Name" /></span>
					
						<span class="property-value" aria-labelledby="legalName-label"><g:fieldValue bean="${organizationInstance}" field="legalName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="organization.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${organizationInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="organization.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${organizationInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="organization.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="postaladdress" action="show" id="${organizationInstance?.address?.id}">${organizationInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.contactPoint}">
				<li class="fieldcontain">
					<span id="contactPoint-label" class="property-label"><g:message code="organization.contactPoint.label" default="Contact Point" /></span>
					
						<span class="property-value" aria-labelledby="contactPoint-label"><g:link controller="contactpoint" action="show" id="${organizationInstance?.contactPoint?.id}">${organizationInstance?.contactPoint?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="organization.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${organizationInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.duns}">
				<li class="fieldcontain">
					<span id="duns-label" class="property-label"><g:message code="organization.duns.label" default="Duns" /></span>
					
						<span class="property-value" aria-labelledby="duns-label"><g:fieldValue bean="${organizationInstance}" field="duns"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="organization.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${organizationInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.foundingDate}">
				<li class="fieldcontain">
					<span id="foundingDate-label" class="property-label"><g:message code="organization.foundingDate.label" default="Founding Date" /></span>
					
						<span class="property-value" aria-labelledby="foundingDate-label"><g:formatDate date="${organizationInstance?.foundingDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.globalLocationNumber}">
				<li class="fieldcontain">
					<span id="globalLocationNumber-label" class="property-label"><g:message code="organization.globalLocationNumber.label" default="Global Location Number" /></span>
					
						<span class="property-value" aria-labelledby="globalLocationNumber-label"><g:fieldValue bean="${organizationInstance}" field="globalLocationNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="organization.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${organizationInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="organization.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${organizationInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.isicV4}">
				<li class="fieldcontain">
					<span id="isicV4-label" class="property-label"><g:message code="organization.isicV4.label" default="Isic V4" /></span>
					
						<span class="property-value" aria-labelledby="isicV4-label"><g:fieldValue bean="${organizationInstance}" field="isicV4"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.logo}">
				<li class="fieldcontain">
					<span id="logo-label" class="property-label"><g:message code="organization.logo.label" default="Logo" /></span>
					
						<span class="property-value" aria-labelledby="logo-label"><g:fieldValue bean="${organizationInstance}" field="logo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="organization.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${organizationInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.taxID}">
				<li class="fieldcontain">
					<span id="taxID-label" class="property-label"><g:message code="organization.taxID.label" default="Tax ID" /></span>
					
						<span class="property-value" aria-labelledby="taxID-label"><g:fieldValue bean="${organizationInstance}" field="taxID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="organization.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${organizationInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="organization.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${organizationInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${organizationInstance?.vatID}">
				<li class="fieldcontain">
					<span id="vatID-label" class="property-label"><g:message code="organization.vatID.label" default="Vat ID" /></span>
					
						<span class="property-value" aria-labelledby="vatID-label"><g:fieldValue bean="${organizationInstance}" field="vatID"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:organizationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${organizationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
