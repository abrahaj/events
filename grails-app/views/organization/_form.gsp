<%@ page import="com.shqiperia.entities.Organization" %>



<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="organization.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${organizationInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'legalName', 'error')} ">
	<label for="legalName">
		<g:message code="organization.legalName.label" default="Legal Name" />
		
	</label>
	<g:textField name="legalName" maxlength="250" value="${organizationInstance?.legalName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="organization.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" maxlength="250" value="${organizationInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="organization.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${organizationInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="organization.address.label" default="Address" />
		
	</label>
	<g:select id="address" name="address.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${organizationInstance?.address?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'contactPoint', 'error')} ">
	<label for="contactPoint">
		<g:message code="organization.contactPoint.label" default="Contact Point" />
		
	</label>
	<g:select id="contactPoint" name="contactPoint.id" from="${com.shqiperia.entities.Contactpoint.list()}" optionKey="id" value="${organizationInstance?.contactPoint?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="organization.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${organizationInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'duns', 'error')} ">
	<label for="duns">
		<g:message code="organization.duns.label" default="Duns" />
		
	</label>
	<g:textField name="duns" maxlength="250" value="${organizationInstance?.duns}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="organization.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${organizationInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'foundingDate', 'error')} ">
	<label for="foundingDate">
		<g:message code="organization.foundingDate.label" default="Founding Date" />
		
	</label>
	<g:datePicker name="foundingDate" precision="day"  value="${organizationInstance?.foundingDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'globalLocationNumber', 'error')} ">
	<label for="globalLocationNumber">
		<g:message code="organization.globalLocationNumber.label" default="Global Location Number" />
		
	</label>
	<g:textField name="globalLocationNumber" maxlength="250" value="${organizationInstance?.globalLocationNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="organization.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${organizationInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="organization.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${organizationInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'isicV4', 'error')} ">
	<label for="isicV4">
		<g:message code="organization.isicV4.label" default="Isic V4" />
		
	</label>
	<g:textField name="isicV4" maxlength="250" value="${organizationInstance?.isicV4}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'logo', 'error')} ">
	<label for="logo">
		<g:message code="organization.logo.label" default="Logo" />
		
	</label>
	<g:textField name="logo" maxlength="250" value="${organizationInstance?.logo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="organization.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${organizationInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'taxID', 'error')} ">
	<label for="taxID">
		<g:message code="organization.taxID.label" default="Tax ID" />
		
	</label>
	<g:textField name="taxID" maxlength="250" value="${organizationInstance?.taxID}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="organization.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${organizationInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="organization.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${organizationInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: organizationInstance, field: 'vatID', 'error')} ">
	<label for="vatID">
		<g:message code="organization.vatID.label" default="Vat ID" />
		
	</label>
	<g:textField name="vatID" maxlength="250" value="${organizationInstance?.vatID}"/>
</div>

