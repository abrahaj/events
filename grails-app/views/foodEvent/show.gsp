
<%@ page import="com.shqiperia.entities.FoodEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'foodEvent.label', default: 'FoodEvent')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-foodEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-foodEvent" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list foodEvent">
			
				<g:if test="${foodEventInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="foodEvent.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${foodEventInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="foodEvent.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${foodEventInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="foodEvent.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${foodEventInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.attendee}">
				<li class="fieldcontain">
					<span id="attendee-label" class="property-label"><g:message code="foodEvent.attendee.label" default="Attendee" /></span>
					
						<g:each in="${foodEventInstance.attendee}" var="a">
						<span class="property-value" aria-labelledby="attendee-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="foodEvent.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${foodEventInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.doorTime}">
				<li class="fieldcontain">
					<span id="doorTime-label" class="property-label"><g:message code="foodEvent.doorTime.label" default="Door Time" /></span>
					
						<span class="property-value" aria-labelledby="doorTime-label"><g:formatDate date="${foodEventInstance?.doorTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="foodEvent.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:formatDate date="${foodEventInstance?.duration}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="foodEvent.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${foodEventInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.eventStatus}">
				<li class="fieldcontain">
					<span id="eventStatus-label" class="property-label"><g:message code="foodEvent.eventStatus.label" default="Event Status" /></span>
					
						<span class="property-value" aria-labelledby="eventStatus-label"><g:fieldValue bean="${foodEventInstance}" field="eventStatus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="foodEvent.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${foodEventInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="foodEvent.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:link controller="postaladdress" action="show" id="${foodEventInstance?.location?.id}">${foodEventInstance?.location?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.performer}">
				<li class="fieldcontain">
					<span id="performer-label" class="property-label"><g:message code="foodEvent.performer.label" default="Performer" /></span>
					
						<g:each in="${foodEventInstance.performer}" var="p">
						<span class="property-value" aria-labelledby="performer-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.previousStartDate}">
				<li class="fieldcontain">
					<span id="previousStartDate-label" class="property-label"><g:message code="foodEvent.previousStartDate.label" default="Previous Start Date" /></span>
					
						<span class="property-value" aria-labelledby="previousStartDate-label"><g:formatDate date="${foodEventInstance?.previousStartDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="foodEvent.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${foodEventInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="foodEvent.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${foodEventInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.subEvent}">
				<li class="fieldcontain">
					<span id="subEvent-label" class="property-label"><g:message code="foodEvent.subEvent.label" default="Sub Event" /></span>
					
						<g:each in="${foodEventInstance.subEvent}" var="s">
						<span class="property-value" aria-labelledby="subEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.superEvent}">
				<li class="fieldcontain">
					<span id="superEvent-label" class="property-label"><g:message code="foodEvent.superEvent.label" default="Super Event" /></span>
					
						<g:each in="${foodEventInstance.superEvent}" var="s">
						<span class="property-value" aria-labelledby="superEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="foodEvent.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${foodEventInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${foodEventInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="foodEvent.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${foodEventInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:foodEventInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${foodEventInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
