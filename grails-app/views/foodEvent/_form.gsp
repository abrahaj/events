<%@ page import="com.shqiperia.entities.FoodEvent" %>



<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="foodEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${foodEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="foodEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${foodEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="foodEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${foodEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="foodEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${foodEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="foodEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${foodEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="foodEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${foodEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="foodEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${foodEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="foodEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${foodEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="foodEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${foodEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="foodEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${foodEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="foodEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${foodEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="foodEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${foodEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="foodEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${foodEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="foodEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${foodEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="foodEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${foodEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="foodEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="foodEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="foodEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${foodEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: foodEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="foodEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${foodEventInstance?.url}"/>
</div>

