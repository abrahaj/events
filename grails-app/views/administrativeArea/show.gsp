
<%@ page import="com.shqiperia.entities.AdministrativeArea" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'administrativeArea.label', default: 'AdministrativeArea')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-administrativeArea" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-administrativeArea" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list administrativeArea">
			
				<g:if test="${administrativeAreaInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="administrativeArea.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${administrativeAreaInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="administrativeArea.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="postaladdress" action="show" id="${administrativeAreaInstance?.address?.id}">${administrativeAreaInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="administrativeArea.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${administrativeAreaInstance?.aggregateRating?.id}">${administrativeAreaInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="administrativeArea.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${administrativeAreaInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.containedIn}">
				<li class="fieldcontain">
					<span id="containedIn-label" class="property-label"><g:message code="administrativeArea.containedIn.label" default="Contained In" /></span>
					
						<span class="property-value" aria-labelledby="containedIn-label"><g:link controller="place" action="show" id="${administrativeAreaInstance?.containedIn?.id}">${administrativeAreaInstance?.containedIn?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="administrativeArea.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${administrativeAreaInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.event}">
				<li class="fieldcontain">
					<span id="event-label" class="property-label"><g:message code="administrativeArea.event.label" default="Event" /></span>
					
						<g:each in="${administrativeAreaInstance.event}" var="e">
						<span class="property-value" aria-labelledby="event-label"><g:link controller="event" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="administrativeArea.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${administrativeAreaInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.globalLocationNumber}">
				<li class="fieldcontain">
					<span id="globalLocationNumber-label" class="property-label"><g:message code="administrativeArea.globalLocationNumber.label" default="Global Location Number" /></span>
					
						<span class="property-value" aria-labelledby="globalLocationNumber-label"><g:fieldValue bean="${administrativeAreaInstance}" field="globalLocationNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="administrativeArea.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${administrativeAreaInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="administrativeArea.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${administrativeAreaInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.isicV4}">
				<li class="fieldcontain">
					<span id="isicV4-label" class="property-label"><g:message code="administrativeArea.isicV4.label" default="Isic V4" /></span>
					
						<span class="property-value" aria-labelledby="isicV4-label"><g:fieldValue bean="${administrativeAreaInstance}" field="isicV4"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.logo}">
				<li class="fieldcontain">
					<span id="logo-label" class="property-label"><g:message code="administrativeArea.logo.label" default="Logo" /></span>
					
						<span class="property-value" aria-labelledby="logo-label"><g:fieldValue bean="${administrativeAreaInstance}" field="logo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.map}">
				<li class="fieldcontain">
					<span id="map-label" class="property-label"><g:message code="administrativeArea.map.label" default="Map" /></span>
					
						<span class="property-value" aria-labelledby="map-label"><g:fieldValue bean="${administrativeAreaInstance}" field="map"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="administrativeArea.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${administrativeAreaInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="administrativeArea.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${administrativeAreaInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="administrativeArea.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${administrativeAreaInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${administrativeAreaInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="administrativeArea.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${administrativeAreaInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:administrativeAreaInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${administrativeAreaInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
