<%@ page import="com.shqiperia.entities.AdministrativeArea" %>



<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="administrativeArea.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${administrativeAreaInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="administrativeArea.address.label" default="Address" />
		
	</label>
	<g:select id="address" name="address.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${administrativeAreaInstance?.address?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="administrativeArea.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${administrativeAreaInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="administrativeArea.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${administrativeAreaInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'containedIn', 'error')} required">
	<label for="containedIn">
		<g:message code="administrativeArea.containedIn.label" default="Contained In" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="containedIn" name="containedIn.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" required="" value="${administrativeAreaInstance?.containedIn?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="administrativeArea.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${administrativeAreaInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'event', 'error')} ">
	<label for="event">
		<g:message code="administrativeArea.event.label" default="Event" />
		
	</label>
	<g:select name="event" from="${com.shqiperia.entities.Event.list()}" multiple="multiple" optionKey="id" size="5" value="${administrativeAreaInstance?.event*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="administrativeArea.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${administrativeAreaInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'globalLocationNumber', 'error')} ">
	<label for="globalLocationNumber">
		<g:message code="administrativeArea.globalLocationNumber.label" default="Global Location Number" />
		
	</label>
	<g:textField name="globalLocationNumber" maxlength="250" value="${administrativeAreaInstance?.globalLocationNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="administrativeArea.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${administrativeAreaInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="administrativeArea.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${administrativeAreaInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'isicV4', 'error')} ">
	<label for="isicV4">
		<g:message code="administrativeArea.isicV4.label" default="Isic V4" />
		
	</label>
	<g:textField name="isicV4" maxlength="250" value="${administrativeAreaInstance?.isicV4}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'logo', 'error')} ">
	<label for="logo">
		<g:message code="administrativeArea.logo.label" default="Logo" />
		
	</label>
	<g:textField name="logo" maxlength="250" value="${administrativeAreaInstance?.logo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'map', 'error')} ">
	<label for="map">
		<g:message code="administrativeArea.map.label" default="Map" />
		
	</label>
	<g:textField name="map" maxlength="250" value="${administrativeAreaInstance?.map}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="administrativeArea.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${administrativeAreaInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="administrativeArea.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${administrativeAreaInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="administrativeArea.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${administrativeAreaInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: administrativeAreaInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="administrativeArea.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${administrativeAreaInstance?.url}"/>
</div>

