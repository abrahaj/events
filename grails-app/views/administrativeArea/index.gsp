
<%@ page import="com.shqiperia.entities.AdministrativeArea" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'administrativeArea.label', default: 'AdministrativeArea')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-administrativeArea" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-administrativeArea" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'administrativeArea.additionalType.label', default: 'Additional Type')}" />
					
						<th><g:message code="administrativeArea.address.label" default="Address" /></th>
					
						<th><g:message code="administrativeArea.aggregateRating.label" default="Aggregate Rating" /></th>
					
						<g:sortableColumn property="alternateName" title="${message(code: 'administrativeArea.alternateName.label', default: 'Alternate Name')}" />
					
						<th><g:message code="administrativeArea.containedIn.label" default="Contained In" /></th>
					
						<g:sortableColumn property="description" title="${message(code: 'administrativeArea.description.label', default: 'Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${administrativeAreaInstanceList}" status="i" var="administrativeAreaInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${administrativeAreaInstance.id}">${fieldValue(bean: administrativeAreaInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: administrativeAreaInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: administrativeAreaInstance, field: "aggregateRating")}</td>
					
						<td>${fieldValue(bean: administrativeAreaInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: administrativeAreaInstance, field: "containedIn")}</td>
					
						<td>${fieldValue(bean: administrativeAreaInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${administrativeAreaInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
