
<%@ page import="com.shqiperia.entities.CreativeWork" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'creativeWork.label', default: 'CreativeWork')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-creativeWork" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-creativeWork" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list creativeWork">
			
				<g:if test="${creativeWorkInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="creativeWork.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${creativeWorkInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="creativeWork.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${creativeWorkInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="creativeWork.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${creativeWorkInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="creativeWork.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${creativeWorkInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="creativeWork.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${creativeWorkInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="creativeWork.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${creativeWorkInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="creativeWork.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${creativeWorkInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="creativeWork.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${creativeWorkInstance?.aggregateRating?.id}">${creativeWorkInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="creativeWork.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${creativeWorkInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="creativeWork.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${creativeWorkInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.associatedMedia}">
				<li class="fieldcontain">
					<span id="associatedMedia-label" class="property-label"><g:message code="creativeWork.associatedMedia.label" default="Associated Media" /></span>
					
						<span class="property-value" aria-labelledby="associatedMedia-label"><g:link controller="mediaObject" action="show" id="${creativeWorkInstance?.associatedMedia?.id}">${creativeWorkInstance?.associatedMedia?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.audience}">
				<li class="fieldcontain">
					<span id="audience-label" class="property-label"><g:message code="creativeWork.audience.label" default="Audience" /></span>
					
						<span class="property-value" aria-labelledby="audience-label"><g:link controller="audience" action="show" id="${creativeWorkInstance?.audience?.id}">${creativeWorkInstance?.audience?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.audio}">
				<li class="fieldcontain">
					<span id="audio-label" class="property-label"><g:message code="creativeWork.audio.label" default="Audio" /></span>
					
						<span class="property-value" aria-labelledby="audio-label"><g:link controller="audioObject" action="show" id="${creativeWorkInstance?.audio?.id}">${creativeWorkInstance?.audio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="creativeWork.author.label" default="Author" /></span>
					
						<g:each in="${creativeWorkInstance.author}" var="a">
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="creativeWork.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${creativeWorkInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="creativeWork.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${creativeWorkInstance?.citation?.id}">${creativeWorkInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.comment}">
				<li class="fieldcontain">
					<span id="comment-label" class="property-label"><g:message code="creativeWork.comment.label" default="Comment" /></span>
					
						<span class="property-value" aria-labelledby="comment-label"><g:link controller="userComments" action="show" id="${creativeWorkInstance?.comment?.id}">${creativeWorkInstance?.comment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="creativeWork.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${creativeWorkInstance?.contentLocation?.id}">${creativeWorkInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="creativeWork.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${creativeWorkInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="creativeWork.contributor.label" default="Contributor" /></span>
					
						<g:each in="${creativeWorkInstance.contributor}" var="c">
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="creativeWork.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<g:each in="${creativeWorkInstance.copyrightHolder}" var="c">
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="creativeWork.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${creativeWorkInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="creativeWork.creator.label" default="Creator" /></span>
					
						<g:each in="${creativeWorkInstance.creator}" var="c">
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="creativeWork.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${creativeWorkInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="creativeWork.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${creativeWorkInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="creativeWork.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${creativeWorkInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="creativeWork.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${creativeWorkInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="creativeWork.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${creativeWorkInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="creativeWork.editor.label" default="Editor" /></span>
					
						<g:each in="${creativeWorkInstance.editor}" var="e">
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.educationalAlignment}">
				<li class="fieldcontain">
					<span id="educationalAlignment-label" class="property-label"><g:message code="creativeWork.educationalAlignment.label" default="Educational Alignment" /></span>
					
						<span class="property-value" aria-labelledby="educationalAlignment-label"><g:link controller="alignmentObject" action="show" id="${creativeWorkInstance?.educationalAlignment?.id}">${creativeWorkInstance?.educationalAlignment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="creativeWork.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${creativeWorkInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.encoding}">
				<li class="fieldcontain">
					<span id="encoding-label" class="property-label"><g:message code="creativeWork.encoding.label" default="Encoding" /></span>
					
						<span class="property-value" aria-labelledby="encoding-label"><g:link controller="mediaObject" action="show" id="${creativeWorkInstance?.encoding?.id}">${creativeWorkInstance?.encoding?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="creativeWork.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${creativeWorkInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="creativeWork.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${creativeWorkInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="creativeWork.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${creativeWorkInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="creativeWork.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${creativeWorkInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="creativeWork.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${creativeWorkInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="creativeWork.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${creativeWorkInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="creativeWork.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${creativeWorkInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="creativeWork.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${creativeWorkInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="creativeWork.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${creativeWorkInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="creativeWork.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${creativeWorkInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="creativeWork.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${creativeWorkInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="creativeWork.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${creativeWorkInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="creativeWork.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${creativeWorkInstance?.offers?.id}">${creativeWorkInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="creativeWork.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${creativeWorkInstance?.provider?.id}">${creativeWorkInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="creativeWork.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${creativeWorkInstance?.publisher?.id}">${creativeWorkInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="creativeWork.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${creativeWorkInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="creativeWork.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${creativeWorkInstance?.review?.id}">${creativeWorkInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="creativeWork.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${creativeWorkInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="creativeWork.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${creativeWorkInstance?.sourceOrganization?.id}">${creativeWorkInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="creativeWork.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${creativeWorkInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="creativeWork.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${creativeWorkInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.timeRequired}">
				<li class="fieldcontain">
					<span id="timeRequired-label" class="property-label"><g:message code="creativeWork.timeRequired.label" default="Time Required" /></span>
					
						<span class="property-value" aria-labelledby="timeRequired-label"><g:fieldValue bean="${creativeWorkInstance}" field="timeRequired"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="creativeWork.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${creativeWorkInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${creativeWorkInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="creativeWork.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${creativeWorkInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:creativeWorkInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${creativeWorkInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
