
<%@ page import="com.shqiperia.entities.CreativeWork" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'creativeWork.label', default: 'CreativeWork')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-creativeWork" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-creativeWork" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="about" title="${message(code: 'creativeWork.about.label', default: 'About')}" />
					
						<g:sortableColumn property="accessibilityAPI" title="${message(code: 'creativeWork.accessibilityAPI.label', default: 'Accessibility API')}" />
					
						<g:sortableColumn property="accessibilityControl" title="${message(code: 'creativeWork.accessibilityControl.label', default: 'Accessibility Control')}" />
					
						<g:sortableColumn property="accessibilityFeature" title="${message(code: 'creativeWork.accessibilityFeature.label', default: 'Accessibility Feature')}" />
					
						<g:sortableColumn property="accessibilityHazard" title="${message(code: 'creativeWork.accessibilityHazard.label', default: 'Accessibility Hazard')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'creativeWork.additionalType.label', default: 'Additional Type')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${creativeWorkInstanceList}" status="i" var="creativeWorkInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${creativeWorkInstance.id}">${fieldValue(bean: creativeWorkInstance, field: "about")}</g:link></td>
					
						<td>${fieldValue(bean: creativeWorkInstance, field: "accessibilityAPI")}</td>
					
						<td>${fieldValue(bean: creativeWorkInstance, field: "accessibilityControl")}</td>
					
						<td>${fieldValue(bean: creativeWorkInstance, field: "accessibilityFeature")}</td>
					
						<td>${fieldValue(bean: creativeWorkInstance, field: "accessibilityHazard")}</td>
					
						<td>${fieldValue(bean: creativeWorkInstance, field: "additionalType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${creativeWorkInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
