<%@ page import="com.shqiperia.entities.CreativeWork" %>



<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'about', 'error')} ">
	<label for="about">
		<g:message code="creativeWork.about.label" default="About" />
		
	</label>
	<g:textField name="about" maxlength="250" value="${creativeWorkInstance?.about}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'accessibilityAPI', 'error')} ">
	<label for="accessibilityAPI">
		<g:message code="creativeWork.accessibilityAPI.label" default="Accessibility API" />
		
	</label>
	<g:textField name="accessibilityAPI" maxlength="250" value="${creativeWorkInstance?.accessibilityAPI}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'accessibilityControl', 'error')} ">
	<label for="accessibilityControl">
		<g:message code="creativeWork.accessibilityControl.label" default="Accessibility Control" />
		
	</label>
	<g:textField name="accessibilityControl" maxlength="250" value="${creativeWorkInstance?.accessibilityControl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'accessibilityFeature', 'error')} ">
	<label for="accessibilityFeature">
		<g:message code="creativeWork.accessibilityFeature.label" default="Accessibility Feature" />
		
	</label>
	<g:textField name="accessibilityFeature" maxlength="250" value="${creativeWorkInstance?.accessibilityFeature}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'accessibilityHazard', 'error')} ">
	<label for="accessibilityHazard">
		<g:message code="creativeWork.accessibilityHazard.label" default="Accessibility Hazard" />
		
	</label>
	<g:textField name="accessibilityHazard" maxlength="250" value="${creativeWorkInstance?.accessibilityHazard}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'accountablePerson', 'error')} ">
	<label for="accountablePerson">
		<g:message code="creativeWork.accountablePerson.label" default="Accountable Person" />
		
	</label>
	<g:select name="accountablePerson" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.accountablePerson*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="creativeWork.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${creativeWorkInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="creativeWork.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${creativeWorkInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="creativeWork.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${creativeWorkInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'alternativeHeadline', 'error')} ">
	<label for="alternativeHeadline">
		<g:message code="creativeWork.alternativeHeadline.label" default="Alternative Headline" />
		
	</label>
	<g:textField name="alternativeHeadline" maxlength="250" value="${creativeWorkInstance?.alternativeHeadline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'associatedMedia', 'error')} ">
	<label for="associatedMedia">
		<g:message code="creativeWork.associatedMedia.label" default="Associated Media" />
		
	</label>
	<g:select id="associatedMedia" name="associatedMedia.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" value="${creativeWorkInstance?.associatedMedia?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'audience', 'error')} ">
	<label for="audience">
		<g:message code="creativeWork.audience.label" default="Audience" />
		
	</label>
	<g:select id="audience" name="audience.id" from="${com.shqiperia.entities.Audience.list()}" optionKey="id" value="${creativeWorkInstance?.audience?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'audio', 'error')} ">
	<label for="audio">
		<g:message code="creativeWork.audio.label" default="Audio" />
		
	</label>
	<g:select id="audio" name="audio.id" from="${com.shqiperia.entities.AudioObject.list()}" optionKey="id" value="${creativeWorkInstance?.audio?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="creativeWork.author.label" default="Author" />
		
	</label>
	<g:select name="author" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.author*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="creativeWork.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${creativeWorkInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'citation', 'error')} required">
	<label for="citation">
		<g:message code="creativeWork.citation.label" default="Citation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="citation" name="citation.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${creativeWorkInstance?.citation?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'comment', 'error')} ">
	<label for="comment">
		<g:message code="creativeWork.comment.label" default="Comment" />
		
	</label>
	<g:select id="comment" name="comment.id" from="${com.shqiperia.entities.UserComments.list()}" optionKey="id" value="${creativeWorkInstance?.comment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'contentLocation', 'error')} ">
	<label for="contentLocation">
		<g:message code="creativeWork.contentLocation.label" default="Content Location" />
		
	</label>
	<g:select id="contentLocation" name="contentLocation.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${creativeWorkInstance?.contentLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'contentRating', 'error')} ">
	<label for="contentRating">
		<g:message code="creativeWork.contentRating.label" default="Content Rating" />
		
	</label>
	<g:textField name="contentRating" maxlength="250" value="${creativeWorkInstance?.contentRating}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'contributor', 'error')} ">
	<label for="contributor">
		<g:message code="creativeWork.contributor.label" default="Contributor" />
		
	</label>
	<g:select name="contributor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.contributor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'copyrightHolder', 'error')} ">
	<label for="copyrightHolder">
		<g:message code="creativeWork.copyrightHolder.label" default="Copyright Holder" />
		
	</label>
	<g:select name="copyrightHolder" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.copyrightHolder*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'copyrightYear', 'error')} ">
	<label for="copyrightYear">
		<g:message code="creativeWork.copyrightYear.label" default="Copyright Year" />
		
	</label>
	<g:field name="copyrightYear" type="number" value="${creativeWorkInstance.copyrightYear}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="creativeWork.creator.label" default="Creator" />
		
	</label>
	<g:select name="creator" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.creator*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'dateModified', 'error')} ">
	<label for="dateModified">
		<g:message code="creativeWork.dateModified.label" default="Date Modified" />
		
	</label>
	<g:datePicker name="dateModified" precision="day"  value="${creativeWorkInstance?.dateModified}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'datePublished', 'error')} ">
	<label for="datePublished">
		<g:message code="creativeWork.datePublished.label" default="Date Published" />
		
	</label>
	<g:datePicker name="datePublished" precision="day"  value="${creativeWorkInstance?.datePublished}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="creativeWork.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${creativeWorkInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'discussionUrl', 'error')} ">
	<label for="discussionUrl">
		<g:message code="creativeWork.discussionUrl.label" default="Discussion Url" />
		
	</label>
	<g:textField name="discussionUrl" maxlength="250" value="${creativeWorkInstance?.discussionUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'editor', 'error')} ">
	<label for="editor">
		<g:message code="creativeWork.editor.label" default="Editor" />
		
	</label>
	<g:select name="editor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${creativeWorkInstance?.editor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'educationalAlignment', 'error')} ">
	<label for="educationalAlignment">
		<g:message code="creativeWork.educationalAlignment.label" default="Educational Alignment" />
		
	</label>
	<g:select id="educationalAlignment" name="educationalAlignment.id" from="${com.shqiperia.entities.AlignmentObject.list()}" optionKey="id" value="${creativeWorkInstance?.educationalAlignment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'educationalUse', 'error')} ">
	<label for="educationalUse">
		<g:message code="creativeWork.educationalUse.label" default="Educational Use" />
		
	</label>
	<g:textField name="educationalUse" maxlength="250" value="${creativeWorkInstance?.educationalUse}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'encoding', 'error')} ">
	<label for="encoding">
		<g:message code="creativeWork.encoding.label" default="Encoding" />
		
	</label>
	<g:select id="encoding" name="encoding.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" value="${creativeWorkInstance?.encoding?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'genre', 'error')} ">
	<label for="genre">
		<g:message code="creativeWork.genre.label" default="Genre" />
		
	</label>
	<g:textField name="genre" maxlength="250" value="${creativeWorkInstance?.genre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'headline', 'error')} ">
	<label for="headline">
		<g:message code="creativeWork.headline.label" default="Headline" />
		
	</label>
	<g:textField name="headline" maxlength="250" value="${creativeWorkInstance?.headline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="creativeWork.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${creativeWorkInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'inLanguage', 'error')} ">
	<label for="inLanguage">
		<g:message code="creativeWork.inLanguage.label" default="In Language" />
		
	</label>
	<g:textField name="inLanguage" maxlength="250" value="${creativeWorkInstance?.inLanguage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="creativeWork.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${creativeWorkInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'interactivityType', 'error')} ">
	<label for="interactivityType">
		<g:message code="creativeWork.interactivityType.label" default="Interactivity Type" />
		
	</label>
	<g:textField name="interactivityType" maxlength="250" value="${creativeWorkInstance?.interactivityType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'isBasedOnUrl', 'error')} ">
	<label for="isBasedOnUrl">
		<g:message code="creativeWork.isBasedOnUrl.label" default="Is Based On Url" />
		
	</label>
	<g:textField name="isBasedOnUrl" maxlength="250" value="${creativeWorkInstance?.isBasedOnUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'isFamilyFriendly', 'error')} ">
	<label for="isFamilyFriendly">
		<g:message code="creativeWork.isFamilyFriendly.label" default="Is Family Friendly" />
		
	</label>
	<g:checkBox name="isFamilyFriendly" value="${creativeWorkInstance?.isFamilyFriendly}" />
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'keywords', 'error')} ">
	<label for="keywords">
		<g:message code="creativeWork.keywords.label" default="Keywords" />
		
	</label>
	<g:textField name="keywords" maxlength="250" value="${creativeWorkInstance?.keywords}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'learningResourceType', 'error')} ">
	<label for="learningResourceType">
		<g:message code="creativeWork.learningResourceType.label" default="Learning Resource Type" />
		
	</label>
	<g:textField name="learningResourceType" maxlength="250" value="${creativeWorkInstance?.learningResourceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'mentions', 'error')} ">
	<label for="mentions">
		<g:message code="creativeWork.mentions.label" default="Mentions" />
		
	</label>
	<g:textField name="mentions" maxlength="250" value="${creativeWorkInstance?.mentions}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="creativeWork.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${creativeWorkInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'offers', 'error')} ">
	<label for="offers">
		<g:message code="creativeWork.offers.label" default="Offers" />
		
	</label>
	<g:select id="offers" name="offers.id" from="${com.shqiperia.entities.Offer.list()}" optionKey="id" value="${creativeWorkInstance?.offers?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'provider', 'error')} ">
	<label for="provider">
		<g:message code="creativeWork.provider.label" default="Provider" />
		
	</label>
	<g:select id="provider" name="provider.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${creativeWorkInstance?.provider?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'publisher', 'error')} ">
	<label for="publisher">
		<g:message code="creativeWork.publisher.label" default="Publisher" />
		
	</label>
	<g:select id="publisher" name="publisher.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${creativeWorkInstance?.publisher?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'publishingPrinciples', 'error')} ">
	<label for="publishingPrinciples">
		<g:message code="creativeWork.publishingPrinciples.label" default="Publishing Principles" />
		
	</label>
	<g:textField name="publishingPrinciples" maxlength="250" value="${creativeWorkInstance?.publishingPrinciples}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'review', 'error')} ">
	<label for="review">
		<g:message code="creativeWork.review.label" default="Review" />
		
	</label>
	<g:select id="review" name="review.id" from="${com.shqiperia.entities.Review.list()}" optionKey="id" value="${creativeWorkInstance?.review?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="creativeWork.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${creativeWorkInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'sourceOrganization', 'error')} ">
	<label for="sourceOrganization">
		<g:message code="creativeWork.sourceOrganization.label" default="Source Organization" />
		
	</label>
	<g:select id="sourceOrganization" name="sourceOrganization.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${creativeWorkInstance?.sourceOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="creativeWork.text.label" default="Text" />
		
	</label>
	<g:textField name="text" maxlength="250" value="${creativeWorkInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'thumbnailUrl', 'error')} ">
	<label for="thumbnailUrl">
		<g:message code="creativeWork.thumbnailUrl.label" default="Thumbnail Url" />
		
	</label>
	<g:textField name="thumbnailUrl" maxlength="250" value="${creativeWorkInstance?.thumbnailUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'timeRequired', 'error')} ">
	<label for="timeRequired">
		<g:message code="creativeWork.timeRequired.label" default="Time Required" />
		
	</label>
	<g:textField name="timeRequired" maxlength="250" value="${creativeWorkInstance?.timeRequired}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="creativeWork.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${creativeWorkInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: creativeWorkInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="creativeWork.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${creativeWorkInstance?.url}"/>
</div>

