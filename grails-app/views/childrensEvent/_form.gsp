<%@ page import="com.shqiperia.entities.ChildrensEvent" %>



<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="childrensEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${childrensEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="childrensEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${childrensEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="childrensEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${childrensEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="childrensEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${childrensEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="childrensEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${childrensEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="childrensEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${childrensEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="childrensEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${childrensEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="childrensEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${childrensEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="childrensEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${childrensEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="childrensEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${childrensEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="childrensEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${childrensEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="childrensEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${childrensEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="childrensEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${childrensEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="childrensEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${childrensEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="childrensEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${childrensEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="childrensEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="childrensEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="childrensEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${childrensEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: childrensEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="childrensEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${childrensEventInstance?.url}"/>
</div>

