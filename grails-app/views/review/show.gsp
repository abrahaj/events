
<%@ page import="com.shqiperia.entities.Review" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'review.label', default: 'Review')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-review" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-review" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list review">
			
				<g:if test="${reviewInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="review.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${reviewInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="review.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${reviewInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="review.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${reviewInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="review.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${reviewInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="review.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${reviewInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="review.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${reviewInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="review.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${reviewInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="review.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${reviewInstance?.aggregateRating?.id}">${reviewInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="review.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${reviewInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="review.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${reviewInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.associatedMedia}">
				<li class="fieldcontain">
					<span id="associatedMedia-label" class="property-label"><g:message code="review.associatedMedia.label" default="Associated Media" /></span>
					
						<span class="property-value" aria-labelledby="associatedMedia-label"><g:link controller="mediaObject" action="show" id="${reviewInstance?.associatedMedia?.id}">${reviewInstance?.associatedMedia?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.audience}">
				<li class="fieldcontain">
					<span id="audience-label" class="property-label"><g:message code="review.audience.label" default="Audience" /></span>
					
						<span class="property-value" aria-labelledby="audience-label"><g:link controller="audience" action="show" id="${reviewInstance?.audience?.id}">${reviewInstance?.audience?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.audio}">
				<li class="fieldcontain">
					<span id="audio-label" class="property-label"><g:message code="review.audio.label" default="Audio" /></span>
					
						<span class="property-value" aria-labelledby="audio-label"><g:link controller="audioObject" action="show" id="${reviewInstance?.audio?.id}">${reviewInstance?.audio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="review.author.label" default="Author" /></span>
					
						<g:each in="${reviewInstance.author}" var="a">
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="review.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${reviewInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="review.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${reviewInstance?.citation?.id}">${reviewInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.comment}">
				<li class="fieldcontain">
					<span id="comment-label" class="property-label"><g:message code="review.comment.label" default="Comment" /></span>
					
						<span class="property-value" aria-labelledby="comment-label"><g:link controller="userComments" action="show" id="${reviewInstance?.comment?.id}">${reviewInstance?.comment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="review.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${reviewInstance?.contentLocation?.id}">${reviewInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="review.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${reviewInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="review.contributor.label" default="Contributor" /></span>
					
						<g:each in="${reviewInstance.contributor}" var="c">
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="review.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<g:each in="${reviewInstance.copyrightHolder}" var="c">
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="review.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${reviewInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="review.creator.label" default="Creator" /></span>
					
						<g:each in="${reviewInstance.creator}" var="c">
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="review.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${reviewInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="review.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${reviewInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="review.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${reviewInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="review.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${reviewInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="review.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${reviewInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="review.editor.label" default="Editor" /></span>
					
						<g:each in="${reviewInstance.editor}" var="e">
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.educationalAlignment}">
				<li class="fieldcontain">
					<span id="educationalAlignment-label" class="property-label"><g:message code="review.educationalAlignment.label" default="Educational Alignment" /></span>
					
						<span class="property-value" aria-labelledby="educationalAlignment-label"><g:link controller="alignmentObject" action="show" id="${reviewInstance?.educationalAlignment?.id}">${reviewInstance?.educationalAlignment?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="review.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${reviewInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.encoding}">
				<li class="fieldcontain">
					<span id="encoding-label" class="property-label"><g:message code="review.encoding.label" default="Encoding" /></span>
					
						<span class="property-value" aria-labelledby="encoding-label"><g:link controller="mediaObject" action="show" id="${reviewInstance?.encoding?.id}">${reviewInstance?.encoding?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="review.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${reviewInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="review.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${reviewInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="review.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${reviewInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="review.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${reviewInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="review.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${reviewInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="review.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${reviewInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="review.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${reviewInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="review.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${reviewInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.itemReviewed}">
				<li class="fieldcontain">
					<span id="itemReviewed-label" class="property-label"><g:message code="review.itemReviewed.label" default="Item Reviewed" /></span>
					
						<span class="property-value" aria-labelledby="itemReviewed-label"><g:fieldValue bean="${reviewInstance}" field="itemReviewed"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="review.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${reviewInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="review.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${reviewInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="review.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${reviewInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="review.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${reviewInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="review.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${reviewInstance?.offers?.id}">${reviewInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="review.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${reviewInstance?.provider?.id}">${reviewInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="review.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${reviewInstance?.publisher?.id}">${reviewInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="review.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${reviewInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="review.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${reviewInstance?.review?.id}">${reviewInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.reviewBody}">
				<li class="fieldcontain">
					<span id="reviewBody-label" class="property-label"><g:message code="review.reviewBody.label" default="Review Body" /></span>
					
						<span class="property-value" aria-labelledby="reviewBody-label"><g:fieldValue bean="${reviewInstance}" field="reviewBody"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.reviewRating}">
				<li class="fieldcontain">
					<span id="reviewRating-label" class="property-label"><g:message code="review.reviewRating.label" default="Review Rating" /></span>
					
						<span class="property-value" aria-labelledby="reviewRating-label"><g:link controller="rating" action="show" id="${reviewInstance?.reviewRating?.id}">${reviewInstance?.reviewRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="review.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${reviewInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="review.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${reviewInstance?.sourceOrganization?.id}">${reviewInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="review.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${reviewInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="review.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${reviewInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.timeRequired}">
				<li class="fieldcontain">
					<span id="timeRequired-label" class="property-label"><g:message code="review.timeRequired.label" default="Time Required" /></span>
					
						<span class="property-value" aria-labelledby="timeRequired-label"><g:fieldValue bean="${reviewInstance}" field="timeRequired"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="review.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${reviewInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${reviewInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="review.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${reviewInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:reviewInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${reviewInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
