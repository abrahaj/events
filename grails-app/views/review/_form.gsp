<%@ page import="com.shqiperia.entities.Review" %>



<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'about', 'error')} ">
	<label for="about">
		<g:message code="review.about.label" default="About" />
		
	</label>
	<g:textField name="about" maxlength="250" value="${reviewInstance?.about}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'accessibilityAPI', 'error')} ">
	<label for="accessibilityAPI">
		<g:message code="review.accessibilityAPI.label" default="Accessibility API" />
		
	</label>
	<g:textField name="accessibilityAPI" maxlength="250" value="${reviewInstance?.accessibilityAPI}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'accessibilityControl', 'error')} ">
	<label for="accessibilityControl">
		<g:message code="review.accessibilityControl.label" default="Accessibility Control" />
		
	</label>
	<g:textField name="accessibilityControl" maxlength="250" value="${reviewInstance?.accessibilityControl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'accessibilityFeature', 'error')} ">
	<label for="accessibilityFeature">
		<g:message code="review.accessibilityFeature.label" default="Accessibility Feature" />
		
	</label>
	<g:textField name="accessibilityFeature" maxlength="250" value="${reviewInstance?.accessibilityFeature}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'accessibilityHazard', 'error')} ">
	<label for="accessibilityHazard">
		<g:message code="review.accessibilityHazard.label" default="Accessibility Hazard" />
		
	</label>
	<g:textField name="accessibilityHazard" maxlength="250" value="${reviewInstance?.accessibilityHazard}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'accountablePerson', 'error')} ">
	<label for="accountablePerson">
		<g:message code="review.accountablePerson.label" default="Accountable Person" />
		
	</label>
	<g:select name="accountablePerson" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.accountablePerson*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="review.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${reviewInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="review.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${reviewInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="review.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${reviewInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'alternativeHeadline', 'error')} ">
	<label for="alternativeHeadline">
		<g:message code="review.alternativeHeadline.label" default="Alternative Headline" />
		
	</label>
	<g:textField name="alternativeHeadline" maxlength="250" value="${reviewInstance?.alternativeHeadline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'associatedMedia', 'error')} ">
	<label for="associatedMedia">
		<g:message code="review.associatedMedia.label" default="Associated Media" />
		
	</label>
	<g:select id="associatedMedia" name="associatedMedia.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" value="${reviewInstance?.associatedMedia?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'audience', 'error')} ">
	<label for="audience">
		<g:message code="review.audience.label" default="Audience" />
		
	</label>
	<g:select id="audience" name="audience.id" from="${com.shqiperia.entities.Audience.list()}" optionKey="id" value="${reviewInstance?.audience?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'audio', 'error')} ">
	<label for="audio">
		<g:message code="review.audio.label" default="Audio" />
		
	</label>
	<g:select id="audio" name="audio.id" from="${com.shqiperia.entities.AudioObject.list()}" optionKey="id" value="${reviewInstance?.audio?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="review.author.label" default="Author" />
		
	</label>
	<g:select name="author" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.author*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="review.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${reviewInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'citation', 'error')} required">
	<label for="citation">
		<g:message code="review.citation.label" default="Citation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="citation" name="citation.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${reviewInstance?.citation?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'comment', 'error')} ">
	<label for="comment">
		<g:message code="review.comment.label" default="Comment" />
		
	</label>
	<g:select id="comment" name="comment.id" from="${com.shqiperia.entities.UserComments.list()}" optionKey="id" value="${reviewInstance?.comment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'contentLocation', 'error')} ">
	<label for="contentLocation">
		<g:message code="review.contentLocation.label" default="Content Location" />
		
	</label>
	<g:select id="contentLocation" name="contentLocation.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${reviewInstance?.contentLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'contentRating', 'error')} ">
	<label for="contentRating">
		<g:message code="review.contentRating.label" default="Content Rating" />
		
	</label>
	<g:textField name="contentRating" maxlength="250" value="${reviewInstance?.contentRating}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'contributor', 'error')} ">
	<label for="contributor">
		<g:message code="review.contributor.label" default="Contributor" />
		
	</label>
	<g:select name="contributor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.contributor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'copyrightHolder', 'error')} ">
	<label for="copyrightHolder">
		<g:message code="review.copyrightHolder.label" default="Copyright Holder" />
		
	</label>
	<g:select name="copyrightHolder" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.copyrightHolder*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'copyrightYear', 'error')} ">
	<label for="copyrightYear">
		<g:message code="review.copyrightYear.label" default="Copyright Year" />
		
	</label>
	<g:field name="copyrightYear" type="number" value="${reviewInstance.copyrightYear}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="review.creator.label" default="Creator" />
		
	</label>
	<g:select name="creator" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.creator*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'dateModified', 'error')} ">
	<label for="dateModified">
		<g:message code="review.dateModified.label" default="Date Modified" />
		
	</label>
	<g:datePicker name="dateModified" precision="day"  value="${reviewInstance?.dateModified}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'datePublished', 'error')} ">
	<label for="datePublished">
		<g:message code="review.datePublished.label" default="Date Published" />
		
	</label>
	<g:datePicker name="datePublished" precision="day"  value="${reviewInstance?.datePublished}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="review.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${reviewInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'discussionUrl', 'error')} ">
	<label for="discussionUrl">
		<g:message code="review.discussionUrl.label" default="Discussion Url" />
		
	</label>
	<g:textField name="discussionUrl" maxlength="250" value="${reviewInstance?.discussionUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'editor', 'error')} ">
	<label for="editor">
		<g:message code="review.editor.label" default="Editor" />
		
	</label>
	<g:select name="editor" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${reviewInstance?.editor*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'educationalAlignment', 'error')} ">
	<label for="educationalAlignment">
		<g:message code="review.educationalAlignment.label" default="Educational Alignment" />
		
	</label>
	<g:select id="educationalAlignment" name="educationalAlignment.id" from="${com.shqiperia.entities.AlignmentObject.list()}" optionKey="id" value="${reviewInstance?.educationalAlignment?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'educationalUse', 'error')} ">
	<label for="educationalUse">
		<g:message code="review.educationalUse.label" default="Educational Use" />
		
	</label>
	<g:textField name="educationalUse" maxlength="250" value="${reviewInstance?.educationalUse}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'encoding', 'error')} ">
	<label for="encoding">
		<g:message code="review.encoding.label" default="Encoding" />
		
	</label>
	<g:select id="encoding" name="encoding.id" from="${com.shqiperia.entities.MediaObject.list()}" optionKey="id" value="${reviewInstance?.encoding?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'genre', 'error')} ">
	<label for="genre">
		<g:message code="review.genre.label" default="Genre" />
		
	</label>
	<g:textField name="genre" maxlength="250" value="${reviewInstance?.genre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'headline', 'error')} ">
	<label for="headline">
		<g:message code="review.headline.label" default="Headline" />
		
	</label>
	<g:textField name="headline" maxlength="250" value="${reviewInstance?.headline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="review.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${reviewInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'inLanguage', 'error')} ">
	<label for="inLanguage">
		<g:message code="review.inLanguage.label" default="In Language" />
		
	</label>
	<g:textField name="inLanguage" maxlength="250" value="${reviewInstance?.inLanguage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="review.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${reviewInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'interactivityType', 'error')} ">
	<label for="interactivityType">
		<g:message code="review.interactivityType.label" default="Interactivity Type" />
		
	</label>
	<g:textField name="interactivityType" maxlength="250" value="${reviewInstance?.interactivityType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'isBasedOnUrl', 'error')} ">
	<label for="isBasedOnUrl">
		<g:message code="review.isBasedOnUrl.label" default="Is Based On Url" />
		
	</label>
	<g:textField name="isBasedOnUrl" maxlength="250" value="${reviewInstance?.isBasedOnUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'isFamilyFriendly', 'error')} ">
	<label for="isFamilyFriendly">
		<g:message code="review.isFamilyFriendly.label" default="Is Family Friendly" />
		
	</label>
	<g:checkBox name="isFamilyFriendly" value="${reviewInstance?.isFamilyFriendly}" />
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'itemReviewed', 'error')} ">
	<label for="itemReviewed">
		<g:message code="review.itemReviewed.label" default="Item Reviewed" />
		
	</label>
	<g:textField name="itemReviewed" maxlength="250" value="${reviewInstance?.itemReviewed}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'keywords', 'error')} ">
	<label for="keywords">
		<g:message code="review.keywords.label" default="Keywords" />
		
	</label>
	<g:textField name="keywords" maxlength="250" value="${reviewInstance?.keywords}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'learningResourceType', 'error')} ">
	<label for="learningResourceType">
		<g:message code="review.learningResourceType.label" default="Learning Resource Type" />
		
	</label>
	<g:textField name="learningResourceType" maxlength="250" value="${reviewInstance?.learningResourceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'mentions', 'error')} ">
	<label for="mentions">
		<g:message code="review.mentions.label" default="Mentions" />
		
	</label>
	<g:textField name="mentions" maxlength="250" value="${reviewInstance?.mentions}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="review.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${reviewInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'offers', 'error')} ">
	<label for="offers">
		<g:message code="review.offers.label" default="Offers" />
		
	</label>
	<g:select id="offers" name="offers.id" from="${com.shqiperia.entities.Offer.list()}" optionKey="id" value="${reviewInstance?.offers?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'provider', 'error')} ">
	<label for="provider">
		<g:message code="review.provider.label" default="Provider" />
		
	</label>
	<g:select id="provider" name="provider.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${reviewInstance?.provider?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'publisher', 'error')} ">
	<label for="publisher">
		<g:message code="review.publisher.label" default="Publisher" />
		
	</label>
	<g:select id="publisher" name="publisher.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${reviewInstance?.publisher?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'publishingPrinciples', 'error')} ">
	<label for="publishingPrinciples">
		<g:message code="review.publishingPrinciples.label" default="Publishing Principles" />
		
	</label>
	<g:textField name="publishingPrinciples" maxlength="250" value="${reviewInstance?.publishingPrinciples}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'review', 'error')} required">
	<label for="review">
		<g:message code="review.review.label" default="Review" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="review" name="review.id" from="${com.shqiperia.entities.Review.list()}" optionKey="id" required="" value="${reviewInstance?.review?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'reviewBody', 'error')} ">
	<label for="reviewBody">
		<g:message code="review.reviewBody.label" default="Review Body" />
		
	</label>
	<g:textField name="reviewBody" maxlength="250" value="${reviewInstance?.reviewBody}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'reviewRating', 'error')} ">
	<label for="reviewRating">
		<g:message code="review.reviewRating.label" default="Review Rating" />
		
	</label>
	<g:select id="reviewRating" name="reviewRating.id" from="${com.shqiperia.entities.Rating.list()}" optionKey="id" value="${reviewInstance?.reviewRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="review.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${reviewInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'sourceOrganization', 'error')} ">
	<label for="sourceOrganization">
		<g:message code="review.sourceOrganization.label" default="Source Organization" />
		
	</label>
	<g:select id="sourceOrganization" name="sourceOrganization.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${reviewInstance?.sourceOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="review.text.label" default="Text" />
		
	</label>
	<g:textField name="text" maxlength="250" value="${reviewInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'thumbnailUrl', 'error')} ">
	<label for="thumbnailUrl">
		<g:message code="review.thumbnailUrl.label" default="Thumbnail Url" />
		
	</label>
	<g:textField name="thumbnailUrl" maxlength="250" value="${reviewInstance?.thumbnailUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'timeRequired', 'error')} ">
	<label for="timeRequired">
		<g:message code="review.timeRequired.label" default="Time Required" />
		
	</label>
	<g:textField name="timeRequired" maxlength="250" value="${reviewInstance?.timeRequired}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="review.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${reviewInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: reviewInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="review.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${reviewInstance?.url}"/>
</div>

