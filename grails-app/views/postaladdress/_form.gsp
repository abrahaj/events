<%@ page import="com.shqiperia.entities.Postaladdress" %>



<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="postaladdress.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" maxlength="250" value="${postaladdressInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="postaladdress.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${postaladdressInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'addressLocality', 'error')} ">
	<label for="addressLocality">
		<g:message code="postaladdress.addressLocality.label" default="Address Locality" />
		
	</label>
	<g:textField name="addressLocality" maxlength="250" value="${postaladdressInstance?.addressLocality}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'addressRegion', 'error')} ">
	<label for="addressRegion">
		<g:message code="postaladdress.addressRegion.label" default="Address Region" />
		
	</label>
	<g:textField name="addressRegion" maxlength="250" value="${postaladdressInstance?.addressRegion}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'contactType', 'error')} ">
	<label for="contactType">
		<g:message code="postaladdress.contactType.label" default="Contact Type" />
		
	</label>
	<g:textField name="contactType" maxlength="250" value="${postaladdressInstance?.contactType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="postaladdress.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${postaladdressInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="postaladdress.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${postaladdressInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="postaladdress.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${postaladdressInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="postaladdress.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${postaladdressInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'postOfficeBoxNumber', 'error')} ">
	<label for="postOfficeBoxNumber">
		<g:message code="postaladdress.postOfficeBoxNumber.label" default="Post Office Box Number" />
		
	</label>
	<g:textField name="postOfficeBoxNumber" maxlength="250" value="${postaladdressInstance?.postOfficeBoxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'postalCode', 'error')} ">
	<label for="postalCode">
		<g:message code="postaladdress.postalCode.label" default="Postal Code" />
		
	</label>
	<g:textField name="postalCode" maxlength="250" value="${postaladdressInstance?.postalCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="postaladdress.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${postaladdressInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'streetAddress', 'error')} ">
	<label for="streetAddress">
		<g:message code="postaladdress.streetAddress.label" default="Street Address" />
		
	</label>
	<g:textField name="streetAddress" maxlength="250" value="${postaladdressInstance?.streetAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="postaladdress.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${postaladdressInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: postaladdressInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="postaladdress.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${postaladdressInstance?.url}"/>
</div>

