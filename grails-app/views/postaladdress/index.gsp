
<%@ page import="com.shqiperia.entities.Postaladdress" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'postaladdress.label', default: 'Postaladdress')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-postaladdress" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-postaladdress" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="email" title="${message(code: 'postaladdress.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'postaladdress.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="addressLocality" title="${message(code: 'postaladdress.addressLocality.label', default: 'Address Locality')}" />
					
						<g:sortableColumn property="addressRegion" title="${message(code: 'postaladdress.addressRegion.label', default: 'Address Region')}" />
					
						<g:sortableColumn property="contactType" title="${message(code: 'postaladdress.contactType.label', default: 'Contact Type')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'postaladdress.description.label', default: 'Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${postaladdressInstanceList}" status="i" var="postaladdressInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${postaladdressInstance.id}">${fieldValue(bean: postaladdressInstance, field: "email")}</g:link></td>
					
						<td>${fieldValue(bean: postaladdressInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: postaladdressInstance, field: "addressLocality")}</td>
					
						<td>${fieldValue(bean: postaladdressInstance, field: "addressRegion")}</td>
					
						<td>${fieldValue(bean: postaladdressInstance, field: "contactType")}</td>
					
						<td>${fieldValue(bean: postaladdressInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${postaladdressInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
