
<%@ page import="com.shqiperia.entities.Postaladdress" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'postaladdress.label', default: 'Postaladdress')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-postaladdress" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-postaladdress" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list postaladdress">
			
				<g:if test="${postaladdressInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="postaladdress.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${postaladdressInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="postaladdress.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${postaladdressInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.addressLocality}">
				<li class="fieldcontain">
					<span id="addressLocality-label" class="property-label"><g:message code="postaladdress.addressLocality.label" default="Address Locality" /></span>
					
						<span class="property-value" aria-labelledby="addressLocality-label"><g:fieldValue bean="${postaladdressInstance}" field="addressLocality"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.addressRegion}">
				<li class="fieldcontain">
					<span id="addressRegion-label" class="property-label"><g:message code="postaladdress.addressRegion.label" default="Address Region" /></span>
					
						<span class="property-value" aria-labelledby="addressRegion-label"><g:fieldValue bean="${postaladdressInstance}" field="addressRegion"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.contactType}">
				<li class="fieldcontain">
					<span id="contactType-label" class="property-label"><g:message code="postaladdress.contactType.label" default="Contact Type" /></span>
					
						<span class="property-value" aria-labelledby="contactType-label"><g:fieldValue bean="${postaladdressInstance}" field="contactType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="postaladdress.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${postaladdressInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="postaladdress.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${postaladdressInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="postaladdress.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${postaladdressInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="postaladdress.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${postaladdressInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.postOfficeBoxNumber}">
				<li class="fieldcontain">
					<span id="postOfficeBoxNumber-label" class="property-label"><g:message code="postaladdress.postOfficeBoxNumber.label" default="Post Office Box Number" /></span>
					
						<span class="property-value" aria-labelledby="postOfficeBoxNumber-label"><g:fieldValue bean="${postaladdressInstance}" field="postOfficeBoxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.postalCode}">
				<li class="fieldcontain">
					<span id="postalCode-label" class="property-label"><g:message code="postaladdress.postalCode.label" default="Postal Code" /></span>
					
						<span class="property-value" aria-labelledby="postalCode-label"><g:fieldValue bean="${postaladdressInstance}" field="postalCode"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="postaladdress.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${postaladdressInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.streetAddress}">
				<li class="fieldcontain">
					<span id="streetAddress-label" class="property-label"><g:message code="postaladdress.streetAddress.label" default="Street Address" /></span>
					
						<span class="property-value" aria-labelledby="streetAddress-label"><g:fieldValue bean="${postaladdressInstance}" field="streetAddress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="postaladdress.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${postaladdressInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${postaladdressInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="postaladdress.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${postaladdressInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:postaladdressInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${postaladdressInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
