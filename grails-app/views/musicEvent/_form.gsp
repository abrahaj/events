<%@ page import="com.shqiperia.entities.MusicEvent" %>



<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="musicEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${musicEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="musicEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${musicEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="musicEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${musicEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="musicEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${musicEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="musicEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${musicEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="musicEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${musicEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="musicEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${musicEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="musicEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${musicEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="musicEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${musicEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="musicEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${musicEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="musicEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${musicEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="musicEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${musicEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="musicEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${musicEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="musicEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${musicEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="musicEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${musicEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="musicEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="musicEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="musicEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${musicEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: musicEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="musicEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${musicEventInstance?.url}"/>
</div>

