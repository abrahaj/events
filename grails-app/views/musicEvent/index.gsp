
<%@ page import="com.shqiperia.entities.MusicEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'musicEvent.label', default: 'MusicEvent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-musicEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-musicEvent" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'musicEvent.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'musicEvent.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'musicEvent.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'musicEvent.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="doorTime" title="${message(code: 'musicEvent.doorTime.label', default: 'Door Time')}" />
					
						<g:sortableColumn property="duration" title="${message(code: 'musicEvent.duration.label', default: 'Duration')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${musicEventInstanceList}" status="i" var="musicEventInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${musicEventInstance.id}">${fieldValue(bean: musicEventInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: musicEventInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: musicEventInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: musicEventInstance, field: "description")}</td>
					
						<td><g:formatDate date="${musicEventInstance.doorTime}" /></td>
					
						<td><g:formatDate date="${musicEventInstance.duration}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${musicEventInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
