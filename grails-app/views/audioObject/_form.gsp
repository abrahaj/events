<%@ page import="com.shqiperia.entities.AudioObject" %>



<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'about', 'error')} ">
	<label for="about">
		<g:message code="audioObject.about.label" default="About" />
		
	</label>
	<g:textField name="about" maxlength="250" value="${audioObjectInstance?.about}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'accessibilityAPI', 'error')} ">
	<label for="accessibilityAPI">
		<g:message code="audioObject.accessibilityAPI.label" default="Accessibility API" />
		
	</label>
	<g:textField name="accessibilityAPI" maxlength="250" value="${audioObjectInstance?.accessibilityAPI}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'accessibilityControl', 'error')} ">
	<label for="accessibilityControl">
		<g:message code="audioObject.accessibilityControl.label" default="Accessibility Control" />
		
	</label>
	<g:textField name="accessibilityControl" maxlength="250" value="${audioObjectInstance?.accessibilityControl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'accessibilityFeature', 'error')} ">
	<label for="accessibilityFeature">
		<g:message code="audioObject.accessibilityFeature.label" default="Accessibility Feature" />
		
	</label>
	<g:textField name="accessibilityFeature" maxlength="250" value="${audioObjectInstance?.accessibilityFeature}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'accessibilityHazard', 'error')} ">
	<label for="accessibilityHazard">
		<g:message code="audioObject.accessibilityHazard.label" default="Accessibility Hazard" />
		
	</label>
	<g:textField name="accessibilityHazard" maxlength="250" value="${audioObjectInstance?.accessibilityHazard}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'accountablePerson', 'error')} ">
	<label for="accountablePerson">
		<g:message code="audioObject.accountablePerson.label" default="Accountable Person" />
		
	</label>
	<g:select name="accountablePerson" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${audioObjectInstance?.accountablePerson*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="audioObject.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${audioObjectInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="audioObject.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${audioObjectInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="audioObject.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${audioObjectInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'alternativeHeadline', 'error')} ">
	<label for="alternativeHeadline">
		<g:message code="audioObject.alternativeHeadline.label" default="Alternative Headline" />
		
	</label>
	<g:textField name="alternativeHeadline" maxlength="250" value="${audioObjectInstance?.alternativeHeadline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="audioObject.author.label" default="Author" />
		
	</label>
	<g:select id="author" name="author.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.author?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="audioObject.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${audioObjectInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'bitrate', 'error')} ">
	<label for="bitrate">
		<g:message code="audioObject.bitrate.label" default="Bitrate" />
		
	</label>
	<g:textField name="bitrate" maxlength="250" value="${audioObjectInstance?.bitrate}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'citation', 'error')} required">
	<label for="citation">
		<g:message code="audioObject.citation.label" default="Citation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="citation" name="citation.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${audioObjectInstance?.citation?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'contentLocation', 'error')} ">
	<label for="contentLocation">
		<g:message code="audioObject.contentLocation.label" default="Content Location" />
		
	</label>
	<g:select id="contentLocation" name="contentLocation.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${audioObjectInstance?.contentLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'contentRating', 'error')} ">
	<label for="contentRating">
		<g:message code="audioObject.contentRating.label" default="Content Rating" />
		
	</label>
	<g:textField name="contentRating" maxlength="250" value="${audioObjectInstance?.contentRating}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'contentSize', 'error')} ">
	<label for="contentSize">
		<g:message code="audioObject.contentSize.label" default="Content Size" />
		
	</label>
	<g:textField name="contentSize" maxlength="250" value="${audioObjectInstance?.contentSize}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'contentUrl', 'error')} ">
	<label for="contentUrl">
		<g:message code="audioObject.contentUrl.label" default="Content Url" />
		
	</label>
	<g:textField name="contentUrl" maxlength="250" value="${audioObjectInstance?.contentUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'contributor', 'error')} ">
	<label for="contributor">
		<g:message code="audioObject.contributor.label" default="Contributor" />
		
	</label>
	<g:select id="contributor" name="contributor.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.contributor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'copyrightHolder', 'error')} ">
	<label for="copyrightHolder">
		<g:message code="audioObject.copyrightHolder.label" default="Copyright Holder" />
		
	</label>
	<g:select id="copyrightHolder" name="copyrightHolder.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.copyrightHolder?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'copyrightYear', 'error')} ">
	<label for="copyrightYear">
		<g:message code="audioObject.copyrightYear.label" default="Copyright Year" />
		
	</label>
	<g:field name="copyrightYear" type="number" value="${audioObjectInstance.copyrightYear}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="audioObject.creator.label" default="Creator" />
		
	</label>
	<g:select id="creator" name="creator.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.creator?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'dateModified', 'error')} ">
	<label for="dateModified">
		<g:message code="audioObject.dateModified.label" default="Date Modified" />
		
	</label>
	<g:datePicker name="dateModified" precision="day"  value="${audioObjectInstance?.dateModified}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'datePublished', 'error')} ">
	<label for="datePublished">
		<g:message code="audioObject.datePublished.label" default="Date Published" />
		
	</label>
	<g:datePicker name="datePublished" precision="day"  value="${audioObjectInstance?.datePublished}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="audioObject.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${audioObjectInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'discussionUrl', 'error')} ">
	<label for="discussionUrl">
		<g:message code="audioObject.discussionUrl.label" default="Discussion Url" />
		
	</label>
	<g:textField name="discussionUrl" maxlength="250" value="${audioObjectInstance?.discussionUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="audioObject.duration.label" default="Duration" />
		
	</label>
	<g:textField name="duration" maxlength="250" value="${audioObjectInstance?.duration}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'editor', 'error')} ">
	<label for="editor">
		<g:message code="audioObject.editor.label" default="Editor" />
		
	</label>
	<g:select id="editor" name="editor.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.editor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'educationalUse', 'error')} ">
	<label for="educationalUse">
		<g:message code="audioObject.educationalUse.label" default="Educational Use" />
		
	</label>
	<g:textField name="educationalUse" maxlength="250" value="${audioObjectInstance?.educationalUse}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'embedUrl', 'error')} ">
	<label for="embedUrl">
		<g:message code="audioObject.embedUrl.label" default="Embed Url" />
		
	</label>
	<g:textField name="embedUrl" maxlength="250" value="${audioObjectInstance?.embedUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'encodesCreativeWork', 'error')} required">
	<label for="encodesCreativeWork">
		<g:message code="audioObject.encodesCreativeWork.label" default="Encodes Creative Work" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="encodesCreativeWork" name="encodesCreativeWork.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${audioObjectInstance?.encodesCreativeWork?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'encodingFormat', 'error')} ">
	<label for="encodingFormat">
		<g:message code="audioObject.encodingFormat.label" default="Encoding Format" />
		
	</label>
	<g:textField name="encodingFormat" maxlength="250" value="${audioObjectInstance?.encodingFormat}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'expires', 'error')} ">
	<label for="expires">
		<g:message code="audioObject.expires.label" default="Expires" />
		
	</label>
	<g:datePicker name="expires" precision="day"  value="${audioObjectInstance?.expires}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'genre', 'error')} ">
	<label for="genre">
		<g:message code="audioObject.genre.label" default="Genre" />
		
	</label>
	<g:textField name="genre" maxlength="250" value="${audioObjectInstance?.genre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'headline', 'error')} ">
	<label for="headline">
		<g:message code="audioObject.headline.label" default="Headline" />
		
	</label>
	<g:textField name="headline" maxlength="250" value="${audioObjectInstance?.headline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="audioObject.height.label" default="Height" />
		
	</label>
	<g:textField name="height" maxlength="250" value="${audioObjectInstance?.height}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="audioObject.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${audioObjectInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'inLanguage', 'error')} ">
	<label for="inLanguage">
		<g:message code="audioObject.inLanguage.label" default="In Language" />
		
	</label>
	<g:textField name="inLanguage" maxlength="250" value="${audioObjectInstance?.inLanguage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="audioObject.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${audioObjectInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'interactivityType', 'error')} ">
	<label for="interactivityType">
		<g:message code="audioObject.interactivityType.label" default="Interactivity Type" />
		
	</label>
	<g:textField name="interactivityType" maxlength="250" value="${audioObjectInstance?.interactivityType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'isBasedOnUrl', 'error')} ">
	<label for="isBasedOnUrl">
		<g:message code="audioObject.isBasedOnUrl.label" default="Is Based On Url" />
		
	</label>
	<g:textField name="isBasedOnUrl" maxlength="250" value="${audioObjectInstance?.isBasedOnUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'isFamilyFriendly', 'error')} ">
	<label for="isFamilyFriendly">
		<g:message code="audioObject.isFamilyFriendly.label" default="Is Family Friendly" />
		
	</label>
	<g:checkBox name="isFamilyFriendly" value="${audioObjectInstance?.isFamilyFriendly}" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'keywords', 'error')} ">
	<label for="keywords">
		<g:message code="audioObject.keywords.label" default="Keywords" />
		
	</label>
	<g:textField name="keywords" maxlength="250" value="${audioObjectInstance?.keywords}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'learningResourceType', 'error')} ">
	<label for="learningResourceType">
		<g:message code="audioObject.learningResourceType.label" default="Learning Resource Type" />
		
	</label>
	<g:textField name="learningResourceType" maxlength="250" value="${audioObjectInstance?.learningResourceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'mentions', 'error')} ">
	<label for="mentions">
		<g:message code="audioObject.mentions.label" default="Mentions" />
		
	</label>
	<g:textField name="mentions" maxlength="250" value="${audioObjectInstance?.mentions}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="audioObject.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${audioObjectInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'offers', 'error')} ">
	<label for="offers">
		<g:message code="audioObject.offers.label" default="Offers" />
		
	</label>
	<g:select id="offers" name="offers.id" from="${com.shqiperia.entities.Offer.list()}" optionKey="id" value="${audioObjectInstance?.offers?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'playerType', 'error')} ">
	<label for="playerType">
		<g:message code="audioObject.playerType.label" default="Player Type" />
		
	</label>
	<g:textField name="playerType" maxlength="250" value="${audioObjectInstance?.playerType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'productionCompany', 'error')} ">
	<label for="productionCompany">
		<g:message code="audioObject.productionCompany.label" default="Production Company" />
		
	</label>
	<g:select id="productionCompany" name="productionCompany.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${audioObjectInstance?.productionCompany?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'provider', 'error')} ">
	<label for="provider">
		<g:message code="audioObject.provider.label" default="Provider" />
		
	</label>
	<g:select id="provider" name="provider.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${audioObjectInstance?.provider?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'publication', 'error')} ">
	<label for="publication">
		<g:message code="audioObject.publication.label" default="Publication" />
		
	</label>
	<g:select id="publication" name="publication.id" from="${com.shqiperia.entities.PublicationEvent.list()}" optionKey="id" value="${audioObjectInstance?.publication?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'publisher', 'error')} ">
	<label for="publisher">
		<g:message code="audioObject.publisher.label" default="Publisher" />
		
	</label>
	<g:select id="publisher" name="publisher.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${audioObjectInstance?.publisher?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'publishingPrinciples', 'error')} ">
	<label for="publishingPrinciples">
		<g:message code="audioObject.publishingPrinciples.label" default="Publishing Principles" />
		
	</label>
	<g:textField name="publishingPrinciples" maxlength="250" value="${audioObjectInstance?.publishingPrinciples}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'regionsAllowed', 'error')} ">
	<label for="regionsAllowed">
		<g:message code="audioObject.regionsAllowed.label" default="Regions Allowed" />
		
	</label>
	<g:select id="regionsAllowed" name="regionsAllowed.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${audioObjectInstance?.regionsAllowed?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'requiresSubscription', 'error')} ">
	<label for="requiresSubscription">
		<g:message code="audioObject.requiresSubscription.label" default="Requires Subscription" />
		
	</label>
	<g:checkBox name="requiresSubscription" value="${audioObjectInstance?.requiresSubscription}" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'review', 'error')} ">
	<label for="review">
		<g:message code="audioObject.review.label" default="Review" />
		
	</label>
	<g:select id="review" name="review.id" from="${com.shqiperia.entities.Review.list()}" optionKey="id" value="${audioObjectInstance?.review?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="audioObject.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${audioObjectInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'sourceOrganization', 'error')} ">
	<label for="sourceOrganization">
		<g:message code="audioObject.sourceOrganization.label" default="Source Organization" />
		
	</label>
	<g:select id="sourceOrganization" name="sourceOrganization.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${audioObjectInstance?.sourceOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="audioObject.text.label" default="Text" />
		
	</label>
	<g:textField name="text" maxlength="250" value="${audioObjectInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'thumbnailUrl', 'error')} ">
	<label for="thumbnailUrl">
		<g:message code="audioObject.thumbnailUrl.label" default="Thumbnail Url" />
		
	</label>
	<g:textField name="thumbnailUrl" maxlength="250" value="${audioObjectInstance?.thumbnailUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'transcript', 'error')} ">
	<label for="transcript">
		<g:message code="audioObject.transcript.label" default="Transcript" />
		
	</label>
	<g:textField name="transcript" maxlength="250" value="${audioObjectInstance?.transcript}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="audioObject.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${audioObjectInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'uploadDate', 'error')} ">
	<label for="uploadDate">
		<g:message code="audioObject.uploadDate.label" default="Upload Date" />
		
	</label>
	<g:datePicker name="uploadDate" precision="day"  value="${audioObjectInstance?.uploadDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="audioObject.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${audioObjectInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: audioObjectInstance, field: 'width', 'error')} ">
	<label for="width">
		<g:message code="audioObject.width.label" default="Width" />
		
	</label>
	<g:textField name="width" maxlength="250" value="${audioObjectInstance?.width}"/>
</div>

