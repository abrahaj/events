
<%@ page import="com.shqiperia.entities.AudioObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'audioObject.label', default: 'AudioObject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-audioObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-audioObject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list audioObject">
			
				<g:if test="${audioObjectInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="audioObject.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${audioObjectInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="audioObject.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${audioObjectInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="audioObject.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${audioObjectInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="audioObject.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${audioObjectInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="audioObject.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${audioObjectInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="audioObject.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${audioObjectInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="audioObject.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${audioObjectInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="audioObject.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${audioObjectInstance?.aggregateRating?.id}">${audioObjectInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="audioObject.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${audioObjectInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="audioObject.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${audioObjectInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="audioObject.author.label" default="Author" /></span>
					
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${audioObjectInstance?.author?.id}">${audioObjectInstance?.author?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="audioObject.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${audioObjectInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.bitrate}">
				<li class="fieldcontain">
					<span id="bitrate-label" class="property-label"><g:message code="audioObject.bitrate.label" default="Bitrate" /></span>
					
						<span class="property-value" aria-labelledby="bitrate-label"><g:fieldValue bean="${audioObjectInstance}" field="bitrate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="audioObject.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${audioObjectInstance?.citation?.id}">${audioObjectInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="audioObject.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${audioObjectInstance?.contentLocation?.id}">${audioObjectInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="audioObject.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${audioObjectInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.contentSize}">
				<li class="fieldcontain">
					<span id="contentSize-label" class="property-label"><g:message code="audioObject.contentSize.label" default="Content Size" /></span>
					
						<span class="property-value" aria-labelledby="contentSize-label"><g:fieldValue bean="${audioObjectInstance}" field="contentSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.contentUrl}">
				<li class="fieldcontain">
					<span id="contentUrl-label" class="property-label"><g:message code="audioObject.contentUrl.label" default="Content Url" /></span>
					
						<span class="property-value" aria-labelledby="contentUrl-label"><g:fieldValue bean="${audioObjectInstance}" field="contentUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="audioObject.contributor.label" default="Contributor" /></span>
					
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${audioObjectInstance?.contributor?.id}">${audioObjectInstance?.contributor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="audioObject.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${audioObjectInstance?.copyrightHolder?.id}">${audioObjectInstance?.copyrightHolder?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="audioObject.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${audioObjectInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="audioObject.creator.label" default="Creator" /></span>
					
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${audioObjectInstance?.creator?.id}">${audioObjectInstance?.creator?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="audioObject.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${audioObjectInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="audioObject.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${audioObjectInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="audioObject.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${audioObjectInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="audioObject.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${audioObjectInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="audioObject.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${audioObjectInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="audioObject.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:fieldValue bean="${audioObjectInstance}" field="duration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="audioObject.editor.label" default="Editor" /></span>
					
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${audioObjectInstance?.editor?.id}">${audioObjectInstance?.editor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="audioObject.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${audioObjectInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.embedUrl}">
				<li class="fieldcontain">
					<span id="embedUrl-label" class="property-label"><g:message code="audioObject.embedUrl.label" default="Embed Url" /></span>
					
						<span class="property-value" aria-labelledby="embedUrl-label"><g:fieldValue bean="${audioObjectInstance}" field="embedUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.encodesCreativeWork}">
				<li class="fieldcontain">
					<span id="encodesCreativeWork-label" class="property-label"><g:message code="audioObject.encodesCreativeWork.label" default="Encodes Creative Work" /></span>
					
						<span class="property-value" aria-labelledby="encodesCreativeWork-label"><g:link controller="creativeWork" action="show" id="${audioObjectInstance?.encodesCreativeWork?.id}">${audioObjectInstance?.encodesCreativeWork?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.encodingFormat}">
				<li class="fieldcontain">
					<span id="encodingFormat-label" class="property-label"><g:message code="audioObject.encodingFormat.label" default="Encoding Format" /></span>
					
						<span class="property-value" aria-labelledby="encodingFormat-label"><g:fieldValue bean="${audioObjectInstance}" field="encodingFormat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.expires}">
				<li class="fieldcontain">
					<span id="expires-label" class="property-label"><g:message code="audioObject.expires.label" default="Expires" /></span>
					
						<span class="property-value" aria-labelledby="expires-label"><g:formatDate date="${audioObjectInstance?.expires}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="audioObject.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${audioObjectInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="audioObject.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${audioObjectInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="audioObject.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${audioObjectInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="audioObject.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${audioObjectInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="audioObject.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${audioObjectInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="audioObject.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${audioObjectInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="audioObject.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${audioObjectInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="audioObject.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${audioObjectInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="audioObject.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${audioObjectInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="audioObject.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${audioObjectInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="audioObject.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${audioObjectInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="audioObject.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${audioObjectInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="audioObject.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${audioObjectInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="audioObject.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${audioObjectInstance?.offers?.id}">${audioObjectInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.playerType}">
				<li class="fieldcontain">
					<span id="playerType-label" class="property-label"><g:message code="audioObject.playerType.label" default="Player Type" /></span>
					
						<span class="property-value" aria-labelledby="playerType-label"><g:fieldValue bean="${audioObjectInstance}" field="playerType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.productionCompany}">
				<li class="fieldcontain">
					<span id="productionCompany-label" class="property-label"><g:message code="audioObject.productionCompany.label" default="Production Company" /></span>
					
						<span class="property-value" aria-labelledby="productionCompany-label"><g:link controller="organization" action="show" id="${audioObjectInstance?.productionCompany?.id}">${audioObjectInstance?.productionCompany?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="audioObject.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${audioObjectInstance?.provider?.id}">${audioObjectInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.publication}">
				<li class="fieldcontain">
					<span id="publication-label" class="property-label"><g:message code="audioObject.publication.label" default="Publication" /></span>
					
						<span class="property-value" aria-labelledby="publication-label"><g:link controller="publicationEvent" action="show" id="${audioObjectInstance?.publication?.id}">${audioObjectInstance?.publication?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="audioObject.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${audioObjectInstance?.publisher?.id}">${audioObjectInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="audioObject.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${audioObjectInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.regionsAllowed}">
				<li class="fieldcontain">
					<span id="regionsAllowed-label" class="property-label"><g:message code="audioObject.regionsAllowed.label" default="Regions Allowed" /></span>
					
						<span class="property-value" aria-labelledby="regionsAllowed-label"><g:link controller="place" action="show" id="${audioObjectInstance?.regionsAllowed?.id}">${audioObjectInstance?.regionsAllowed?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.requiresSubscription}">
				<li class="fieldcontain">
					<span id="requiresSubscription-label" class="property-label"><g:message code="audioObject.requiresSubscription.label" default="Requires Subscription" /></span>
					
						<span class="property-value" aria-labelledby="requiresSubscription-label"><g:formatBoolean boolean="${audioObjectInstance?.requiresSubscription}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="audioObject.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${audioObjectInstance?.review?.id}">${audioObjectInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="audioObject.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${audioObjectInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="audioObject.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${audioObjectInstance?.sourceOrganization?.id}">${audioObjectInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="audioObject.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${audioObjectInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="audioObject.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${audioObjectInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.transcript}">
				<li class="fieldcontain">
					<span id="transcript-label" class="property-label"><g:message code="audioObject.transcript.label" default="Transcript" /></span>
					
						<span class="property-value" aria-labelledby="transcript-label"><g:fieldValue bean="${audioObjectInstance}" field="transcript"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="audioObject.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${audioObjectInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.uploadDate}">
				<li class="fieldcontain">
					<span id="uploadDate-label" class="property-label"><g:message code="audioObject.uploadDate.label" default="Upload Date" /></span>
					
						<span class="property-value" aria-labelledby="uploadDate-label"><g:formatDate date="${audioObjectInstance?.uploadDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="audioObject.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${audioObjectInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${audioObjectInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="audioObject.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${audioObjectInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:audioObjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${audioObjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
