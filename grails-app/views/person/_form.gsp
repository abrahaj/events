<%@ page import="com.shqiperia.entities.Person" %>



<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="person.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${personInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'additionalName', 'error')} ">
	<label for="additionalName">
		<g:message code="person.additionalName.label" default="Additional Name" />
		
	</label>
	<g:textField name="additionalName" maxlength="250" value="${personInstance?.additionalName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="person.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" maxlength="250" value="${personInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'colleague', 'error')} ">
	<label for="colleague">
		<g:message code="person.colleague.label" default="Colleague" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'workLocation', 'error')} ">
	<label for="workLocation">
		<g:message code="person.workLocation.label" default="Work Location" />
		
	</label>
	<g:textArea name="workLocation" cols="40" rows="5" maxlength="250" value="${personInstance?.workLocation}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="person.address.label" default="Address" />
		
	</label>
	<g:select id="address" name="address.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${personInstance?.address?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'homeLocation', 'error')} ">
	<label for="homeLocation">
		<g:message code="person.homeLocation.label" default="Home Location" />
		
	</label>
	<g:textArea name="homeLocation" cols="40" rows="5" maxlength="250" value="${personInstance?.homeLocation}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="person.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${personInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'affiliation', 'error')} ">
	<label for="affiliation">
		<g:message code="person.affiliation.label" default="Affiliation" />
		
	</label>
	<g:select name="affiliation" from="${com.shqiperia.entities.Organization.list()}" multiple="multiple" optionKey="id" size="5" value="${personInstance?.affiliation*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="person.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${personInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'alumniOf', 'error')} ">
	<label for="alumniOf">
		<g:message code="person.alumniOf.label" default="Alumni Of" />
		
	</label>
	<g:select name="alumniOf" from="${com.shqiperia.entities.EducationalOrganization.list()}" multiple="multiple" optionKey="id" size="5" value="${personInstance?.alumniOf*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="person.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${personInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'birthDate', 'error')} ">
	<label for="birthDate">
		<g:message code="person.birthDate.label" default="Birth Date" />
		
	</label>
	<g:datePicker name="birthDate" precision="day"  value="${personInstance?.birthDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'children', 'error')} ">
	<label for="children">
		<g:message code="person.children.label" default="Children" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'deathDate', 'error')} ">
	<label for="deathDate">
		<g:message code="person.deathDate.label" default="Death Date" />
		
	</label>
	<g:datePicker name="deathDate" precision="day"  value="${personInstance?.deathDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="person.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${personInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'duns', 'error')} ">
	<label for="duns">
		<g:message code="person.duns.label" default="Duns" />
		
	</label>
	<g:textField name="duns" maxlength="250" value="${personInstance?.duns}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'familyName', 'error')} ">
	<label for="familyName">
		<g:message code="person.familyName.label" default="Family Name" />
		
	</label>
	<g:textField name="familyName" maxlength="250" value="${personInstance?.familyName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'follows', 'error')} ">
	<label for="follows">
		<g:message code="person.follows.label" default="Follows" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'gender', 'error')} ">
	<label for="gender">
		<g:message code="person.gender.label" default="Gender" />
		
	</label>
	<g:textField name="gender" maxlength="250" value="${personInstance?.gender}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'givenName', 'error')} ">
	<label for="givenName">
		<g:message code="person.givenName.label" default="Given Name" />
		
	</label>
	<g:textField name="givenName" maxlength="250" value="${personInstance?.givenName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'globalLocationNumber', 'error')} ">
	<label for="globalLocationNumber">
		<g:message code="person.globalLocationNumber.label" default="Global Location Number" />
		
	</label>
	<g:textField name="globalLocationNumber" maxlength="250" value="${personInstance?.globalLocationNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'honorificPrefix', 'error')} ">
	<label for="honorificPrefix">
		<g:message code="person.honorificPrefix.label" default="Honorific Prefix" />
		
	</label>
	<g:textField name="honorificPrefix" maxlength="250" value="${personInstance?.honorificPrefix}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'honorificSuffix', 'error')} ">
	<label for="honorificSuffix">
		<g:message code="person.honorificSuffix.label" default="Honorific Suffix" />
		
	</label>
	<g:textField name="honorificSuffix" maxlength="250" value="${personInstance?.honorificSuffix}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="person.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${personInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="person.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${personInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'isicV4', 'error')} ">
	<label for="isicV4">
		<g:message code="person.isicV4.label" default="Isic V4" />
		
	</label>
	<g:textField name="isicV4" maxlength="250" value="${personInstance?.isicV4}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'jobTitle', 'error')} ">
	<label for="jobTitle">
		<g:message code="person.jobTitle.label" default="Job Title" />
		
	</label>
	<g:textField name="jobTitle" maxlength="250" value="${personInstance?.jobTitle}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'knows', 'error')} ">
	<label for="knows">
		<g:message code="person.knows.label" default="Knows" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'memberOf', 'error')} ">
	<label for="memberOf">
		<g:message code="person.memberOf.label" default="Member Of" />
		
	</label>
	<g:select id="memberOf" name="memberOf.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${personInstance?.memberOf?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'naics', 'error')} ">
	<label for="naics">
		<g:message code="person.naics.label" default="Naics" />
		
	</label>
	<g:textField name="naics" maxlength="250" value="${personInstance?.naics}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'nationality', 'error')} ">
	<label for="nationality">
		<g:message code="person.nationality.label" default="Nationality" />
		
	</label>
	<g:textField name="nationality" maxlength="250" value="${personInstance?.nationality}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'parent', 'error')} ">
	<label for="parent">
		<g:message code="person.parent.label" default="Parent" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'relatedTo', 'error')} ">
	<label for="relatedTo">
		<g:message code="person.relatedTo.label" default="Related To" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="person.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${personInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'sibling', 'error')} ">
	<label for="sibling">
		<g:message code="person.sibling.label" default="Sibling" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'spouse', 'error')} ">
	<label for="spouse">
		<g:message code="person.spouse.label" default="Spouse" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'taxID', 'error')} ">
	<label for="taxID">
		<g:message code="person.taxID.label" default="Tax ID" />
		
	</label>
	<g:textField name="taxID" maxlength="250" value="${personInstance?.taxID}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="person.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${personInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="person.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${personInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'vatID', 'error')} ">
	<label for="vatID">
		<g:message code="person.vatID.label" default="Vat ID" />
		
	</label>
	<g:textField name="vatID" maxlength="250" value="${personInstance?.vatID}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'worksFor', 'error')} ">
	<label for="worksFor">
		<g:message code="person.worksFor.label" default="Works For" />
		
	</label>
	<g:select id="worksFor" name="worksFor.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${personInstance?.worksFor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

