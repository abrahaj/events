
<%@ page import="com.shqiperia.entities.Person" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-person" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-person" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list person">
			
				<g:if test="${personInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="person.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${personInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.additionalName}">
				<li class="fieldcontain">
					<span id="additionalName-label" class="property-label"><g:message code="person.additionalName.label" default="Additional Name" /></span>
					
						<span class="property-value" aria-labelledby="additionalName-label"><g:fieldValue bean="${personInstance}" field="additionalName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="person.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${personInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.colleague}">
				<li class="fieldcontain">
					<span id="colleague-label" class="property-label"><g:message code="person.colleague.label" default="Colleague" /></span>
					
						<g:each in="${personInstance.colleague}" var="c">
						<span class="property-value" aria-labelledby="colleague-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.workLocation}">
				<li class="fieldcontain">
					<span id="workLocation-label" class="property-label"><g:message code="person.workLocation.label" default="Work Location" /></span>
					
						<span class="property-value" aria-labelledby="workLocation-label"><g:fieldValue bean="${personInstance}" field="workLocation"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="person.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="postaladdress" action="show" id="${personInstance?.address?.id}">${personInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.homeLocation}">
				<li class="fieldcontain">
					<span id="homeLocation-label" class="property-label"><g:message code="person.homeLocation.label" default="Home Location" /></span>
					
						<span class="property-value" aria-labelledby="homeLocation-label"><g:fieldValue bean="${personInstance}" field="homeLocation"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="person.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${personInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.affiliation}">
				<li class="fieldcontain">
					<span id="affiliation-label" class="property-label"><g:message code="person.affiliation.label" default="Affiliation" /></span>
					
						<g:each in="${personInstance.affiliation}" var="a">
						<span class="property-value" aria-labelledby="affiliation-label"><g:link controller="organization" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="person.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${personInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.alumniOf}">
				<li class="fieldcontain">
					<span id="alumniOf-label" class="property-label"><g:message code="person.alumniOf.label" default="Alumni Of" /></span>
					
						<g:each in="${personInstance.alumniOf}" var="a">
						<span class="property-value" aria-labelledby="alumniOf-label"><g:link controller="educationalOrganization" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="person.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${personInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.birthDate}">
				<li class="fieldcontain">
					<span id="birthDate-label" class="property-label"><g:message code="person.birthDate.label" default="Birth Date" /></span>
					
						<span class="property-value" aria-labelledby="birthDate-label"><g:formatDate date="${personInstance?.birthDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.children}">
				<li class="fieldcontain">
					<span id="children-label" class="property-label"><g:message code="person.children.label" default="Children" /></span>
					
						<g:each in="${personInstance.children}" var="c">
						<span class="property-value" aria-labelledby="children-label"><g:link controller="person" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.deathDate}">
				<li class="fieldcontain">
					<span id="deathDate-label" class="property-label"><g:message code="person.deathDate.label" default="Death Date" /></span>
					
						<span class="property-value" aria-labelledby="deathDate-label"><g:formatDate date="${personInstance?.deathDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="person.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${personInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.duns}">
				<li class="fieldcontain">
					<span id="duns-label" class="property-label"><g:message code="person.duns.label" default="Duns" /></span>
					
						<span class="property-value" aria-labelledby="duns-label"><g:fieldValue bean="${personInstance}" field="duns"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.familyName}">
				<li class="fieldcontain">
					<span id="familyName-label" class="property-label"><g:message code="person.familyName.label" default="Family Name" /></span>
					
						<span class="property-value" aria-labelledby="familyName-label"><g:fieldValue bean="${personInstance}" field="familyName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.follows}">
				<li class="fieldcontain">
					<span id="follows-label" class="property-label"><g:message code="person.follows.label" default="Follows" /></span>
					
						<g:each in="${personInstance.follows}" var="f">
						<span class="property-value" aria-labelledby="follows-label"><g:link controller="person" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.gender}">
				<li class="fieldcontain">
					<span id="gender-label" class="property-label"><g:message code="person.gender.label" default="Gender" /></span>
					
						<span class="property-value" aria-labelledby="gender-label"><g:fieldValue bean="${personInstance}" field="gender"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.givenName}">
				<li class="fieldcontain">
					<span id="givenName-label" class="property-label"><g:message code="person.givenName.label" default="Given Name" /></span>
					
						<span class="property-value" aria-labelledby="givenName-label"><g:fieldValue bean="${personInstance}" field="givenName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.globalLocationNumber}">
				<li class="fieldcontain">
					<span id="globalLocationNumber-label" class="property-label"><g:message code="person.globalLocationNumber.label" default="Global Location Number" /></span>
					
						<span class="property-value" aria-labelledby="globalLocationNumber-label"><g:fieldValue bean="${personInstance}" field="globalLocationNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.honorificPrefix}">
				<li class="fieldcontain">
					<span id="honorificPrefix-label" class="property-label"><g:message code="person.honorificPrefix.label" default="Honorific Prefix" /></span>
					
						<span class="property-value" aria-labelledby="honorificPrefix-label"><g:fieldValue bean="${personInstance}" field="honorificPrefix"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.honorificSuffix}">
				<li class="fieldcontain">
					<span id="honorificSuffix-label" class="property-label"><g:message code="person.honorificSuffix.label" default="Honorific Suffix" /></span>
					
						<span class="property-value" aria-labelledby="honorificSuffix-label"><g:fieldValue bean="${personInstance}" field="honorificSuffix"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="person.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${personInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="person.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${personInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.isicV4}">
				<li class="fieldcontain">
					<span id="isicV4-label" class="property-label"><g:message code="person.isicV4.label" default="Isic V4" /></span>
					
						<span class="property-value" aria-labelledby="isicV4-label"><g:fieldValue bean="${personInstance}" field="isicV4"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.jobTitle}">
				<li class="fieldcontain">
					<span id="jobTitle-label" class="property-label"><g:message code="person.jobTitle.label" default="Job Title" /></span>
					
						<span class="property-value" aria-labelledby="jobTitle-label"><g:fieldValue bean="${personInstance}" field="jobTitle"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.knows}">
				<li class="fieldcontain">
					<span id="knows-label" class="property-label"><g:message code="person.knows.label" default="Knows" /></span>
					
						<g:each in="${personInstance.knows}" var="k">
						<span class="property-value" aria-labelledby="knows-label"><g:link controller="person" action="show" id="${k.id}">${k?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.memberOf}">
				<li class="fieldcontain">
					<span id="memberOf-label" class="property-label"><g:message code="person.memberOf.label" default="Member Of" /></span>
					
						<span class="property-value" aria-labelledby="memberOf-label"><g:link controller="organization" action="show" id="${personInstance?.memberOf?.id}">${personInstance?.memberOf?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.naics}">
				<li class="fieldcontain">
					<span id="naics-label" class="property-label"><g:message code="person.naics.label" default="Naics" /></span>
					
						<span class="property-value" aria-labelledby="naics-label"><g:fieldValue bean="${personInstance}" field="naics"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.nationality}">
				<li class="fieldcontain">
					<span id="nationality-label" class="property-label"><g:message code="person.nationality.label" default="Nationality" /></span>
					
						<span class="property-value" aria-labelledby="nationality-label"><g:fieldValue bean="${personInstance}" field="nationality"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.parent}">
				<li class="fieldcontain">
					<span id="parent-label" class="property-label"><g:message code="person.parent.label" default="Parent" /></span>
					
						<g:each in="${personInstance.parent}" var="p">
						<span class="property-value" aria-labelledby="parent-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.relatedTo}">
				<li class="fieldcontain">
					<span id="relatedTo-label" class="property-label"><g:message code="person.relatedTo.label" default="Related To" /></span>
					
						<g:each in="${personInstance.relatedTo}" var="r">
						<span class="property-value" aria-labelledby="relatedTo-label"><g:link controller="person" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="person.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${personInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.sibling}">
				<li class="fieldcontain">
					<span id="sibling-label" class="property-label"><g:message code="person.sibling.label" default="Sibling" /></span>
					
						<g:each in="${personInstance.sibling}" var="s">
						<span class="property-value" aria-labelledby="sibling-label"><g:link controller="person" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.spouse}">
				<li class="fieldcontain">
					<span id="spouse-label" class="property-label"><g:message code="person.spouse.label" default="Spouse" /></span>
					
						<g:each in="${personInstance.spouse}" var="s">
						<span class="property-value" aria-labelledby="spouse-label"><g:link controller="person" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.taxID}">
				<li class="fieldcontain">
					<span id="taxID-label" class="property-label"><g:message code="person.taxID.label" default="Tax ID" /></span>
					
						<span class="property-value" aria-labelledby="taxID-label"><g:fieldValue bean="${personInstance}" field="taxID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="person.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${personInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="person.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${personInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.vatID}">
				<li class="fieldcontain">
					<span id="vatID-label" class="property-label"><g:message code="person.vatID.label" default="Vat ID" /></span>
					
						<span class="property-value" aria-labelledby="vatID-label"><g:fieldValue bean="${personInstance}" field="vatID"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${personInstance?.worksFor}">
				<li class="fieldcontain">
					<span id="worksFor-label" class="property-label"><g:message code="person.worksFor.label" default="Works For" /></span>
					
						<span class="property-value" aria-labelledby="worksFor-label"><g:link controller="organization" action="show" id="${personInstance?.worksFor?.id}">${personInstance?.worksFor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:personInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${personInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
