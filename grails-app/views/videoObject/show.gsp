
<%@ page import="com.shqiperia.entities.VideoObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'videoObject.label', default: 'VideoObject')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-videoObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-videoObject" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list videoObject">
			
				<g:if test="${videoObjectInstance?.about}">
				<li class="fieldcontain">
					<span id="about-label" class="property-label"><g:message code="videoObject.about.label" default="About" /></span>
					
						<span class="property-value" aria-labelledby="about-label"><g:fieldValue bean="${videoObjectInstance}" field="about"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.accessibilityAPI}">
				<li class="fieldcontain">
					<span id="accessibilityAPI-label" class="property-label"><g:message code="videoObject.accessibilityAPI.label" default="Accessibility API" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityAPI-label"><g:fieldValue bean="${videoObjectInstance}" field="accessibilityAPI"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.accessibilityControl}">
				<li class="fieldcontain">
					<span id="accessibilityControl-label" class="property-label"><g:message code="videoObject.accessibilityControl.label" default="Accessibility Control" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityControl-label"><g:fieldValue bean="${videoObjectInstance}" field="accessibilityControl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.accessibilityFeature}">
				<li class="fieldcontain">
					<span id="accessibilityFeature-label" class="property-label"><g:message code="videoObject.accessibilityFeature.label" default="Accessibility Feature" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityFeature-label"><g:fieldValue bean="${videoObjectInstance}" field="accessibilityFeature"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.accessibilityHazard}">
				<li class="fieldcontain">
					<span id="accessibilityHazard-label" class="property-label"><g:message code="videoObject.accessibilityHazard.label" default="Accessibility Hazard" /></span>
					
						<span class="property-value" aria-labelledby="accessibilityHazard-label"><g:fieldValue bean="${videoObjectInstance}" field="accessibilityHazard"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.accountablePerson}">
				<li class="fieldcontain">
					<span id="accountablePerson-label" class="property-label"><g:message code="videoObject.accountablePerson.label" default="Accountable Person" /></span>
					
						<g:each in="${videoObjectInstance.accountablePerson}" var="a">
						<span class="property-value" aria-labelledby="accountablePerson-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="videoObject.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${videoObjectInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="videoObject.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${videoObjectInstance?.aggregateRating?.id}">${videoObjectInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="videoObject.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${videoObjectInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.alternativeHeadline}">
				<li class="fieldcontain">
					<span id="alternativeHeadline-label" class="property-label"><g:message code="videoObject.alternativeHeadline.label" default="Alternative Headline" /></span>
					
						<span class="property-value" aria-labelledby="alternativeHeadline-label"><g:fieldValue bean="${videoObjectInstance}" field="alternativeHeadline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.author}">
				<li class="fieldcontain">
					<span id="author-label" class="property-label"><g:message code="videoObject.author.label" default="Author" /></span>
					
						<span class="property-value" aria-labelledby="author-label"><g:link controller="person" action="show" id="${videoObjectInstance?.author?.id}">${videoObjectInstance?.author?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.award}">
				<li class="fieldcontain">
					<span id="award-label" class="property-label"><g:message code="videoObject.award.label" default="Award" /></span>
					
						<span class="property-value" aria-labelledby="award-label"><g:fieldValue bean="${videoObjectInstance}" field="award"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.bitrate}">
				<li class="fieldcontain">
					<span id="bitrate-label" class="property-label"><g:message code="videoObject.bitrate.label" default="Bitrate" /></span>
					
						<span class="property-value" aria-labelledby="bitrate-label"><g:fieldValue bean="${videoObjectInstance}" field="bitrate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.caption}">
				<li class="fieldcontain">
					<span id="caption-label" class="property-label"><g:message code="videoObject.caption.label" default="Caption" /></span>
					
						<span class="property-value" aria-labelledby="caption-label"><g:fieldValue bean="${videoObjectInstance}" field="caption"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.citation}">
				<li class="fieldcontain">
					<span id="citation-label" class="property-label"><g:message code="videoObject.citation.label" default="Citation" /></span>
					
						<span class="property-value" aria-labelledby="citation-label"><g:link controller="creativeWork" action="show" id="${videoObjectInstance?.citation?.id}">${videoObjectInstance?.citation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.contentLocation}">
				<li class="fieldcontain">
					<span id="contentLocation-label" class="property-label"><g:message code="videoObject.contentLocation.label" default="Content Location" /></span>
					
						<span class="property-value" aria-labelledby="contentLocation-label"><g:link controller="place" action="show" id="${videoObjectInstance?.contentLocation?.id}">${videoObjectInstance?.contentLocation?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.contentRating}">
				<li class="fieldcontain">
					<span id="contentRating-label" class="property-label"><g:message code="videoObject.contentRating.label" default="Content Rating" /></span>
					
						<span class="property-value" aria-labelledby="contentRating-label"><g:fieldValue bean="${videoObjectInstance}" field="contentRating"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.contentSize}">
				<li class="fieldcontain">
					<span id="contentSize-label" class="property-label"><g:message code="videoObject.contentSize.label" default="Content Size" /></span>
					
						<span class="property-value" aria-labelledby="contentSize-label"><g:fieldValue bean="${videoObjectInstance}" field="contentSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.contentUrl}">
				<li class="fieldcontain">
					<span id="contentUrl-label" class="property-label"><g:message code="videoObject.contentUrl.label" default="Content Url" /></span>
					
						<span class="property-value" aria-labelledby="contentUrl-label"><g:fieldValue bean="${videoObjectInstance}" field="contentUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.contributor}">
				<li class="fieldcontain">
					<span id="contributor-label" class="property-label"><g:message code="videoObject.contributor.label" default="Contributor" /></span>
					
						<span class="property-value" aria-labelledby="contributor-label"><g:link controller="person" action="show" id="${videoObjectInstance?.contributor?.id}">${videoObjectInstance?.contributor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.copyrightHolder}">
				<li class="fieldcontain">
					<span id="copyrightHolder-label" class="property-label"><g:message code="videoObject.copyrightHolder.label" default="Copyright Holder" /></span>
					
						<span class="property-value" aria-labelledby="copyrightHolder-label"><g:link controller="person" action="show" id="${videoObjectInstance?.copyrightHolder?.id}">${videoObjectInstance?.copyrightHolder?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.copyrightYear}">
				<li class="fieldcontain">
					<span id="copyrightYear-label" class="property-label"><g:message code="videoObject.copyrightYear.label" default="Copyright Year" /></span>
					
						<span class="property-value" aria-labelledby="copyrightYear-label"><g:fieldValue bean="${videoObjectInstance}" field="copyrightYear"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="videoObject.creator.label" default="Creator" /></span>
					
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${videoObjectInstance?.creator?.id}">${videoObjectInstance?.creator?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="videoObject.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${videoObjectInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.dateModified}">
				<li class="fieldcontain">
					<span id="dateModified-label" class="property-label"><g:message code="videoObject.dateModified.label" default="Date Modified" /></span>
					
						<span class="property-value" aria-labelledby="dateModified-label"><g:formatDate date="${videoObjectInstance?.dateModified}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.datePublished}">
				<li class="fieldcontain">
					<span id="datePublished-label" class="property-label"><g:message code="videoObject.datePublished.label" default="Date Published" /></span>
					
						<span class="property-value" aria-labelledby="datePublished-label"><g:formatDate date="${videoObjectInstance?.datePublished}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="videoObject.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${videoObjectInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.discussionUrl}">
				<li class="fieldcontain">
					<span id="discussionUrl-label" class="property-label"><g:message code="videoObject.discussionUrl.label" default="Discussion Url" /></span>
					
						<span class="property-value" aria-labelledby="discussionUrl-label"><g:fieldValue bean="${videoObjectInstance}" field="discussionUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="videoObject.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:fieldValue bean="${videoObjectInstance}" field="duration"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.editor}">
				<li class="fieldcontain">
					<span id="editor-label" class="property-label"><g:message code="videoObject.editor.label" default="Editor" /></span>
					
						<span class="property-value" aria-labelledby="editor-label"><g:link controller="person" action="show" id="${videoObjectInstance?.editor?.id}">${videoObjectInstance?.editor?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.educationalUse}">
				<li class="fieldcontain">
					<span id="educationalUse-label" class="property-label"><g:message code="videoObject.educationalUse.label" default="Educational Use" /></span>
					
						<span class="property-value" aria-labelledby="educationalUse-label"><g:fieldValue bean="${videoObjectInstance}" field="educationalUse"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.embedUrl}">
				<li class="fieldcontain">
					<span id="embedUrl-label" class="property-label"><g:message code="videoObject.embedUrl.label" default="Embed Url" /></span>
					
						<span class="property-value" aria-labelledby="embedUrl-label"><g:fieldValue bean="${videoObjectInstance}" field="embedUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.encodesCreativeWork}">
				<li class="fieldcontain">
					<span id="encodesCreativeWork-label" class="property-label"><g:message code="videoObject.encodesCreativeWork.label" default="Encodes Creative Work" /></span>
					
						<span class="property-value" aria-labelledby="encodesCreativeWork-label"><g:link controller="creativeWork" action="show" id="${videoObjectInstance?.encodesCreativeWork?.id}">${videoObjectInstance?.encodesCreativeWork?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.encodingFormat}">
				<li class="fieldcontain">
					<span id="encodingFormat-label" class="property-label"><g:message code="videoObject.encodingFormat.label" default="Encoding Format" /></span>
					
						<span class="property-value" aria-labelledby="encodingFormat-label"><g:fieldValue bean="${videoObjectInstance}" field="encodingFormat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.expires}">
				<li class="fieldcontain">
					<span id="expires-label" class="property-label"><g:message code="videoObject.expires.label" default="Expires" /></span>
					
						<span class="property-value" aria-labelledby="expires-label"><g:formatDate date="${videoObjectInstance?.expires}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.genre}">
				<li class="fieldcontain">
					<span id="genre-label" class="property-label"><g:message code="videoObject.genre.label" default="Genre" /></span>
					
						<span class="property-value" aria-labelledby="genre-label"><g:fieldValue bean="${videoObjectInstance}" field="genre"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.headline}">
				<li class="fieldcontain">
					<span id="headline-label" class="property-label"><g:message code="videoObject.headline.label" default="Headline" /></span>
					
						<span class="property-value" aria-labelledby="headline-label"><g:fieldValue bean="${videoObjectInstance}" field="headline"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.height}">
				<li class="fieldcontain">
					<span id="height-label" class="property-label"><g:message code="videoObject.height.label" default="Height" /></span>
					
						<span class="property-value" aria-labelledby="height-label"><g:fieldValue bean="${videoObjectInstance}" field="height"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="videoObject.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${videoObjectInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.inLanguage}">
				<li class="fieldcontain">
					<span id="inLanguage-label" class="property-label"><g:message code="videoObject.inLanguage.label" default="In Language" /></span>
					
						<span class="property-value" aria-labelledby="inLanguage-label"><g:fieldValue bean="${videoObjectInstance}" field="inLanguage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="videoObject.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${videoObjectInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.interactivityType}">
				<li class="fieldcontain">
					<span id="interactivityType-label" class="property-label"><g:message code="videoObject.interactivityType.label" default="Interactivity Type" /></span>
					
						<span class="property-value" aria-labelledby="interactivityType-label"><g:fieldValue bean="${videoObjectInstance}" field="interactivityType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.isBasedOnUrl}">
				<li class="fieldcontain">
					<span id="isBasedOnUrl-label" class="property-label"><g:message code="videoObject.isBasedOnUrl.label" default="Is Based On Url" /></span>
					
						<span class="property-value" aria-labelledby="isBasedOnUrl-label"><g:fieldValue bean="${videoObjectInstance}" field="isBasedOnUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.isFamilyFriendly}">
				<li class="fieldcontain">
					<span id="isFamilyFriendly-label" class="property-label"><g:message code="videoObject.isFamilyFriendly.label" default="Is Family Friendly" /></span>
					
						<span class="property-value" aria-labelledby="isFamilyFriendly-label"><g:formatBoolean boolean="${videoObjectInstance?.isFamilyFriendly}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.keywords}">
				<li class="fieldcontain">
					<span id="keywords-label" class="property-label"><g:message code="videoObject.keywords.label" default="Keywords" /></span>
					
						<span class="property-value" aria-labelledby="keywords-label"><g:fieldValue bean="${videoObjectInstance}" field="keywords"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.learningResourceType}">
				<li class="fieldcontain">
					<span id="learningResourceType-label" class="property-label"><g:message code="videoObject.learningResourceType.label" default="Learning Resource Type" /></span>
					
						<span class="property-value" aria-labelledby="learningResourceType-label"><g:fieldValue bean="${videoObjectInstance}" field="learningResourceType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.mentions}">
				<li class="fieldcontain">
					<span id="mentions-label" class="property-label"><g:message code="videoObject.mentions.label" default="Mentions" /></span>
					
						<span class="property-value" aria-labelledby="mentions-label"><g:fieldValue bean="${videoObjectInstance}" field="mentions"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="videoObject.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${videoObjectInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.offers}">
				<li class="fieldcontain">
					<span id="offers-label" class="property-label"><g:message code="videoObject.offers.label" default="Offers" /></span>
					
						<span class="property-value" aria-labelledby="offers-label"><g:link controller="offer" action="show" id="${videoObjectInstance?.offers?.id}">${videoObjectInstance?.offers?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.playerType}">
				<li class="fieldcontain">
					<span id="playerType-label" class="property-label"><g:message code="videoObject.playerType.label" default="Player Type" /></span>
					
						<span class="property-value" aria-labelledby="playerType-label"><g:fieldValue bean="${videoObjectInstance}" field="playerType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.productionCompany}">
				<li class="fieldcontain">
					<span id="productionCompany-label" class="property-label"><g:message code="videoObject.productionCompany.label" default="Production Company" /></span>
					
						<span class="property-value" aria-labelledby="productionCompany-label"><g:link controller="organization" action="show" id="${videoObjectInstance?.productionCompany?.id}">${videoObjectInstance?.productionCompany?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.provider}">
				<li class="fieldcontain">
					<span id="provider-label" class="property-label"><g:message code="videoObject.provider.label" default="Provider" /></span>
					
						<span class="property-value" aria-labelledby="provider-label"><g:link controller="person" action="show" id="${videoObjectInstance?.provider?.id}">${videoObjectInstance?.provider?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.publication}">
				<li class="fieldcontain">
					<span id="publication-label" class="property-label"><g:message code="videoObject.publication.label" default="Publication" /></span>
					
						<span class="property-value" aria-labelledby="publication-label"><g:link controller="publicationEvent" action="show" id="${videoObjectInstance?.publication?.id}">${videoObjectInstance?.publication?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.publisher}">
				<li class="fieldcontain">
					<span id="publisher-label" class="property-label"><g:message code="videoObject.publisher.label" default="Publisher" /></span>
					
						<span class="property-value" aria-labelledby="publisher-label"><g:link controller="organization" action="show" id="${videoObjectInstance?.publisher?.id}">${videoObjectInstance?.publisher?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.publishingPrinciples}">
				<li class="fieldcontain">
					<span id="publishingPrinciples-label" class="property-label"><g:message code="videoObject.publishingPrinciples.label" default="Publishing Principles" /></span>
					
						<span class="property-value" aria-labelledby="publishingPrinciples-label"><g:fieldValue bean="${videoObjectInstance}" field="publishingPrinciples"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.regionsAllowed}">
				<li class="fieldcontain">
					<span id="regionsAllowed-label" class="property-label"><g:message code="videoObject.regionsAllowed.label" default="Regions Allowed" /></span>
					
						<span class="property-value" aria-labelledby="regionsAllowed-label"><g:link controller="place" action="show" id="${videoObjectInstance?.regionsAllowed?.id}">${videoObjectInstance?.regionsAllowed?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.requiresSubscription}">
				<li class="fieldcontain">
					<span id="requiresSubscription-label" class="property-label"><g:message code="videoObject.requiresSubscription.label" default="Requires Subscription" /></span>
					
						<span class="property-value" aria-labelledby="requiresSubscription-label"><g:formatBoolean boolean="${videoObjectInstance?.requiresSubscription}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="videoObject.review.label" default="Review" /></span>
					
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${videoObjectInstance?.review?.id}">${videoObjectInstance?.review?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="videoObject.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${videoObjectInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.sourceOrganization}">
				<li class="fieldcontain">
					<span id="sourceOrganization-label" class="property-label"><g:message code="videoObject.sourceOrganization.label" default="Source Organization" /></span>
					
						<span class="property-value" aria-labelledby="sourceOrganization-label"><g:link controller="organization" action="show" id="${videoObjectInstance?.sourceOrganization?.id}">${videoObjectInstance?.sourceOrganization?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.text}">
				<li class="fieldcontain">
					<span id="text-label" class="property-label"><g:message code="videoObject.text.label" default="Text" /></span>
					
						<span class="property-value" aria-labelledby="text-label"><g:fieldValue bean="${videoObjectInstance}" field="text"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.thumbnail}">
				<li class="fieldcontain">
					<span id="thumbnail-label" class="property-label"><g:message code="videoObject.thumbnail.label" default="Thumbnail" /></span>
					
						<span class="property-value" aria-labelledby="thumbnail-label"><g:fieldValue bean="${videoObjectInstance}" field="thumbnail"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.thumbnailUrl}">
				<li class="fieldcontain">
					<span id="thumbnailUrl-label" class="property-label"><g:message code="videoObject.thumbnailUrl.label" default="Thumbnail Url" /></span>
					
						<span class="property-value" aria-labelledby="thumbnailUrl-label"><g:fieldValue bean="${videoObjectInstance}" field="thumbnailUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.transcript}">
				<li class="fieldcontain">
					<span id="transcript-label" class="property-label"><g:message code="videoObject.transcript.label" default="Transcript" /></span>
					
						<span class="property-value" aria-labelledby="transcript-label"><g:fieldValue bean="${videoObjectInstance}" field="transcript"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="videoObject.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${videoObjectInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.uploadDate}">
				<li class="fieldcontain">
					<span id="uploadDate-label" class="property-label"><g:message code="videoObject.uploadDate.label" default="Upload Date" /></span>
					
						<span class="property-value" aria-labelledby="uploadDate-label"><g:formatDate date="${videoObjectInstance?.uploadDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="videoObject.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${videoObjectInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.videoFrameSize}">
				<li class="fieldcontain">
					<span id="videoFrameSize-label" class="property-label"><g:message code="videoObject.videoFrameSize.label" default="Video Frame Size" /></span>
					
						<span class="property-value" aria-labelledby="videoFrameSize-label"><g:fieldValue bean="${videoObjectInstance}" field="videoFrameSize"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.videoQuality}">
				<li class="fieldcontain">
					<span id="videoQuality-label" class="property-label"><g:message code="videoObject.videoQuality.label" default="Video Quality" /></span>
					
						<span class="property-value" aria-labelledby="videoQuality-label"><g:fieldValue bean="${videoObjectInstance}" field="videoQuality"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${videoObjectInstance?.width}">
				<li class="fieldcontain">
					<span id="width-label" class="property-label"><g:message code="videoObject.width.label" default="Width" /></span>
					
						<span class="property-value" aria-labelledby="width-label"><g:fieldValue bean="${videoObjectInstance}" field="width"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:videoObjectInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${videoObjectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
