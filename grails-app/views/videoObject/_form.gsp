<%@ page import="com.shqiperia.entities.VideoObject" %>



<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'about', 'error')} ">
	<label for="about">
		<g:message code="videoObject.about.label" default="About" />
		
	</label>
	<g:textField name="about" maxlength="250" value="${videoObjectInstance?.about}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'accessibilityAPI', 'error')} ">
	<label for="accessibilityAPI">
		<g:message code="videoObject.accessibilityAPI.label" default="Accessibility API" />
		
	</label>
	<g:textField name="accessibilityAPI" maxlength="250" value="${videoObjectInstance?.accessibilityAPI}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'accessibilityControl', 'error')} ">
	<label for="accessibilityControl">
		<g:message code="videoObject.accessibilityControl.label" default="Accessibility Control" />
		
	</label>
	<g:textField name="accessibilityControl" maxlength="250" value="${videoObjectInstance?.accessibilityControl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'accessibilityFeature', 'error')} ">
	<label for="accessibilityFeature">
		<g:message code="videoObject.accessibilityFeature.label" default="Accessibility Feature" />
		
	</label>
	<g:textField name="accessibilityFeature" maxlength="250" value="${videoObjectInstance?.accessibilityFeature}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'accessibilityHazard', 'error')} ">
	<label for="accessibilityHazard">
		<g:message code="videoObject.accessibilityHazard.label" default="Accessibility Hazard" />
		
	</label>
	<g:textField name="accessibilityHazard" maxlength="250" value="${videoObjectInstance?.accessibilityHazard}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'accountablePerson', 'error')} ">
	<label for="accountablePerson">
		<g:message code="videoObject.accountablePerson.label" default="Accountable Person" />
		
	</label>
	<g:select name="accountablePerson" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${videoObjectInstance?.accountablePerson*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="videoObject.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${videoObjectInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="videoObject.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${videoObjectInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="videoObject.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${videoObjectInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'alternativeHeadline', 'error')} ">
	<label for="alternativeHeadline">
		<g:message code="videoObject.alternativeHeadline.label" default="Alternative Headline" />
		
	</label>
	<g:textField name="alternativeHeadline" maxlength="250" value="${videoObjectInstance?.alternativeHeadline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'author', 'error')} ">
	<label for="author">
		<g:message code="videoObject.author.label" default="Author" />
		
	</label>
	<g:select id="author" name="author.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.author?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'award', 'error')} ">
	<label for="award">
		<g:message code="videoObject.award.label" default="Award" />
		
	</label>
	<g:textField name="award" maxlength="250" value="${videoObjectInstance?.award}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'bitrate', 'error')} ">
	<label for="bitrate">
		<g:message code="videoObject.bitrate.label" default="Bitrate" />
		
	</label>
	<g:textField name="bitrate" maxlength="250" value="${videoObjectInstance?.bitrate}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'caption', 'error')} ">
	<label for="caption">
		<g:message code="videoObject.caption.label" default="Caption" />
		
	</label>
	<g:textField name="caption" maxlength="250" value="${videoObjectInstance?.caption}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'citation', 'error')} required">
	<label for="citation">
		<g:message code="videoObject.citation.label" default="Citation" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="citation" name="citation.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${videoObjectInstance?.citation?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'contentLocation', 'error')} ">
	<label for="contentLocation">
		<g:message code="videoObject.contentLocation.label" default="Content Location" />
		
	</label>
	<g:select id="contentLocation" name="contentLocation.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${videoObjectInstance?.contentLocation?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'contentRating', 'error')} ">
	<label for="contentRating">
		<g:message code="videoObject.contentRating.label" default="Content Rating" />
		
	</label>
	<g:textField name="contentRating" maxlength="250" value="${videoObjectInstance?.contentRating}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'contentSize', 'error')} ">
	<label for="contentSize">
		<g:message code="videoObject.contentSize.label" default="Content Size" />
		
	</label>
	<g:textField name="contentSize" maxlength="250" value="${videoObjectInstance?.contentSize}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'contentUrl', 'error')} ">
	<label for="contentUrl">
		<g:message code="videoObject.contentUrl.label" default="Content Url" />
		
	</label>
	<g:textField name="contentUrl" maxlength="250" value="${videoObjectInstance?.contentUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'contributor', 'error')} ">
	<label for="contributor">
		<g:message code="videoObject.contributor.label" default="Contributor" />
		
	</label>
	<g:select id="contributor" name="contributor.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.contributor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'copyrightHolder', 'error')} ">
	<label for="copyrightHolder">
		<g:message code="videoObject.copyrightHolder.label" default="Copyright Holder" />
		
	</label>
	<g:select id="copyrightHolder" name="copyrightHolder.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.copyrightHolder?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'copyrightYear', 'error')} ">
	<label for="copyrightYear">
		<g:message code="videoObject.copyrightYear.label" default="Copyright Year" />
		
	</label>
	<g:field name="copyrightYear" type="number" value="${videoObjectInstance.copyrightYear}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="videoObject.creator.label" default="Creator" />
		
	</label>
	<g:select id="creator" name="creator.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.creator?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'dateModified', 'error')} ">
	<label for="dateModified">
		<g:message code="videoObject.dateModified.label" default="Date Modified" />
		
	</label>
	<g:datePicker name="dateModified" precision="day"  value="${videoObjectInstance?.dateModified}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'datePublished', 'error')} ">
	<label for="datePublished">
		<g:message code="videoObject.datePublished.label" default="Date Published" />
		
	</label>
	<g:datePicker name="datePublished" precision="day"  value="${videoObjectInstance?.datePublished}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="videoObject.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${videoObjectInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'discussionUrl', 'error')} ">
	<label for="discussionUrl">
		<g:message code="videoObject.discussionUrl.label" default="Discussion Url" />
		
	</label>
	<g:textField name="discussionUrl" maxlength="250" value="${videoObjectInstance?.discussionUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="videoObject.duration.label" default="Duration" />
		
	</label>
	<g:textField name="duration" maxlength="250" value="${videoObjectInstance?.duration}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'editor', 'error')} ">
	<label for="editor">
		<g:message code="videoObject.editor.label" default="Editor" />
		
	</label>
	<g:select id="editor" name="editor.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.editor?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'educationalUse', 'error')} ">
	<label for="educationalUse">
		<g:message code="videoObject.educationalUse.label" default="Educational Use" />
		
	</label>
	<g:textField name="educationalUse" maxlength="250" value="${videoObjectInstance?.educationalUse}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'embedUrl', 'error')} ">
	<label for="embedUrl">
		<g:message code="videoObject.embedUrl.label" default="Embed Url" />
		
	</label>
	<g:textField name="embedUrl" maxlength="250" value="${videoObjectInstance?.embedUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'encodesCreativeWork', 'error')} required">
	<label for="encodesCreativeWork">
		<g:message code="videoObject.encodesCreativeWork.label" default="Encodes Creative Work" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="encodesCreativeWork" name="encodesCreativeWork.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" required="" value="${videoObjectInstance?.encodesCreativeWork?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'encodingFormat', 'error')} ">
	<label for="encodingFormat">
		<g:message code="videoObject.encodingFormat.label" default="Encoding Format" />
		
	</label>
	<g:textField name="encodingFormat" maxlength="250" value="${videoObjectInstance?.encodingFormat}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'expires', 'error')} ">
	<label for="expires">
		<g:message code="videoObject.expires.label" default="Expires" />
		
	</label>
	<g:datePicker name="expires" precision="day"  value="${videoObjectInstance?.expires}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'genre', 'error')} ">
	<label for="genre">
		<g:message code="videoObject.genre.label" default="Genre" />
		
	</label>
	<g:textField name="genre" maxlength="250" value="${videoObjectInstance?.genre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'headline', 'error')} ">
	<label for="headline">
		<g:message code="videoObject.headline.label" default="Headline" />
		
	</label>
	<g:textField name="headline" maxlength="250" value="${videoObjectInstance?.headline}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'height', 'error')} ">
	<label for="height">
		<g:message code="videoObject.height.label" default="Height" />
		
	</label>
	<g:textField name="height" maxlength="250" value="${videoObjectInstance?.height}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="videoObject.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${videoObjectInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'inLanguage', 'error')} ">
	<label for="inLanguage">
		<g:message code="videoObject.inLanguage.label" default="In Language" />
		
	</label>
	<g:textField name="inLanguage" maxlength="250" value="${videoObjectInstance?.inLanguage}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="videoObject.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${videoObjectInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'interactivityType', 'error')} ">
	<label for="interactivityType">
		<g:message code="videoObject.interactivityType.label" default="Interactivity Type" />
		
	</label>
	<g:textField name="interactivityType" maxlength="250" value="${videoObjectInstance?.interactivityType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'isBasedOnUrl', 'error')} ">
	<label for="isBasedOnUrl">
		<g:message code="videoObject.isBasedOnUrl.label" default="Is Based On Url" />
		
	</label>
	<g:textField name="isBasedOnUrl" maxlength="250" value="${videoObjectInstance?.isBasedOnUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'isFamilyFriendly', 'error')} ">
	<label for="isFamilyFriendly">
		<g:message code="videoObject.isFamilyFriendly.label" default="Is Family Friendly" />
		
	</label>
	<g:checkBox name="isFamilyFriendly" value="${videoObjectInstance?.isFamilyFriendly}" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'keywords', 'error')} ">
	<label for="keywords">
		<g:message code="videoObject.keywords.label" default="Keywords" />
		
	</label>
	<g:textField name="keywords" maxlength="250" value="${videoObjectInstance?.keywords}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'learningResourceType', 'error')} ">
	<label for="learningResourceType">
		<g:message code="videoObject.learningResourceType.label" default="Learning Resource Type" />
		
	</label>
	<g:textField name="learningResourceType" maxlength="250" value="${videoObjectInstance?.learningResourceType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'mentions', 'error')} ">
	<label for="mentions">
		<g:message code="videoObject.mentions.label" default="Mentions" />
		
	</label>
	<g:textField name="mentions" maxlength="250" value="${videoObjectInstance?.mentions}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="videoObject.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${videoObjectInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'offers', 'error')} ">
	<label for="offers">
		<g:message code="videoObject.offers.label" default="Offers" />
		
	</label>
	<g:select id="offers" name="offers.id" from="${com.shqiperia.entities.Offer.list()}" optionKey="id" value="${videoObjectInstance?.offers?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'playerType', 'error')} ">
	<label for="playerType">
		<g:message code="videoObject.playerType.label" default="Player Type" />
		
	</label>
	<g:textField name="playerType" maxlength="250" value="${videoObjectInstance?.playerType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'productionCompany', 'error')} ">
	<label for="productionCompany">
		<g:message code="videoObject.productionCompany.label" default="Production Company" />
		
	</label>
	<g:select id="productionCompany" name="productionCompany.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${videoObjectInstance?.productionCompany?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'provider', 'error')} ">
	<label for="provider">
		<g:message code="videoObject.provider.label" default="Provider" />
		
	</label>
	<g:select id="provider" name="provider.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${videoObjectInstance?.provider?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'publication', 'error')} ">
	<label for="publication">
		<g:message code="videoObject.publication.label" default="Publication" />
		
	</label>
	<g:select id="publication" name="publication.id" from="${com.shqiperia.entities.PublicationEvent.list()}" optionKey="id" value="${videoObjectInstance?.publication?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'publisher', 'error')} ">
	<label for="publisher">
		<g:message code="videoObject.publisher.label" default="Publisher" />
		
	</label>
	<g:select id="publisher" name="publisher.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${videoObjectInstance?.publisher?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'publishingPrinciples', 'error')} ">
	<label for="publishingPrinciples">
		<g:message code="videoObject.publishingPrinciples.label" default="Publishing Principles" />
		
	</label>
	<g:textField name="publishingPrinciples" maxlength="250" value="${videoObjectInstance?.publishingPrinciples}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'regionsAllowed', 'error')} ">
	<label for="regionsAllowed">
		<g:message code="videoObject.regionsAllowed.label" default="Regions Allowed" />
		
	</label>
	<g:select id="regionsAllowed" name="regionsAllowed.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${videoObjectInstance?.regionsAllowed?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'requiresSubscription', 'error')} ">
	<label for="requiresSubscription">
		<g:message code="videoObject.requiresSubscription.label" default="Requires Subscription" />
		
	</label>
	<g:checkBox name="requiresSubscription" value="${videoObjectInstance?.requiresSubscription}" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'review', 'error')} ">
	<label for="review">
		<g:message code="videoObject.review.label" default="Review" />
		
	</label>
	<g:select id="review" name="review.id" from="${com.shqiperia.entities.Review.list()}" optionKey="id" value="${videoObjectInstance?.review?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="videoObject.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${videoObjectInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'sourceOrganization', 'error')} ">
	<label for="sourceOrganization">
		<g:message code="videoObject.sourceOrganization.label" default="Source Organization" />
		
	</label>
	<g:select id="sourceOrganization" name="sourceOrganization.id" from="${com.shqiperia.entities.Organization.list()}" optionKey="id" value="${videoObjectInstance?.sourceOrganization?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'text', 'error')} ">
	<label for="text">
		<g:message code="videoObject.text.label" default="Text" />
		
	</label>
	<g:textField name="text" maxlength="250" value="${videoObjectInstance?.text}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'thumbnail', 'error')} ">
	<label for="thumbnail">
		<g:message code="videoObject.thumbnail.label" default="Thumbnail" />
		
	</label>
	<g:textField name="thumbnail" maxlength="250" value="${videoObjectInstance?.thumbnail}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'thumbnailUrl', 'error')} ">
	<label for="thumbnailUrl">
		<g:message code="videoObject.thumbnailUrl.label" default="Thumbnail Url" />
		
	</label>
	<g:textField name="thumbnailUrl" maxlength="250" value="${videoObjectInstance?.thumbnailUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'transcript', 'error')} ">
	<label for="transcript">
		<g:message code="videoObject.transcript.label" default="Transcript" />
		
	</label>
	<g:textField name="transcript" maxlength="250" value="${videoObjectInstance?.transcript}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="videoObject.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${videoObjectInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'uploadDate', 'error')} ">
	<label for="uploadDate">
		<g:message code="videoObject.uploadDate.label" default="Upload Date" />
		
	</label>
	<g:datePicker name="uploadDate" precision="day"  value="${videoObjectInstance?.uploadDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="videoObject.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${videoObjectInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'videoFrameSize', 'error')} ">
	<label for="videoFrameSize">
		<g:message code="videoObject.videoFrameSize.label" default="Video Frame Size" />
		
	</label>
	<g:textField name="videoFrameSize" maxlength="250" value="${videoObjectInstance?.videoFrameSize}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'videoQuality', 'error')} ">
	<label for="videoQuality">
		<g:message code="videoObject.videoQuality.label" default="Video Quality" />
		
	</label>
	<g:textField name="videoQuality" maxlength="250" value="${videoObjectInstance?.videoQuality}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: videoObjectInstance, field: 'width', 'error')} ">
	<label for="width">
		<g:message code="videoObject.width.label" default="Width" />
		
	</label>
	<g:textField name="width" maxlength="250" value="${videoObjectInstance?.width}"/>
</div>

