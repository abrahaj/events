
<%@ page import="com.shqiperia.entities.VideoObject" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'videoObject.label', default: 'VideoObject')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-videoObject" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-videoObject" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="about" title="${message(code: 'videoObject.about.label', default: 'About')}" />
					
						<g:sortableColumn property="accessibilityAPI" title="${message(code: 'videoObject.accessibilityAPI.label', default: 'Accessibility API')}" />
					
						<g:sortableColumn property="accessibilityControl" title="${message(code: 'videoObject.accessibilityControl.label', default: 'Accessibility Control')}" />
					
						<g:sortableColumn property="accessibilityFeature" title="${message(code: 'videoObject.accessibilityFeature.label', default: 'Accessibility Feature')}" />
					
						<g:sortableColumn property="accessibilityHazard" title="${message(code: 'videoObject.accessibilityHazard.label', default: 'Accessibility Hazard')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'videoObject.additionalType.label', default: 'Additional Type')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${videoObjectInstanceList}" status="i" var="videoObjectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${videoObjectInstance.id}">${fieldValue(bean: videoObjectInstance, field: "about")}</g:link></td>
					
						<td>${fieldValue(bean: videoObjectInstance, field: "accessibilityAPI")}</td>
					
						<td>${fieldValue(bean: videoObjectInstance, field: "accessibilityControl")}</td>
					
						<td>${fieldValue(bean: videoObjectInstance, field: "accessibilityFeature")}</td>
					
						<td>${fieldValue(bean: videoObjectInstance, field: "accessibilityHazard")}</td>
					
						<td>${fieldValue(bean: videoObjectInstance, field: "additionalType")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${videoObjectInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
