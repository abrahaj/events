<%@ page import="com.shqiperia.entities.UserComments" %>



<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="userComments.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${userCommentsInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="userComments.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${userCommentsInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="userComments.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${userCommentsInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="userComments.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${userCommentsInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'commentText', 'error')} ">
	<label for="commentText">
		<g:message code="userComments.commentText.label" default="Comment Text" />
		
	</label>
	<g:textField name="commentText" maxlength="250" value="${userCommentsInstance?.commentText}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'commentTime', 'error')} ">
	<label for="commentTime">
		<g:message code="userComments.commentTime.label" default="Comment Time" />
		
	</label>
	<g:datePicker name="commentTime" precision="day"  value="${userCommentsInstance?.commentTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'creator', 'error')} ">
	<label for="creator">
		<g:message code="userComments.creator.label" default="Creator" />
		
	</label>
	<g:select id="creator" name="creator.id" from="${com.shqiperia.entities.Person.list()}" optionKey="id" value="${userCommentsInstance?.creator?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="userComments.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${userCommentsInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'discusses', 'error')} ">
	<label for="discusses">
		<g:message code="userComments.discusses.label" default="Discusses" />
		
	</label>
	<g:select id="discusses" name="discusses.id" from="${com.shqiperia.entities.CreativeWork.list()}" optionKey="id" value="${userCommentsInstance?.discusses?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="userComments.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${userCommentsInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="userComments.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${userCommentsInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="userComments.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${userCommentsInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="userComments.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${userCommentsInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="userComments.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${userCommentsInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="userComments.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${userCommentsInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="userComments.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${userCommentsInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="userComments.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${userCommentsInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'replyToUrl', 'error')} ">
	<label for="replyToUrl">
		<g:message code="userComments.replyToUrl.label" default="Reply To Url" />
		
	</label>
	<g:textField name="replyToUrl" maxlength="250" value="${userCommentsInstance?.replyToUrl}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="userComments.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${userCommentsInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="userComments.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${userCommentsInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="userComments.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="userComments.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="userComments.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${userCommentsInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userCommentsInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="userComments.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${userCommentsInstance?.url}"/>
</div>

