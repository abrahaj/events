
<%@ page import="com.shqiperia.entities.UserComments" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'userComments.label', default: 'UserComments')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-userComments" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-userComments" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list userComments">
			
				<g:if test="${userCommentsInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="userComments.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${userCommentsInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="userComments.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${userCommentsInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="userComments.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${userCommentsInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.attendee}">
				<li class="fieldcontain">
					<span id="attendee-label" class="property-label"><g:message code="userComments.attendee.label" default="Attendee" /></span>
					
						<g:each in="${userCommentsInstance.attendee}" var="a">
						<span class="property-value" aria-labelledby="attendee-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.commentText}">
				<li class="fieldcontain">
					<span id="commentText-label" class="property-label"><g:message code="userComments.commentText.label" default="Comment Text" /></span>
					
						<span class="property-value" aria-labelledby="commentText-label"><g:fieldValue bean="${userCommentsInstance}" field="commentText"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.commentTime}">
				<li class="fieldcontain">
					<span id="commentTime-label" class="property-label"><g:message code="userComments.commentTime.label" default="Comment Time" /></span>
					
						<span class="property-value" aria-labelledby="commentTime-label"><g:formatDate date="${userCommentsInstance?.commentTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.creator}">
				<li class="fieldcontain">
					<span id="creator-label" class="property-label"><g:message code="userComments.creator.label" default="Creator" /></span>
					
						<span class="property-value" aria-labelledby="creator-label"><g:link controller="person" action="show" id="${userCommentsInstance?.creator?.id}">${userCommentsInstance?.creator?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="userComments.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${userCommentsInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.discusses}">
				<li class="fieldcontain">
					<span id="discusses-label" class="property-label"><g:message code="userComments.discusses.label" default="Discusses" /></span>
					
						<span class="property-value" aria-labelledby="discusses-label"><g:link controller="creativeWork" action="show" id="${userCommentsInstance?.discusses?.id}">${userCommentsInstance?.discusses?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.doorTime}">
				<li class="fieldcontain">
					<span id="doorTime-label" class="property-label"><g:message code="userComments.doorTime.label" default="Door Time" /></span>
					
						<span class="property-value" aria-labelledby="doorTime-label"><g:formatDate date="${userCommentsInstance?.doorTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="userComments.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:formatDate date="${userCommentsInstance?.duration}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="userComments.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${userCommentsInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.eventStatus}">
				<li class="fieldcontain">
					<span id="eventStatus-label" class="property-label"><g:message code="userComments.eventStatus.label" default="Event Status" /></span>
					
						<span class="property-value" aria-labelledby="eventStatus-label"><g:fieldValue bean="${userCommentsInstance}" field="eventStatus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="userComments.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${userCommentsInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="userComments.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:link controller="postaladdress" action="show" id="${userCommentsInstance?.location?.id}">${userCommentsInstance?.location?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.performer}">
				<li class="fieldcontain">
					<span id="performer-label" class="property-label"><g:message code="userComments.performer.label" default="Performer" /></span>
					
						<g:each in="${userCommentsInstance.performer}" var="p">
						<span class="property-value" aria-labelledby="performer-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.previousStartDate}">
				<li class="fieldcontain">
					<span id="previousStartDate-label" class="property-label"><g:message code="userComments.previousStartDate.label" default="Previous Start Date" /></span>
					
						<span class="property-value" aria-labelledby="previousStartDate-label"><g:formatDate date="${userCommentsInstance?.previousStartDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.replyToUrl}">
				<li class="fieldcontain">
					<span id="replyToUrl-label" class="property-label"><g:message code="userComments.replyToUrl.label" default="Reply To Url" /></span>
					
						<span class="property-value" aria-labelledby="replyToUrl-label"><g:fieldValue bean="${userCommentsInstance}" field="replyToUrl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="userComments.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${userCommentsInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="userComments.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${userCommentsInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.subEvent}">
				<li class="fieldcontain">
					<span id="subEvent-label" class="property-label"><g:message code="userComments.subEvent.label" default="Sub Event" /></span>
					
						<g:each in="${userCommentsInstance.subEvent}" var="s">
						<span class="property-value" aria-labelledby="subEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.superEvent}">
				<li class="fieldcontain">
					<span id="superEvent-label" class="property-label"><g:message code="userComments.superEvent.label" default="Super Event" /></span>
					
						<g:each in="${userCommentsInstance.superEvent}" var="s">
						<span class="property-value" aria-labelledby="superEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="userComments.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${userCommentsInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userCommentsInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="userComments.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${userCommentsInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:userCommentsInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${userCommentsInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
