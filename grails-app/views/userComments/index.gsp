
<%@ page import="com.shqiperia.entities.UserComments" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'userComments.label', default: 'UserComments')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-userComments" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-userComments" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'userComments.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'userComments.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'userComments.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="commentText" title="${message(code: 'userComments.commentText.label', default: 'Comment Text')}" />
					
						<g:sortableColumn property="commentTime" title="${message(code: 'userComments.commentTime.label', default: 'Comment Time')}" />
					
						<th><g:message code="userComments.creator.label" default="Creator" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${userCommentsInstanceList}" status="i" var="userCommentsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${userCommentsInstance.id}">${fieldValue(bean: userCommentsInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: userCommentsInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: userCommentsInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: userCommentsInstance, field: "commentText")}</td>
					
						<td><g:formatDate date="${userCommentsInstance.commentTime}" /></td>
					
						<td>${fieldValue(bean: userCommentsInstance, field: "creator")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${userCommentsInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
