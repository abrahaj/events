
<%@ page import="com.shqiperia.entities.StructuredValue" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'structuredValue.label', default: 'StructuredValue')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-structuredValue" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-structuredValue" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list structuredValue">
			
				<g:if test="${structuredValueInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="structuredValue.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${structuredValueInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="structuredValue.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${structuredValueInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="structuredValue.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${structuredValueInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="structuredValue.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${structuredValueInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="structuredValue.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${structuredValueInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="structuredValue.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${structuredValueInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${structuredValueInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="structuredValue.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${structuredValueInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:structuredValueInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${structuredValueInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
