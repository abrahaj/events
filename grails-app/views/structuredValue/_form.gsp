<%@ page import="com.shqiperia.entities.StructuredValue" %>



<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="structuredValue.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${structuredValueInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="structuredValue.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${structuredValueInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="structuredValue.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${structuredValueInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="structuredValue.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${structuredValueInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="structuredValue.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${structuredValueInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="structuredValue.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${structuredValueInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: structuredValueInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="structuredValue.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${structuredValueInstance?.url}"/>
</div>

