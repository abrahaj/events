<%@ page import="com.shqiperia.entities.DeliveryEvent" %>



<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="deliveryEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${deliveryEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="deliveryEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${deliveryEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="deliveryEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${deliveryEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="deliveryEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${deliveryEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="deliveryEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${deliveryEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="deliveryEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${deliveryEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="deliveryEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${deliveryEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="deliveryEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${deliveryEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="deliveryEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${deliveryEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="deliveryEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${deliveryEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="deliveryEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${deliveryEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="deliveryEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${deliveryEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="deliveryEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${deliveryEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="deliveryEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${deliveryEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="deliveryEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${deliveryEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="deliveryEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="deliveryEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="deliveryEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${deliveryEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deliveryEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="deliveryEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${deliveryEventInstance?.url}"/>
</div>

