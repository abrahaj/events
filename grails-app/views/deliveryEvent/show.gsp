
<%@ page import="com.shqiperia.entities.DeliveryEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'deliveryEvent.label', default: 'DeliveryEvent')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-deliveryEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-deliveryEvent" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list deliveryEvent">
			
				<g:if test="${deliveryEventInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="deliveryEvent.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${deliveryEventInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="deliveryEvent.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${deliveryEventInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="deliveryEvent.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${deliveryEventInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.attendee}">
				<li class="fieldcontain">
					<span id="attendee-label" class="property-label"><g:message code="deliveryEvent.attendee.label" default="Attendee" /></span>
					
						<g:each in="${deliveryEventInstance.attendee}" var="a">
						<span class="property-value" aria-labelledby="attendee-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="deliveryEvent.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${deliveryEventInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.doorTime}">
				<li class="fieldcontain">
					<span id="doorTime-label" class="property-label"><g:message code="deliveryEvent.doorTime.label" default="Door Time" /></span>
					
						<span class="property-value" aria-labelledby="doorTime-label"><g:formatDate date="${deliveryEventInstance?.doorTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="deliveryEvent.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:formatDate date="${deliveryEventInstance?.duration}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="deliveryEvent.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${deliveryEventInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.eventStatus}">
				<li class="fieldcontain">
					<span id="eventStatus-label" class="property-label"><g:message code="deliveryEvent.eventStatus.label" default="Event Status" /></span>
					
						<span class="property-value" aria-labelledby="eventStatus-label"><g:fieldValue bean="${deliveryEventInstance}" field="eventStatus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="deliveryEvent.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${deliveryEventInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="deliveryEvent.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:link controller="postaladdress" action="show" id="${deliveryEventInstance?.location?.id}">${deliveryEventInstance?.location?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.performer}">
				<li class="fieldcontain">
					<span id="performer-label" class="property-label"><g:message code="deliveryEvent.performer.label" default="Performer" /></span>
					
						<g:each in="${deliveryEventInstance.performer}" var="p">
						<span class="property-value" aria-labelledby="performer-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.previousStartDate}">
				<li class="fieldcontain">
					<span id="previousStartDate-label" class="property-label"><g:message code="deliveryEvent.previousStartDate.label" default="Previous Start Date" /></span>
					
						<span class="property-value" aria-labelledby="previousStartDate-label"><g:formatDate date="${deliveryEventInstance?.previousStartDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="deliveryEvent.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${deliveryEventInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="deliveryEvent.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${deliveryEventInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.subEvent}">
				<li class="fieldcontain">
					<span id="subEvent-label" class="property-label"><g:message code="deliveryEvent.subEvent.label" default="Sub Event" /></span>
					
						<g:each in="${deliveryEventInstance.subEvent}" var="s">
						<span class="property-value" aria-labelledby="subEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.superEvent}">
				<li class="fieldcontain">
					<span id="superEvent-label" class="property-label"><g:message code="deliveryEvent.superEvent.label" default="Super Event" /></span>
					
						<g:each in="${deliveryEventInstance.superEvent}" var="s">
						<span class="property-value" aria-labelledby="superEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="deliveryEvent.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${deliveryEventInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deliveryEventInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="deliveryEvent.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${deliveryEventInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:deliveryEventInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${deliveryEventInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
