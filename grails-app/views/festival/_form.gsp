<%@ page import="com.shqiperia.entities.Festival" %>



<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="festival.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${festivalInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="festival.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${festivalInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="festival.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${festivalInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="festival.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${festivalInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="festival.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${festivalInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="festival.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${festivalInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="festival.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${festivalInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="festival.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${festivalInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="festival.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${festivalInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="festival.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${festivalInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="festival.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${festivalInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="festival.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${festivalInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="festival.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${festivalInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="festival.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${festivalInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="festival.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${festivalInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="festival.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="festival.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="festival.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${festivalInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: festivalInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="festival.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${festivalInstance?.url}"/>
</div>

