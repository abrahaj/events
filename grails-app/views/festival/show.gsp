
<%@ page import="com.shqiperia.entities.Festival" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'festival.label', default: 'Festival')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-festival" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-festival" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list festival">
			
				<g:if test="${festivalInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="festival.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${festivalInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="festival.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${festivalInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="festival.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${festivalInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.attendee}">
				<li class="fieldcontain">
					<span id="attendee-label" class="property-label"><g:message code="festival.attendee.label" default="Attendee" /></span>
					
						<g:each in="${festivalInstance.attendee}" var="a">
						<span class="property-value" aria-labelledby="attendee-label"><g:link controller="person" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="festival.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${festivalInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.doorTime}">
				<li class="fieldcontain">
					<span id="doorTime-label" class="property-label"><g:message code="festival.doorTime.label" default="Door Time" /></span>
					
						<span class="property-value" aria-labelledby="doorTime-label"><g:formatDate date="${festivalInstance?.doorTime}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.duration}">
				<li class="fieldcontain">
					<span id="duration-label" class="property-label"><g:message code="festival.duration.label" default="Duration" /></span>
					
						<span class="property-value" aria-labelledby="duration-label"><g:formatDate date="${festivalInstance?.duration}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.endDate}">
				<li class="fieldcontain">
					<span id="endDate-label" class="property-label"><g:message code="festival.endDate.label" default="End Date" /></span>
					
						<span class="property-value" aria-labelledby="endDate-label"><g:formatDate date="${festivalInstance?.endDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.eventStatus}">
				<li class="fieldcontain">
					<span id="eventStatus-label" class="property-label"><g:message code="festival.eventStatus.label" default="Event Status" /></span>
					
						<span class="property-value" aria-labelledby="eventStatus-label"><g:fieldValue bean="${festivalInstance}" field="eventStatus"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="festival.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${festivalInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="festival.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:link controller="postaladdress" action="show" id="${festivalInstance?.location?.id}">${festivalInstance?.location?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.performer}">
				<li class="fieldcontain">
					<span id="performer-label" class="property-label"><g:message code="festival.performer.label" default="Performer" /></span>
					
						<g:each in="${festivalInstance.performer}" var="p">
						<span class="property-value" aria-labelledby="performer-label"><g:link controller="person" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.previousStartDate}">
				<li class="fieldcontain">
					<span id="previousStartDate-label" class="property-label"><g:message code="festival.previousStartDate.label" default="Previous Start Date" /></span>
					
						<span class="property-value" aria-labelledby="previousStartDate-label"><g:formatDate date="${festivalInstance?.previousStartDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="festival.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${festivalInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.startDate}">
				<li class="fieldcontain">
					<span id="startDate-label" class="property-label"><g:message code="festival.startDate.label" default="Start Date" /></span>
					
						<span class="property-value" aria-labelledby="startDate-label"><g:formatDate date="${festivalInstance?.startDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.subEvent}">
				<li class="fieldcontain">
					<span id="subEvent-label" class="property-label"><g:message code="festival.subEvent.label" default="Sub Event" /></span>
					
						<g:each in="${festivalInstance.subEvent}" var="s">
						<span class="property-value" aria-labelledby="subEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.superEvent}">
				<li class="fieldcontain">
					<span id="superEvent-label" class="property-label"><g:message code="festival.superEvent.label" default="Super Event" /></span>
					
						<g:each in="${festivalInstance.superEvent}" var="s">
						<span class="property-value" aria-labelledby="superEvent-label"><g:link controller="event" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.typicalAgeRange}">
				<li class="fieldcontain">
					<span id="typicalAgeRange-label" class="property-label"><g:message code="festival.typicalAgeRange.label" default="Typical Age Range" /></span>
					
						<span class="property-value" aria-labelledby="typicalAgeRange-label"><g:fieldValue bean="${festivalInstance}" field="typicalAgeRange"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${festivalInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="festival.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${festivalInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:festivalInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${festivalInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
