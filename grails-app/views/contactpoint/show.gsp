
<%@ page import="com.shqiperia.entities.Contactpoint" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'contactpoint.label', default: 'Contactpoint')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-contactpoint" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-contactpoint" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list contactpoint">
			
				<g:if test="${contactpointInstance?.email}">
				<li class="fieldcontain">
					<span id="email-label" class="property-label"><g:message code="contactpoint.email.label" default="Email" /></span>
					
						<span class="property-value" aria-labelledby="email-label"><g:fieldValue bean="${contactpointInstance}" field="email"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="contactpoint.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${contactpointInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.contactType}">
				<li class="fieldcontain">
					<span id="contactType-label" class="property-label"><g:message code="contactpoint.contactType.label" default="Contact Type" /></span>
					
						<span class="property-value" aria-labelledby="contactType-label"><g:fieldValue bean="${contactpointInstance}" field="contactType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="contactpoint.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${contactpointInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="contactpoint.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${contactpointInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="contactpoint.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${contactpointInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="contactpoint.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${contactpointInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="contactpoint.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${contactpointInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="contactpoint.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${contactpointInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${contactpointInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="contactpoint.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${contactpointInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:contactpointInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${contactpointInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
