<%@ page import="com.shqiperia.entities.Contactpoint" %>



<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'email', 'error')} ">
	<label for="email">
		<g:message code="contactpoint.email.label" default="Email" />
		
	</label>
	<g:field type="email" name="email" maxlength="250" value="${contactpointInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="contactpoint.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${contactpointInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'contactType', 'error')} ">
	<label for="contactType">
		<g:message code="contactpoint.contactType.label" default="Contact Type" />
		
	</label>
	<g:textField name="contactType" maxlength="250" value="${contactpointInstance?.contactType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="contactpoint.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${contactpointInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="contactpoint.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${contactpointInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="contactpoint.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${contactpointInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="contactpoint.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${contactpointInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="contactpoint.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${contactpointInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="contactpoint.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${contactpointInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contactpointInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="contactpoint.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${contactpointInstance?.url}"/>
</div>

