
<%@ page import="com.shqiperia.entities.Contactpoint" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'contactpoint.label', default: 'Contactpoint')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-contactpoint" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-contactpoint" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="email" title="${message(code: 'contactpoint.email.label', default: 'Email')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'contactpoint.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="contactType" title="${message(code: 'contactpoint.contactType.label', default: 'Contact Type')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'contactpoint.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="faxNumber" title="${message(code: 'contactpoint.faxNumber.label', default: 'Fax Number')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'contactpoint.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${contactpointInstanceList}" status="i" var="contactpointInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${contactpointInstance.id}">${fieldValue(bean: contactpointInstance, field: "email")}</g:link></td>
					
						<td>${fieldValue(bean: contactpointInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: contactpointInstance, field: "contactType")}</td>
					
						<td>${fieldValue(bean: contactpointInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: contactpointInstance, field: "faxNumber")}</td>
					
						<td>${fieldValue(bean: contactpointInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${contactpointInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
