
<%@ page import="com.shqiperia.entities.SocialEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'socialEvent.label', default: 'SocialEvent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-socialEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-socialEvent" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'socialEvent.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'socialEvent.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'socialEvent.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'socialEvent.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="doorTime" title="${message(code: 'socialEvent.doorTime.label', default: 'Door Time')}" />
					
						<g:sortableColumn property="duration" title="${message(code: 'socialEvent.duration.label', default: 'Duration')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${socialEventInstanceList}" status="i" var="socialEventInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${socialEventInstance.id}">${fieldValue(bean: socialEventInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: socialEventInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: socialEventInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: socialEventInstance, field: "description")}</td>
					
						<td><g:formatDate date="${socialEventInstance.doorTime}" /></td>
					
						<td><g:formatDate date="${socialEventInstance.duration}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${socialEventInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
