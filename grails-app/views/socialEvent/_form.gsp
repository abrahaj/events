<%@ page import="com.shqiperia.entities.SocialEvent" %>



<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="socialEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${socialEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="socialEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${socialEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="socialEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${socialEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="socialEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${socialEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="socialEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${socialEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="socialEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${socialEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="socialEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${socialEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="socialEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${socialEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="socialEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${socialEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="socialEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${socialEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="socialEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${socialEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="socialEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${socialEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="socialEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${socialEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="socialEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${socialEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="socialEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${socialEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="socialEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="socialEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="socialEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${socialEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: socialEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="socialEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${socialEventInstance?.url}"/>
</div>

