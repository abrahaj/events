<%@ page import="com.shqiperia.entities.VisualArtsEvent" %>



<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="visualArtsEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${visualArtsEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="visualArtsEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${visualArtsEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="visualArtsEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${visualArtsEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="visualArtsEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${visualArtsEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="visualArtsEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${visualArtsEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="visualArtsEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${visualArtsEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="visualArtsEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${visualArtsEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="visualArtsEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${visualArtsEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="visualArtsEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${visualArtsEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="visualArtsEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${visualArtsEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="visualArtsEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${visualArtsEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="visualArtsEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${visualArtsEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="visualArtsEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${visualArtsEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="visualArtsEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${visualArtsEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="visualArtsEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${visualArtsEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="visualArtsEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="visualArtsEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="visualArtsEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${visualArtsEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: visualArtsEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="visualArtsEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${visualArtsEventInstance?.url}"/>
</div>

