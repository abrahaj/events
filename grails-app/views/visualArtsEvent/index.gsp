
<%@ page import="com.shqiperia.entities.VisualArtsEvent" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'visualArtsEvent.label', default: 'VisualArtsEvent')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-visualArtsEvent" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-visualArtsEvent" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'visualArtsEvent.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="additionalType" title="${message(code: 'visualArtsEvent.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'visualArtsEvent.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'visualArtsEvent.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="doorTime" title="${message(code: 'visualArtsEvent.doorTime.label', default: 'Door Time')}" />
					
						<g:sortableColumn property="duration" title="${message(code: 'visualArtsEvent.duration.label', default: 'Duration')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${visualArtsEventInstanceList}" status="i" var="visualArtsEventInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${visualArtsEventInstance.id}">${fieldValue(bean: visualArtsEventInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: visualArtsEventInstance, field: "additionalType")}</td>
					
						<td>${fieldValue(bean: visualArtsEventInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: visualArtsEventInstance, field: "description")}</td>
					
						<td><g:formatDate date="${visualArtsEventInstance.doorTime}" /></td>
					
						<td><g:formatDate date="${visualArtsEventInstance.duration}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${visualArtsEventInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
