<%@ page import="com.shqiperia.entities.PublicationEvent" %>



<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="publicationEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${publicationEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="publicationEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${publicationEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="publicationEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${publicationEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="publicationEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${publicationEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="publicationEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${publicationEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="publicationEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${publicationEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="publicationEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${publicationEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="publicationEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${publicationEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="publicationEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${publicationEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="publicationEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${publicationEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="publicationEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${publicationEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="publicationEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${publicationEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="publicationEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${publicationEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="publicationEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${publicationEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="publicationEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${publicationEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="publicationEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="publicationEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="publicationEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${publicationEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: publicationEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="publicationEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${publicationEventInstance?.url}"/>
</div>

