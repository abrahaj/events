<%@ page import="com.shqiperia.entities.Thing" %>



<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="thing.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${thingInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="thing.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${thingInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="thing.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${thingInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="thing.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${thingInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="thing.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${thingInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="thing.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${thingInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: thingInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="thing.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${thingInstance?.url}"/>
</div>

