<%@ page import="com.shqiperia.entities.OpeningHoursSpecification" %>



<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="openingHoursSpecification.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${openingHoursSpecificationInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="openingHoursSpecification.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${openingHoursSpecificationInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'closes', 'error')} ">
	<label for="closes">
		<g:message code="openingHoursSpecification.closes.label" default="Closes" />
		
	</label>
	<g:datePicker name="closes" precision="minute"  value="${openingHoursSpecificationInstance?.closes}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'dayOfWeek', 'error')} ">
	<label for="dayOfWeek">
		<g:message code="openingHoursSpecification.dayOfWeek.label" default="Day Of Week" />
		
	</label>
	<g:select name="dayOfWeek" from="${DayOfWeek?.values()}" keys="${DayOfWeek.values()*.name()}" value="${openingHoursSpecificationInstance?.dayOfWeek?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="openingHoursSpecification.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${openingHoursSpecificationInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="openingHoursSpecification.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${openingHoursSpecificationInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="openingHoursSpecification.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${openingHoursSpecificationInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'opens', 'error')} ">
	<label for="opens">
		<g:message code="openingHoursSpecification.opens.label" default="Opens" />
		
	</label>
	<g:datePicker name="opens" precision="minute"  value="${openingHoursSpecificationInstance?.opens}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="openingHoursSpecification.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${openingHoursSpecificationInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="openingHoursSpecification.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${openingHoursSpecificationInstance?.url}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'validFrom', 'error')} ">
	<label for="validFrom">
		<g:message code="openingHoursSpecification.validFrom.label" default="Valid From" />
		
	</label>
	<g:datePicker name="validFrom" precision="day"  value="${openingHoursSpecificationInstance?.validFrom}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: openingHoursSpecificationInstance, field: 'validThrough', 'error')} ">
	<label for="validThrough">
		<g:message code="openingHoursSpecification.validThrough.label" default="Valid Through" />
		
	</label>
	<g:datePicker name="validThrough" precision="day"  value="${openingHoursSpecificationInstance?.validThrough}" default="none" noSelection="['': '']" />
</div>

