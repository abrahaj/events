
<%@ page import="com.shqiperia.entities.OpeningHoursSpecification" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'openingHoursSpecification.label', default: 'OpeningHoursSpecification')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-openingHoursSpecification" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-openingHoursSpecification" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list openingHoursSpecification">
			
				<g:if test="${openingHoursSpecificationInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="openingHoursSpecification.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="openingHoursSpecification.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.closes}">
				<li class="fieldcontain">
					<span id="closes-label" class="property-label"><g:message code="openingHoursSpecification.closes.label" default="Closes" /></span>
					
						<span class="property-value" aria-labelledby="closes-label"><g:formatDate date="${openingHoursSpecificationInstance?.closes}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.dayOfWeek}">
				<li class="fieldcontain">
					<span id="dayOfWeek-label" class="property-label"><g:message code="openingHoursSpecification.dayOfWeek.label" default="Day Of Week" /></span>
					
						<span class="property-value" aria-labelledby="dayOfWeek-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="dayOfWeek"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="openingHoursSpecification.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="openingHoursSpecification.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="openingHoursSpecification.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.opens}">
				<li class="fieldcontain">
					<span id="opens-label" class="property-label"><g:message code="openingHoursSpecification.opens.label" default="Opens" /></span>
					
						<span class="property-value" aria-labelledby="opens-label"><g:formatDate date="${openingHoursSpecificationInstance?.opens}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="openingHoursSpecification.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="openingHoursSpecification.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${openingHoursSpecificationInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.validFrom}">
				<li class="fieldcontain">
					<span id="validFrom-label" class="property-label"><g:message code="openingHoursSpecification.validFrom.label" default="Valid From" /></span>
					
						<span class="property-value" aria-labelledby="validFrom-label"><g:formatDate date="${openingHoursSpecificationInstance?.validFrom}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${openingHoursSpecificationInstance?.validThrough}">
				<li class="fieldcontain">
					<span id="validThrough-label" class="property-label"><g:message code="openingHoursSpecification.validThrough.label" default="Valid Through" /></span>
					
						<span class="property-value" aria-labelledby="validThrough-label"><g:formatDate date="${openingHoursSpecificationInstance?.validThrough}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:openingHoursSpecificationInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${openingHoursSpecificationInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
