
<%@ page import="com.shqiperia.entities.OpeningHoursSpecification" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'openingHoursSpecification.label', default: 'OpeningHoursSpecification')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-openingHoursSpecification" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-openingHoursSpecification" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'openingHoursSpecification.additionalType.label', default: 'Additional Type')}" />
					
						<g:sortableColumn property="alternateName" title="${message(code: 'openingHoursSpecification.alternateName.label', default: 'Alternate Name')}" />
					
						<g:sortableColumn property="closes" title="${message(code: 'openingHoursSpecification.closes.label', default: 'Closes')}" />
					
						<g:sortableColumn property="dayOfWeek" title="${message(code: 'openingHoursSpecification.dayOfWeek.label', default: 'Day Of Week')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'openingHoursSpecification.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="image" title="${message(code: 'openingHoursSpecification.image.label', default: 'Image')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${openingHoursSpecificationInstanceList}" status="i" var="openingHoursSpecificationInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${openingHoursSpecificationInstance.id}">${fieldValue(bean: openingHoursSpecificationInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: openingHoursSpecificationInstance, field: "alternateName")}</td>
					
						<td><g:formatDate date="${openingHoursSpecificationInstance.closes}" /></td>
					
						<td>${fieldValue(bean: openingHoursSpecificationInstance, field: "dayOfWeek")}</td>
					
						<td>${fieldValue(bean: openingHoursSpecificationInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: openingHoursSpecificationInstance, field: "image")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${openingHoursSpecificationInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
