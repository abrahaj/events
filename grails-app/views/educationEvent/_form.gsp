<%@ page import="com.shqiperia.entities.EducationEvent" %>



<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="educationEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${educationEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="educationEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${educationEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="educationEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${educationEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="educationEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${educationEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="educationEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${educationEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="educationEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${educationEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="educationEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${educationEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="educationEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${educationEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="educationEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${educationEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="educationEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${educationEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="educationEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${educationEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="educationEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${educationEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="educationEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${educationEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="educationEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${educationEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="educationEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${educationEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="educationEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="educationEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="educationEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${educationEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: educationEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="educationEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${educationEventInstance?.url}"/>
</div>

