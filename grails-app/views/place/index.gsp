
<%@ page import="com.shqiperia.entities.Place" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'place.label', default: 'Place')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-place" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-place" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="additionalType" title="${message(code: 'place.additionalType.label', default: 'Additional Type')}" />
					
						<th><g:message code="place.address.label" default="Address" /></th>
					
						<th><g:message code="place.aggregateRating.label" default="Aggregate Rating" /></th>
					
						<g:sortableColumn property="alternateName" title="${message(code: 'place.alternateName.label', default: 'Alternate Name')}" />
					
						<th><g:message code="place.containedIn.label" default="Contained In" /></th>
					
						<g:sortableColumn property="description" title="${message(code: 'place.description.label', default: 'Description')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${placeInstanceList}" status="i" var="placeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${placeInstance.id}">${fieldValue(bean: placeInstance, field: "additionalType")}</g:link></td>
					
						<td>${fieldValue(bean: placeInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: placeInstance, field: "aggregateRating")}</td>
					
						<td>${fieldValue(bean: placeInstance, field: "alternateName")}</td>
					
						<td>${fieldValue(bean: placeInstance, field: "containedIn")}</td>
					
						<td>${fieldValue(bean: placeInstance, field: "description")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${placeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
