<%@ page import="com.shqiperia.entities.Place" %>



<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="place.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${placeInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'address', 'error')} ">
	<label for="address">
		<g:message code="place.address.label" default="Address" />
		
	</label>
	<g:select id="address" name="address.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${placeInstance?.address?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'aggregateRating', 'error')} ">
	<label for="aggregateRating">
		<g:message code="place.aggregateRating.label" default="Aggregate Rating" />
		
	</label>
	<g:select id="aggregateRating" name="aggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${placeInstance?.aggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="place.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${placeInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'containedIn', 'error')} required">
	<label for="containedIn">
		<g:message code="place.containedIn.label" default="Contained In" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="containedIn" name="containedIn.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" required="" value="${placeInstance?.containedIn?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="place.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${placeInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'event', 'error')} ">
	<label for="event">
		<g:message code="place.event.label" default="Event" />
		
	</label>
	<g:select name="event" from="${com.shqiperia.entities.Event.list()}" multiple="multiple" optionKey="id" size="5" value="${placeInstance?.event*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'faxNumber', 'error')} ">
	<label for="faxNumber">
		<g:message code="place.faxNumber.label" default="Fax Number" />
		
	</label>
	<g:textField name="faxNumber" maxlength="250" value="${placeInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'geo', 'error')} ">
	<label for="geo">
		<g:message code="place.geo.label" default="Geo" />
		
	</label>
	<g:select id="geo" name="geo.id" from="${com.shqiperia.entities.GeoCoordinates.list()}" optionKey="id" value="${placeInstance?.geo?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'globalLocationNumber', 'error')} ">
	<label for="globalLocationNumber">
		<g:message code="place.globalLocationNumber.label" default="Global Location Number" />
		
	</label>
	<g:textField name="globalLocationNumber" maxlength="250" value="${placeInstance?.globalLocationNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="place.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${placeInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'interactionCount', 'error')} ">
	<label for="interactionCount">
		<g:message code="place.interactionCount.label" default="Interaction Count" />
		
	</label>
	<g:textField name="interactionCount" maxlength="250" value="${placeInstance?.interactionCount}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'isicV4', 'error')} ">
	<label for="isicV4">
		<g:message code="place.isicV4.label" default="Isic V4" />
		
	</label>
	<g:textField name="isicV4" maxlength="250" value="${placeInstance?.isicV4}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'logo', 'error')} ">
	<label for="logo">
		<g:message code="place.logo.label" default="Logo" />
		
	</label>
	<g:textField name="logo" maxlength="250" value="${placeInstance?.logo}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'map', 'error')} ">
	<label for="map">
		<g:message code="place.map.label" default="Map" />
		
	</label>
	<g:textField name="map" maxlength="250" value="${placeInstance?.map}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="place.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${placeInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'openingHoursSpecification', 'error')} ">
	<label for="openingHoursSpecification">
		<g:message code="place.openingHoursSpecification.label" default="Opening Hours Specification" />
		
	</label>
	<g:select id="openingHoursSpecification" name="openingHoursSpecification.id" from="${com.shqiperia.entities.OpeningHoursSpecification.list()}" optionKey="id" value="${placeInstance?.openingHoursSpecification?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'photo', 'error')} ">
	<label for="photo">
		<g:message code="place.photo.label" default="Photo" />
		
	</label>
	<g:select id="photo" name="photo.id" from="${com.shqiperia.entities.ImageObject.list()}" optionKey="id" value="${placeInstance?.photo?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'review', 'error')} ">
	<label for="review">
		<g:message code="place.review.label" default="Review" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${placeInstance?.review?}" var="r">
    <li><g:link controller="review" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="review" action="create" params="['place.id': placeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'review.label', default: 'Review')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="place.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${placeInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="place.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" maxlength="250" value="${placeInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: placeInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="place.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${placeInstance?.url}"/>
</div>

