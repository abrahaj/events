
<%@ page import="com.shqiperia.entities.Place" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'place.label', default: 'Place')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-place" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-place" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list place">
			
				<g:if test="${placeInstance?.additionalType}">
				<li class="fieldcontain">
					<span id="additionalType-label" class="property-label"><g:message code="place.additionalType.label" default="Additional Type" /></span>
					
						<span class="property-value" aria-labelledby="additionalType-label"><g:fieldValue bean="${placeInstance}" field="additionalType"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="place.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:link controller="postaladdress" action="show" id="${placeInstance?.address?.id}">${placeInstance?.address?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.aggregateRating}">
				<li class="fieldcontain">
					<span id="aggregateRating-label" class="property-label"><g:message code="place.aggregateRating.label" default="Aggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="aggregateRating-label"><g:link controller="aggregateRating" action="show" id="${placeInstance?.aggregateRating?.id}">${placeInstance?.aggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.alternateName}">
				<li class="fieldcontain">
					<span id="alternateName-label" class="property-label"><g:message code="place.alternateName.label" default="Alternate Name" /></span>
					
						<span class="property-value" aria-labelledby="alternateName-label"><g:fieldValue bean="${placeInstance}" field="alternateName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.containedIn}">
				<li class="fieldcontain">
					<span id="containedIn-label" class="property-label"><g:message code="place.containedIn.label" default="Contained In" /></span>
					
						<span class="property-value" aria-labelledby="containedIn-label"><g:link controller="place" action="show" id="${placeInstance?.containedIn?.id}">${placeInstance?.containedIn?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="place.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${placeInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.event}">
				<li class="fieldcontain">
					<span id="event-label" class="property-label"><g:message code="place.event.label" default="Event" /></span>
					
						<g:each in="${placeInstance.event}" var="e">
						<span class="property-value" aria-labelledby="event-label"><g:link controller="event" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.faxNumber}">
				<li class="fieldcontain">
					<span id="faxNumber-label" class="property-label"><g:message code="place.faxNumber.label" default="Fax Number" /></span>
					
						<span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue bean="${placeInstance}" field="faxNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.geo}">
				<li class="fieldcontain">
					<span id="geo-label" class="property-label"><g:message code="place.geo.label" default="Geo" /></span>
					
						<span class="property-value" aria-labelledby="geo-label"><g:link controller="geoCoordinates" action="show" id="${placeInstance?.geo?.id}">${placeInstance?.geo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.globalLocationNumber}">
				<li class="fieldcontain">
					<span id="globalLocationNumber-label" class="property-label"><g:message code="place.globalLocationNumber.label" default="Global Location Number" /></span>
					
						<span class="property-value" aria-labelledby="globalLocationNumber-label"><g:fieldValue bean="${placeInstance}" field="globalLocationNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.image}">
				<li class="fieldcontain">
					<span id="image-label" class="property-label"><g:message code="place.image.label" default="Image" /></span>
					
						<span class="property-value" aria-labelledby="image-label"><g:fieldValue bean="${placeInstance}" field="image"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.interactionCount}">
				<li class="fieldcontain">
					<span id="interactionCount-label" class="property-label"><g:message code="place.interactionCount.label" default="Interaction Count" /></span>
					
						<span class="property-value" aria-labelledby="interactionCount-label"><g:fieldValue bean="${placeInstance}" field="interactionCount"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.isicV4}">
				<li class="fieldcontain">
					<span id="isicV4-label" class="property-label"><g:message code="place.isicV4.label" default="Isic V4" /></span>
					
						<span class="property-value" aria-labelledby="isicV4-label"><g:fieldValue bean="${placeInstance}" field="isicV4"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.logo}">
				<li class="fieldcontain">
					<span id="logo-label" class="property-label"><g:message code="place.logo.label" default="Logo" /></span>
					
						<span class="property-value" aria-labelledby="logo-label"><g:fieldValue bean="${placeInstance}" field="logo"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.map}">
				<li class="fieldcontain">
					<span id="map-label" class="property-label"><g:message code="place.map.label" default="Map" /></span>
					
						<span class="property-value" aria-labelledby="map-label"><g:fieldValue bean="${placeInstance}" field="map"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="place.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${placeInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.openingHoursSpecification}">
				<li class="fieldcontain">
					<span id="openingHoursSpecification-label" class="property-label"><g:message code="place.openingHoursSpecification.label" default="Opening Hours Specification" /></span>
					
						<span class="property-value" aria-labelledby="openingHoursSpecification-label"><g:link controller="openingHoursSpecification" action="show" id="${placeInstance?.openingHoursSpecification?.id}">${placeInstance?.openingHoursSpecification?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.photo}">
				<li class="fieldcontain">
					<span id="photo-label" class="property-label"><g:message code="place.photo.label" default="Photo" /></span>
					
						<span class="property-value" aria-labelledby="photo-label"><g:link controller="imageObject" action="show" id="${placeInstance?.photo?.id}">${placeInstance?.photo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.review}">
				<li class="fieldcontain">
					<span id="review-label" class="property-label"><g:message code="place.review.label" default="Review" /></span>
					
						<g:each in="${placeInstance.review}" var="r">
						<span class="property-value" aria-labelledby="review-label"><g:link controller="review" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.sameAs}">
				<li class="fieldcontain">
					<span id="sameAs-label" class="property-label"><g:message code="place.sameAs.label" default="Same As" /></span>
					
						<span class="property-value" aria-labelledby="sameAs-label"><g:fieldValue bean="${placeInstance}" field="sameAs"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.telephone}">
				<li class="fieldcontain">
					<span id="telephone-label" class="property-label"><g:message code="place.telephone.label" default="Telephone" /></span>
					
						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${placeInstance}" field="telephone"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${placeInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="place.url.label" default="Url" /></span>
					
						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${placeInstance}" field="url"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:placeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${placeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
