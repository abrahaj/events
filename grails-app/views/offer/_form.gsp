<%@ page import="com.shqiperia.entities.Offer" %>



<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'acceptedPaymentMethod', 'error')} ">
	<label for="acceptedPaymentMethod">
		<g:message code="offer.acceptedPaymentMethod.label" default="Accepted Payment Method" />
		
	</label>
	<g:select name="acceptedPaymentMethod" from="${com.shqiperia.entities.Offer$PaymentMethod?.values()}" keys="${com.shqiperia.entities.Offer$PaymentMethod.values()*.name()}" value="${offerInstance?.acceptedPaymentMethod?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'addOn', 'error')} ">
	<label for="addOn">
		<g:message code="offer.addOn.label" default="Add On" />
		
	</label>
	<g:select name="addOn" from="${com.shqiperia.entities.Offer.list()}" multiple="multiple" optionKey="id" size="5" value="${offerInstance?.addOn*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'advanceBookingRequirement', 'error')} ">
	<label for="advanceBookingRequirement">
		<g:message code="offer.advanceBookingRequirement.label" default="Advance Booking Requirement" />
		
	</label>
	<g:select id="advanceBookingRequirement" name="advanceBookingRequirement.id" from="${com.shqiperia.entities.QuantitativeValue.list()}" optionKey="id" value="${offerInstance?.advanceBookingRequirement?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'availabilityEnds', 'error')} ">
	<label for="availabilityEnds">
		<g:message code="offer.availabilityEnds.label" default="Availability Ends" />
		
	</label>
	<g:datePicker name="availabilityEnds" precision="day"  value="${offerInstance?.availabilityEnds}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'availabilityStarts', 'error')} ">
	<label for="availabilityStarts">
		<g:message code="offer.availabilityStarts.label" default="Availability Starts" />
		
	</label>
	<g:datePicker name="availabilityStarts" precision="day"  value="${offerInstance?.availabilityStarts}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'availableAtOrFrom', 'error')} ">
	<label for="availableAtOrFrom">
		<g:message code="offer.availableAtOrFrom.label" default="Available At Or From" />
		
	</label>
	<g:select id="availableAtOrFrom" name="availableAtOrFrom.id" from="${com.shqiperia.entities.Place.list()}" optionKey="id" value="${offerInstance?.availableAtOrFrom?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'ggregateRating', 'error')} ">
	<label for="ggregateRating">
		<g:message code="offer.ggregateRating.label" default="Ggregate Rating" />
		
	</label>
	<g:select id="ggregateRating" name="ggregateRating.id" from="${com.shqiperia.entities.AggregateRating.list()}" optionKey="id" value="${offerInstance?.ggregateRating?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'gtin13', 'error')} ">
	<label for="gtin13">
		<g:message code="offer.gtin13.label" default="Gtin13" />
		
	</label>
	<g:textField name="gtin13" maxlength="250" value="${offerInstance?.gtin13}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'gtin14', 'error')} ">
	<label for="gtin14">
		<g:message code="offer.gtin14.label" default="Gtin14" />
		
	</label>
	<g:textField name="gtin14" maxlength="250" value="${offerInstance?.gtin14}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'gtin8', 'error')} ">
	<label for="gtin8">
		<g:message code="offer.gtin8.label" default="Gtin8" />
		
	</label>
	<g:textField name="gtin8" maxlength="250" value="${offerInstance?.gtin8}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'mpn', 'error')} ">
	<label for="mpn">
		<g:message code="offer.mpn.label" default="Mpn" />
		
	</label>
	<g:textField name="mpn" maxlength="250" value="${offerInstance?.mpn}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'price', 'error')} ">
	<label for="price">
		<g:message code="offer.price.label" default="Price" />
		
	</label>
	<g:field name="price" value="${fieldValue(bean: offerInstance, field: 'price')}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'priceCurrency', 'error')} ">
	<label for="priceCurrency">
		<g:message code="offer.priceCurrency.label" default="Price Currency" />
		
	</label>
	<g:textField name="priceCurrency" maxlength="250" value="${offerInstance?.priceCurrency}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'priceValidUntil', 'error')} ">
	<label for="priceValidUntil">
		<g:message code="offer.priceValidUntil.label" default="Price Valid Until" />
		
	</label>
	<g:datePicker name="priceValidUntil" precision="day"  value="${offerInstance?.priceValidUntil}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'serialNumber', 'error')} ">
	<label for="serialNumber">
		<g:message code="offer.serialNumber.label" default="Serial Number" />
		
	</label>
	<g:textField name="serialNumber" maxlength="250" value="${offerInstance?.serialNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'sku', 'error')} ">
	<label for="sku">
		<g:message code="offer.sku.label" default="Sku" />
		
	</label>
	<g:textField name="sku" maxlength="250" value="${offerInstance?.sku}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'validFrom', 'error')} ">
	<label for="validFrom">
		<g:message code="offer.validFrom.label" default="Valid From" />
		
	</label>
	<g:datePicker name="validFrom" precision="day"  value="${offerInstance?.validFrom}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: offerInstance, field: 'validThrough', 'error')} ">
	<label for="validThrough">
		<g:message code="offer.validThrough.label" default="Valid Through" />
		
	</label>
	<g:datePicker name="validThrough" precision="day"  value="${offerInstance?.validThrough}" default="none" noSelection="['': '']" />
</div>

