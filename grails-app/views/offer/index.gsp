
<%@ page import="com.shqiperia.entities.Offer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'offer.label', default: 'Offer')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-offer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-offer" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="acceptedPaymentMethod" title="${message(code: 'offer.acceptedPaymentMethod.label', default: 'Accepted Payment Method')}" />
					
						<th><g:message code="offer.advanceBookingRequirement.label" default="Advance Booking Requirement" /></th>
					
						<g:sortableColumn property="availabilityEnds" title="${message(code: 'offer.availabilityEnds.label', default: 'Availability Ends')}" />
					
						<g:sortableColumn property="availabilityStarts" title="${message(code: 'offer.availabilityStarts.label', default: 'Availability Starts')}" />
					
						<th><g:message code="offer.availableAtOrFrom.label" default="Available At Or From" /></th>
					
						<th><g:message code="offer.ggregateRating.label" default="Ggregate Rating" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${offerInstanceList}" status="i" var="offerInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${offerInstance.id}">${fieldValue(bean: offerInstance, field: "acceptedPaymentMethod")}</g:link></td>
					
						<td>${fieldValue(bean: offerInstance, field: "advanceBookingRequirement")}</td>
					
						<td><g:formatDate date="${offerInstance.availabilityEnds}" /></td>
					
						<td><g:formatDate date="${offerInstance.availabilityStarts}" /></td>
					
						<td>${fieldValue(bean: offerInstance, field: "availableAtOrFrom")}</td>
					
						<td>${fieldValue(bean: offerInstance, field: "ggregateRating")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${offerInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
