
<%@ page import="com.shqiperia.entities.Offer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'offer.label', default: 'Offer')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-offer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-offer" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list offer">
			
				<g:if test="${offerInstance?.acceptedPaymentMethod}">
				<li class="fieldcontain">
					<span id="acceptedPaymentMethod-label" class="property-label"><g:message code="offer.acceptedPaymentMethod.label" default="Accepted Payment Method" /></span>
					
						<span class="property-value" aria-labelledby="acceptedPaymentMethod-label"><g:fieldValue bean="${offerInstance}" field="acceptedPaymentMethod"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.addOn}">
				<li class="fieldcontain">
					<span id="addOn-label" class="property-label"><g:message code="offer.addOn.label" default="Add On" /></span>
					
						<g:each in="${offerInstance.addOn}" var="a">
						<span class="property-value" aria-labelledby="addOn-label"><g:link controller="offer" action="show" id="${a.id}">${a?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.advanceBookingRequirement}">
				<li class="fieldcontain">
					<span id="advanceBookingRequirement-label" class="property-label"><g:message code="offer.advanceBookingRequirement.label" default="Advance Booking Requirement" /></span>
					
						<span class="property-value" aria-labelledby="advanceBookingRequirement-label"><g:link controller="quantitativeValue" action="show" id="${offerInstance?.advanceBookingRequirement?.id}">${offerInstance?.advanceBookingRequirement?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.availabilityEnds}">
				<li class="fieldcontain">
					<span id="availabilityEnds-label" class="property-label"><g:message code="offer.availabilityEnds.label" default="Availability Ends" /></span>
					
						<span class="property-value" aria-labelledby="availabilityEnds-label"><g:formatDate date="${offerInstance?.availabilityEnds}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.availabilityStarts}">
				<li class="fieldcontain">
					<span id="availabilityStarts-label" class="property-label"><g:message code="offer.availabilityStarts.label" default="Availability Starts" /></span>
					
						<span class="property-value" aria-labelledby="availabilityStarts-label"><g:formatDate date="${offerInstance?.availabilityStarts}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.availableAtOrFrom}">
				<li class="fieldcontain">
					<span id="availableAtOrFrom-label" class="property-label"><g:message code="offer.availableAtOrFrom.label" default="Available At Or From" /></span>
					
						<span class="property-value" aria-labelledby="availableAtOrFrom-label"><g:link controller="place" action="show" id="${offerInstance?.availableAtOrFrom?.id}">${offerInstance?.availableAtOrFrom?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.ggregateRating}">
				<li class="fieldcontain">
					<span id="ggregateRating-label" class="property-label"><g:message code="offer.ggregateRating.label" default="Ggregate Rating" /></span>
					
						<span class="property-value" aria-labelledby="ggregateRating-label"><g:link controller="aggregateRating" action="show" id="${offerInstance?.ggregateRating?.id}">${offerInstance?.ggregateRating?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.gtin13}">
				<li class="fieldcontain">
					<span id="gtin13-label" class="property-label"><g:message code="offer.gtin13.label" default="Gtin13" /></span>
					
						<span class="property-value" aria-labelledby="gtin13-label"><g:fieldValue bean="${offerInstance}" field="gtin13"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.gtin14}">
				<li class="fieldcontain">
					<span id="gtin14-label" class="property-label"><g:message code="offer.gtin14.label" default="Gtin14" /></span>
					
						<span class="property-value" aria-labelledby="gtin14-label"><g:fieldValue bean="${offerInstance}" field="gtin14"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.gtin8}">
				<li class="fieldcontain">
					<span id="gtin8-label" class="property-label"><g:message code="offer.gtin8.label" default="Gtin8" /></span>
					
						<span class="property-value" aria-labelledby="gtin8-label"><g:fieldValue bean="${offerInstance}" field="gtin8"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.mpn}">
				<li class="fieldcontain">
					<span id="mpn-label" class="property-label"><g:message code="offer.mpn.label" default="Mpn" /></span>
					
						<span class="property-value" aria-labelledby="mpn-label"><g:fieldValue bean="${offerInstance}" field="mpn"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.price}">
				<li class="fieldcontain">
					<span id="price-label" class="property-label"><g:message code="offer.price.label" default="Price" /></span>
					
						<span class="property-value" aria-labelledby="price-label"><g:fieldValue bean="${offerInstance}" field="price"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.priceCurrency}">
				<li class="fieldcontain">
					<span id="priceCurrency-label" class="property-label"><g:message code="offer.priceCurrency.label" default="Price Currency" /></span>
					
						<span class="property-value" aria-labelledby="priceCurrency-label"><g:fieldValue bean="${offerInstance}" field="priceCurrency"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.priceValidUntil}">
				<li class="fieldcontain">
					<span id="priceValidUntil-label" class="property-label"><g:message code="offer.priceValidUntil.label" default="Price Valid Until" /></span>
					
						<span class="property-value" aria-labelledby="priceValidUntil-label"><g:formatDate date="${offerInstance?.priceValidUntil}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.serialNumber}">
				<li class="fieldcontain">
					<span id="serialNumber-label" class="property-label"><g:message code="offer.serialNumber.label" default="Serial Number" /></span>
					
						<span class="property-value" aria-labelledby="serialNumber-label"><g:fieldValue bean="${offerInstance}" field="serialNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.sku}">
				<li class="fieldcontain">
					<span id="sku-label" class="property-label"><g:message code="offer.sku.label" default="Sku" /></span>
					
						<span class="property-value" aria-labelledby="sku-label"><g:fieldValue bean="${offerInstance}" field="sku"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.validFrom}">
				<li class="fieldcontain">
					<span id="validFrom-label" class="property-label"><g:message code="offer.validFrom.label" default="Valid From" /></span>
					
						<span class="property-value" aria-labelledby="validFrom-label"><g:formatDate date="${offerInstance?.validFrom}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${offerInstance?.validThrough}">
				<li class="fieldcontain">
					<span id="validThrough-label" class="property-label"><g:message code="offer.validThrough.label" default="Valid Through" /></span>
					
						<span class="property-value" aria-labelledby="validThrough-label"><g:formatDate date="${offerInstance?.validThrough}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:offerInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${offerInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
