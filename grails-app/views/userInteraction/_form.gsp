<%@ page import="com.shqiperia.entities.UserInteraction" %>



<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="userInteraction.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${userInteractionInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="userInteraction.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${userInteractionInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="userInteraction.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${userInteractionInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="userInteraction.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${userInteractionInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="userInteraction.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${userInteractionInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="userInteraction.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${userInteractionInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="userInteraction.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${userInteractionInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="userInteraction.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${userInteractionInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="userInteraction.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${userInteractionInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="userInteraction.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${userInteractionInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="userInteraction.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${userInteractionInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="userInteraction.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${userInteractionInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="userInteraction.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${userInteractionInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="userInteraction.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${userInteractionInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="userInteraction.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${userInteractionInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="userInteraction.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="userInteraction.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="userInteraction.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${userInteractionInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInteractionInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="userInteraction.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${userInteractionInstance?.url}"/>
</div>

