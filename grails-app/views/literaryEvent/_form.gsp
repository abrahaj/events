<%@ page import="com.shqiperia.entities.LiteraryEvent" %>



<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="literaryEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${literaryEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="literaryEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${literaryEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="literaryEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${literaryEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="literaryEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${literaryEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="literaryEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${literaryEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="literaryEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${literaryEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="literaryEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${literaryEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="literaryEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${literaryEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="literaryEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${literaryEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="literaryEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${literaryEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="literaryEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${literaryEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="literaryEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${literaryEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="literaryEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${literaryEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="literaryEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${literaryEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="literaryEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${literaryEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="literaryEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="literaryEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="literaryEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${literaryEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: literaryEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="literaryEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${literaryEventInstance?.url}"/>
</div>

