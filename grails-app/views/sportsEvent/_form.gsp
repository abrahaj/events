<%@ page import="com.shqiperia.entities.SportsEvent" %>



<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="sportsEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${sportsEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="sportsEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${sportsEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="sportsEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${sportsEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="sportsEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${sportsEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="sportsEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${sportsEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="sportsEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${sportsEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="sportsEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${sportsEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="sportsEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${sportsEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="sportsEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${sportsEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="sportsEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${sportsEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="sportsEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${sportsEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="sportsEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${sportsEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="sportsEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${sportsEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="sportsEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${sportsEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="sportsEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${sportsEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="sportsEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="sportsEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="sportsEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${sportsEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: sportsEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="sportsEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${sportsEventInstance?.url}"/>
</div>

