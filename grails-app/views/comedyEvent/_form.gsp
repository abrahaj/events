<%@ page import="com.shqiperia.entities.ComedyEvent" %>



<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="comedyEvent.name.label" default="Name" />
		
	</label>
	<g:textField name="name" maxlength="250" value="${comedyEventInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'additionalType', 'error')} ">
	<label for="additionalType">
		<g:message code="comedyEvent.additionalType.label" default="Additional Type" />
		
	</label>
	<g:textField name="additionalType" maxlength="250" value="${comedyEventInstance?.additionalType}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'alternateName', 'error')} ">
	<label for="alternateName">
		<g:message code="comedyEvent.alternateName.label" default="Alternate Name" />
		
	</label>
	<g:textField name="alternateName" maxlength="250" value="${comedyEventInstance?.alternateName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'attendee', 'error')} ">
	<label for="attendee">
		<g:message code="comedyEvent.attendee.label" default="Attendee" />
		
	</label>
	<g:select name="attendee" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${comedyEventInstance?.attendee*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="comedyEvent.description.label" default="Description" />
		
	</label>
	<g:textField name="description" maxlength="250" value="${comedyEventInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'doorTime', 'error')} ">
	<label for="doorTime">
		<g:message code="comedyEvent.doorTime.label" default="Door Time" />
		
	</label>
	<g:datePicker name="doorTime" precision="day"  value="${comedyEventInstance?.doorTime}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'duration', 'error')} ">
	<label for="duration">
		<g:message code="comedyEvent.duration.label" default="Duration" />
		
	</label>
	<g:datePicker name="duration" precision="day"  value="${comedyEventInstance?.duration}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'endDate', 'error')} ">
	<label for="endDate">
		<g:message code="comedyEvent.endDate.label" default="End Date" />
		
	</label>
	<g:datePicker name="endDate" precision="day"  value="${comedyEventInstance?.endDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'eventStatus', 'error')} ">
	<label for="eventStatus">
		<g:message code="comedyEvent.eventStatus.label" default="Event Status" />
		
	</label>
	<g:select name="eventStatus" from="${com.shqiperia.entities.Event$EventStatusType?.values()}" keys="${com.shqiperia.entities.Event$EventStatusType.values()*.name()}" value="${comedyEventInstance?.eventStatus?.name()}" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'image', 'error')} ">
	<label for="image">
		<g:message code="comedyEvent.image.label" default="Image" />
		
	</label>
	<g:textField name="image" maxlength="250" value="${comedyEventInstance?.image}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'location', 'error')} ">
	<label for="location">
		<g:message code="comedyEvent.location.label" default="Location" />
		
	</label>
	<g:select id="location" name="location.id" from="${com.shqiperia.entities.Postaladdress.list()}" optionKey="id" value="${comedyEventInstance?.location?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'performer', 'error')} ">
	<label for="performer">
		<g:message code="comedyEvent.performer.label" default="Performer" />
		
	</label>
	<g:select name="performer" from="${com.shqiperia.entities.Person.list()}" multiple="multiple" optionKey="id" size="5" value="${comedyEventInstance?.performer*.id}" class="many-to-many"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'previousStartDate', 'error')} ">
	<label for="previousStartDate">
		<g:message code="comedyEvent.previousStartDate.label" default="Previous Start Date" />
		
	</label>
	<g:datePicker name="previousStartDate" precision="day"  value="${comedyEventInstance?.previousStartDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'sameAs', 'error')} ">
	<label for="sameAs">
		<g:message code="comedyEvent.sameAs.label" default="Same As" />
		
	</label>
	<g:textField name="sameAs" maxlength="250" value="${comedyEventInstance?.sameAs}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'startDate', 'error')} ">
	<label for="startDate">
		<g:message code="comedyEvent.startDate.label" default="Start Date" />
		
	</label>
	<g:datePicker name="startDate" precision="day"  value="${comedyEventInstance?.startDate}" default="none" noSelection="['': '']" />
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'subEvent', 'error')} ">
	<label for="subEvent">
		<g:message code="comedyEvent.subEvent.label" default="Sub Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'superEvent', 'error')} ">
	<label for="superEvent">
		<g:message code="comedyEvent.superEvent.label" default="Super Event" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'typicalAgeRange', 'error')} ">
	<label for="typicalAgeRange">
		<g:message code="comedyEvent.typicalAgeRange.label" default="Typical Age Range" />
		
	</label>
	<g:textField name="typicalAgeRange" maxlength="250" value="${comedyEventInstance?.typicalAgeRange}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: comedyEventInstance, field: 'url', 'error')} ">
	<label for="url">
		<g:message code="comedyEvent.url.label" default="Url" />
		
	</label>
	<g:textField name="url" maxlength="250" value="${comedyEventInstance?.url}"/>
</div>

