package com.shqiperia.entities

class PriceSpecification extends StructuredValue {
    String additionalType="http://schema.org/StructuredValue"
    QuantitativeValue eligibleQuantity   // QuantitativeValue   The interval and unit of measurement of ordering quantities for which the offer or price specification is valid. This allows e.g. specifying that a certain freight charge is valid only for a certain quantity.
    PriceSpecification eligibleTransactionVolume   //PriceSpecification  The transaction volume, in a monetary unit, for which the offer or price specification is valid, e.g. for indicating a minimal purchasing volume, to express free shipping above a certain order volume, or to limit the acceptance of credit cards to purchases to a certain minimal amount.
    Float maxPrice    //Number  The highest price if the price is a range.
    Float minPrice    //Number  The lowest price if the price is a range.
    Float price   //Number or Text  The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
    String priceCurrency   //Text    The currency (in 3-letter ISO 4217 format) of the offer price or a price component, when attached to PriceSpecification and its subtypes.
    Date validFrom   //DateTime    The date when the item becomes valid.
    Date validThrough    //DateTime    The end of the validity of offer, price specification, or opening hours data.
    boolean valueAddedTaxIncluded   //Boolean     Specifies whether the applicable value-added tax (VAT) is included in the price specification or not.

    static constraints = {
    }
}