package com.shqiperia.entities

class Rating extends Intangible{
	
	String additionalType="http://schema.org/Rating"
	Float bestRating=5	//	Number or Text 	The highest value allowed in this rating system. If bestRating is omitted, 5 is assumed.
	String ratingValue 	//Text 	The rating for the content.
	Float worstRating
    static constraints = {
    }
}
