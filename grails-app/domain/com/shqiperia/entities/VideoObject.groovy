package com.shqiperia.entities

class VideoObject extends MediaObject{

    String additionalType="http://schema.org/VideoObject"
    
    String caption     //Text    The caption for this object.
    String thumbnail   //ImageObject     Thumbnail image for an image or video.
    String transcript  //Text    If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
    String videoFrameSize  //Text    The frame size of the video.
    String videoQuality    //Text    The quality of the video.

    static constraints = {
    }
}
