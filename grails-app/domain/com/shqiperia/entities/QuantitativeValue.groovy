package com.shqiperia.entities

class QuantitativeValue extends StructuredValue {
	String additionalType="http://schema.org/QuantitativeValue"
	Float maxValue 	//Number 	The upper of the product characteristic.
	Float	minValue 	//Number 	The lower value of the product characteristic.
	String unitCode 	//Text 	The unit of measurement given using the UN/CEFACT Common Code (3 characters).
	Float value 	//Number 	The value of the product characteristic.
	StructuredValue valueReference

	static constraints = {
	}
}
