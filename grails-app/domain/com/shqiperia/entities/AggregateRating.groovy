package com.shqiperia.entities

class AggregateRating extends Rating{
	String additionalType="http://schema.org/AggregateRating"
	Thing itemReviewed 	//Thing 	The item that is being reviewed/rated.
	Float ratingCount 	//Number 	The count of total number of ratings.
	Integer reviewCount

    static constraints = {
    }
}
