package com.shqiperia.entities

class Audience extends Intangible{
    String additionalType="http://schema.org/Audience"   //URL   An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
    
    String audienceType    //Text    The target group associated with a given audience (e.g. veterans, car owners, musicians, etc.) domain: Audience Range: Text
    AdministrativeArea geographicArea  //AdministrativeArea  The geographic area associated with the audience.
    static constraints = {
    }
}
