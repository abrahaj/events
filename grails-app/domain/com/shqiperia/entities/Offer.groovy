package com.shqiperia.entities

class Offer {
    PaymentMethod acceptedPaymentMethod 	//PaymentMethod 	The payment method(s) accepted by seller for this offer.
    //HASMANY Offer addOn 	//Offer 	An additional offer that can only be obtained in combination with the first base offer (e.g. supplements and extensions that are available for a surcharge).
    QuantitativeValue advanceBookingRequirement 	//QuantitativeValue 	The amount of time that is required between accepting the offer and the actual usage of the resource or service.
    AggregateRating ggregateRating 	//AggregateRating 	The overall rating, based on a collection of reviews or ratings, of the item.
    ItemAvailability availability 	//ItemAvailability 	The availability of this item—for example In stock, Out of stock, Pre-order, etc.
    Date availabilityEnds 	//DateTime 	The end of the availability of the product or service included in the offer.
    Date availabilityStarts 	//DateTime 	The beginning of the availability of the product or service included in the offer.
    Place availableAtOrFrom 	//Place 	The place(s) from which the offer can be obtained (e.g. store locations).
    DeliveryMethod availableDeliveryMethod 	//DeliveryMethod 	The delivery method(s) available for this offer.
    BusinessFunction businessFunction 	//BusinessFunction 	The business function (e.g. sell, lease, repair, dispose) of the offer or component of a bundle (TypeAndQuantityNode). The default is http://purl.org/goodrelations/v1#Sell.
    String category 	//PhysicalActivityCategory or Text or Thing 	A category for the item. Greater signs or slashes can be used to informally indicate a category hierarchy.
    QuantitativeValue deliveryLeadTime 	//QuantitativeValue 	The typical delay between the receipt of the order and the goods leaving the warehouse.
    BusinessEntityType eligibleCustomerType 	//BusinessEntityType 	The type(s) of customers for which the given offer is valid.
    QuantitativeValue eligibleDuration 	//QuantitativeValue 	The duration for which the given offer is valid.
    QuantitativeValue eligibleQuantity 	//QuantitativeValue 	The interval and unit of measurement of ordering quantities for which the offer or price specification is valid. This allows e.g. specifying that a certain freight charge is valid only for a certain quantity.
    String eligibleRegion 	//GeoShape or Text 	The ISO 3166-1 (ISO 3166-1 alpha-2) or ISO 3166-2 code, or the GeoShape for the geo-political region(s) for which the offer or delivery charge specification is valid.
    PriceSpecification eligibleTransactionVolume 	//PriceSpecification 	The transaction volume, in a monetary unit, for which the offer or price specification is valid, e.g. for indicating a minimal purchasing volume, to express free shipping above a certain order volume, or to limit the acceptance of credit cards to purchases to a certain minimal amount.
    String gtin13 	//Text 	The GTIN-13 code of the product, or the product to which the offer refers. This is equivalent to 13-digit ISBN codes and EAN UCC-13. Former 12-digit UPC codes can be converted into a GTIN-13 code by simply adding a preceeding zero.
    String gtin14 	//Text 	The GTIN-14 code of the product, or the product to which the offer refers.
    String gtin8 	//Text 	The GTIN-8 code of the product, or the product to which the offer refers. This code is also known as EAN/UCC-8 or 8-digit EAN.
    TypeAndQuantityNode includesObject 	//TypeAndQuantityNode 	This links to a node or nodes indicating the exact quantity of the products included in the offer.
    QuantitativeValue inventoryLevel 	//QuantitativeValue 	The current approximate inventory level for the item or items.
    OfferItemCondition itemCondition 	//OfferItemCondition 	A predefined value from OfferItemCondition or a textual description of the condition of the product or service, or the products or services included in the offer.
    Product itemOffered 	//Product 	The item being sold.
    String mpn 	//Text 	The Manufacturer Part Number (MPN) of the product, or the product to which the offer refers.
    Float price 	//Number or Text 	The offer price of a product, or of a price component when attached to PriceSpecification and its subtypes.
    String priceCurrency 	//Text 	The currency (in 3-letter ISO 4217 format) of the offer price or a price component, when attached to PriceSpecification and its subtypes.
    PriceSpecification priceSpecification 	//PriceSpecification 	One or more detailed price specifications, indicating the unit price and delivery or payment charges.
    Date priceValidUntil 	//Date 	The date after which the price is no longer available.
    Review review 	//Review 	A review of the item.
    Organization seller 	//Organization or Person 	The seller.
    String serialNumber 	//Text 	The serial number or any alphanumeric identifier of a particular product. When attached to an offer, it is a shortcut for the serial number of the product included in the offer.
    String sku 	//Text 	The Stock Keeping Unit (SKU), i.e. a merchant-specific identifier for a product or service, or the product to which the offer refers.
    Date validFrom 	//DateTime 	The date when the item becomes valid.
    Date validThrough 	//DateTime 	The end of the validity of offer, price specification, or opening hours data.
    WarrantyPromise warranty 	//WarrantyPromise 	The warranty promise(s) included in the offer.

    static hasMany = [addOn:Offer]

    static constraints = {
    }

    enum PaymentMethod {
        BYBANKTRANSFERINADVANCE('http://purl.org/goodrelations/v1#ByBankTransferInAdvance'),
        BYINVOICE('http://purl.org/goodrelations/v1#ByInvoice'),
        CASH('http://purl.org/goodrelations/v1#Cash'),
        CHECKINADVANCE('http://purl.org/goodrelations/v1#CheckInAdvance'),
        COD('http://purl.org/goodrelations/v1#COD'),
        DIRECTDEBIT('http://purl.org/goodrelations/v1#DirectDebit'),
        PAYPAL('http://purl.org/goodrelations/v1#PayPal'),
        PAYSWARM('http://purl.org/goodrelations/v1#PaySwarm')
        String id

        PaymentMethod(String id) {
            this.id = id
        }
    }

    enum ItemAvailability {
        DISCONTINUED('Discontinued'),
        INSTOCK('InStock'),
        INSTOREONLY('InStoreOnly'),
        LIMITEDAVAILABILITY('LimitedAvailability'),
        ONLINEONLY('OnlineOnly'),
        OUTOFSTOCK('OutOfStock'),
        PREORDER('PreOrder'),
        SOLDOUT('SoldOut'),
        String id

        ItemAvailability(String id) {
            this.id = id
        }
    }

    enum DeliveryMethod{
        LOCKERDELIVERY('LockerDelivery'),
        ONSITEPICKUP('OnSitePickup'),
        PARCELSERVICE('ParcelService')
        String id
        DeliveryMethod(String id) {
            this.id = id
        }
    }

    enum BusinessFunction{
        CONSTRUCTIONINSTALLATION('http://purl.org/goodrelations/v1#ConstructionInstallation'),
        LEASEOUT('http://purl.org/goodrelations/v1#Dispose http://purl.org/goodrelations/v1#LeaseOut'),
        PROVIDESERVICE('http://purl.org/goodrelations/v1#ProvideService'),
        MAINTAIN('http://purl.org/goodrelations/v1#Maintain'),
        REPAIR('http://purl.org/goodrelations/v1#Repair'),
        SELL('http://purl.org/goodrelations/v1#Sell'),
        BUY('http://purl.org/goodrelations/v1#Buy')
        String id
        BusinessFunction(String id) {
            this.id = id
        }
    }

    enum BusinessEntityType{
        BUSINESS('http://purl.org/goodrelations/v1#Business'),
        ENDUSER('http://purl.org/goodrelations/v1#Enduser'),
        PUBLICINSTITUTION('http://purl.org/goodrelations/v1#PublicInstitution'),
        RESELLER('http://purl.org/goodrelations/v1#Reseller')
        String id
        BusinessEntityType(String id) {
            this.id = id
        }
    }
    
    enum OfferItemCondition{
        DAMAGECONDITION('DamagedCondition'),
        NEWCONDITION('NewCondition'),
        REFURBISHEDCONDITION('RefurbishedCondition'),
        USEDCONDITION('UsedCondition')
        String id
        OfferItemCondition(String id) {
            this.id = id
        }
    }
}
