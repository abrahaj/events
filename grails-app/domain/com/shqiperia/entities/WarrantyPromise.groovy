package com.shqiperia.entities

class WarrantyPromise extends StructuredValue {
    String additionalType="http://schema.org/WarrantyPromise"
    QuantitativeValue durationOfWarranty  //QuantitativeValue   The duration of the warranty promise. Common unitCode values are ANN for year, MON for months, or DAY for days.
    WarrantyScope warrantyScope   //WarrantyScope   The scope of the warranty promise.
    static constraints = {
    }

    enum WarrantyScope{
        LABORBRINGIN('http://purl.org/goodrelations/v1#Labor-BringIn'),
        PARTSANDLABORBRINGIN('http://purl.org/goodrelations/v1#PartsAndLabor-BringIn'),
        PARTSANDLABORPICKUP('http://purl.org/goodrelations/v1#PartsAndLabor-PickUp')
        String id

        WarrantyScope(String id) {
            this.id = id
        }
    }
}
