package com.shqiperia.entities

/**
 * Duration & Url in this file are moved to String
 * @author ajb
 *
 */
class CreativeWork {

    String name     //Text  The name of the item.
    String alternateName    //Text  An alias for the item.
    String image    //URL   URL of an image of the item.
    String sameAs   //URL   URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
    String url      //URL   URL of the item.
    String description  //Text  A short description of the item.
    String additionalType="http://schema.org/CreativeWork"     //URL   An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.

    
    String about   //Changed to String-Thing   The subject matter of the content.
    String accessibilityAPI    //Text    Indicates that the resource is compatible with the referenced accessibility API (WebSchemas wiki lists possible values).
    String accessibilityControl    //Text    Identifies input methods that are sufficient to fully control the described resource (WebSchemas wiki lists possible values).
    String accessibilityFeature    //Text    Content features of the resource, such as accessible media, alternatives and supported enhancements for accessibility (WebSchemas wiki lists possible values).
    String accessibilityHazard     //Text    A characteristic of the described resource that is physiologically dangerous to some users. Related to WCAG 2.0 guideline 2.3. (WebSchemas wiki lists possible values)
    //Person accountablePerson   //Person  Specifies the Person that is legally accountable for the CreativeWork.
    AggregateRating aggregateRating     //AggregateRating     The overall rating, based on a collection of reviews or ratings, of the item.
    String alternativeHeadline     //Text    A secondary title of the CreativeWork.
    MediaObject associatedMedia     //MediaObject     The media objects that encode this creative work. This property is a synonym for encodings.
    Audience audience    //Audience    The intended audience of the item, i.e. the group for whom the item was created.
    AudioObject audio   //AudioObject     An embedded audio object.
    //Person author  //Organization or Person  The author of this content. Please note that author is special in that HTML 5 provides a special mechanism for indicating authorship via the rel tag. That is equivalent to this and may be used interchangeably.
    String award   //Text    An award won by this person or for this creative work.
    CreativeWork citation    //CreativeWork or Text    A citation or reference to another creative work, such as another publication, web page, scholarly article, etc. NOTE: Candidate for promotion to ScholarlyArticle.
    UserComments comment     //UserComments    Comments, typically from users, on this CreativeWork.
    Place contentLocation     //Place   The location of the content.
    String contentRating   //Text    Official rating of a piece of content—for example,'MPAA PG-13'.
    //Person contributor     //Organization or Person  A secondary contributor to the CreativeWork.
    //Person copyrightHolder     //Organization or Person  The party holding the legal copyright to the CreativeWork.
    Integer copyrightYear   //Number  The year during which the claimed copyright for the CreativeWork was first asserted.
    //Person creator     //Organization or Person  The creator/author of this CreativeWork or UserComments. This is the same as the Author property for CreativeWork.
    Date dateCreated     //Date    The date on which the CreativeWork was created.
    Date dateModified    //Date    The date on which the CreativeWork was most recently modified.
    Date datePublished   //Date    Date of first broadcast/publication.
    String discussionUrl   //URL     A link to the page containing the comments of the CreativeWork.
    //Person editor  //Person  Specifies the Person who edited the CreativeWork.
    AlignmentObject educationalAlignment    //AlignmentObject     An alignment to an established educational framework.
    String educationalUse  //Text    The purpose of a work in the context of education; for example, 'assignment', 'group work'.
    MediaObject encoding    //MediaObject     A media object that encode this CreativeWork.
    String genre   //Text    Genre of the creative work
    String headline    //Text    Headline of the article
    String inLanguage  //Text    The language of the content. please use one of the language codes from the IETF BCP 47 standard.
    String interactionCount    //Text    A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
    String interactivityType   //Text    The predominant mode of learning supported by the learning resource. Acceptable values are 'active', 'expositive', or 'mixed'.
    String isBasedOnUrl    //URL     A resource that was used in the creation of this resource. This term can be repeated for multiple sources. For example, http://example.com/great-multiplication-intro.html
    boolean isFamilyFriendly    //Boolean     Indicates whether this content is family friendly.
    String keywords    //Text    The keywords/tags used to describe this content.
    String learningResourceType    //Text    The predominant type or kind characterizing the learning resource. For example, 'presentation', 'handout'.
    String mentions    //Thing   Indicates that the CreativeWork contains a reference to, but is not necessarily about a concept.
    Offer offers  //Offer   An offer to sell this item—for example, an offer to sell a product, the DVD of a movie, or tickets to an event.
    Person provider   // Organization or Person  The organization or agency that is providing the service.
    Organization publisher   //Organization    The publisher of the creative work.
    String publishingPrinciples    //URL     Link to page describing the editorial principles of the organization primarily responsible for the creation of the CreativeWork.
    Review review  //Review  A review of the item.
    Organization sourceOrganization  //Organization    The Organization on whose behalf the creator was working.
    String text    //Text    The textual content of this CreativeWork.
    String thumbnailUrl    //URL     A thumbnail image relevant to the Thing.
    String timeRequired    //Duration    Approximate or typical time it takes to work with or through this learning resource for the typical intended target audience, e.g. 'P30M', 'P1H25M'.
    String typicalAgeRange     //Text    The typical expected age range, e.g. '7-9', '11-'.
    String  version     //Number  The version of the CreativeWork embodied by a specified resource.
    //video   VideoObject     An embedded video object.
    
    static hasMany = [accountablePerson:Person, author:Person,contributor:Person,copyrightHolder:Person,creator:Person,editor:Person]
    static constraints = {
    }
}
