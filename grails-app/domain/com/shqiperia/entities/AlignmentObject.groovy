package com.shqiperia.entities

class AlignmentObject extends Intangible {
    String additionalType="http://schema.org/AlignmentObject" 
    
    String alignmentType   //Text    A category of alignment between the learning resource and the framework node. Recommended values include: 'assesses', 'teaches', 'requires', 'textComplexity', 'readingLevel', 'educationalSubject', and 'educationLevel'.
    String educationalFramework    //Text    The framework to which the resource being described is aligned.
    String targetDescription   //Text    The description of a node in an established educational framework.
    String targetName  //Text    The name of a node in an established educational framework.
    String targetUrl   //URL     The URL of a node in an established educational framework.
    
    static constraints = {
    }
}
