package com.shqiperia.entities

/**
 * 
 * @author ajb
 *
 */
class GeoCoordinates extends StructuredValue{
    
    String additionalType="http://schema.org/GeoCoordinates"
    
    String elevation   //Number or Text  The elevation of a location.
    String latitude    //Number or Text  The latitude of a location. For example 37.42242.
    String longitude   //Number or Text
    
    
    static constraints = {
    }
}
