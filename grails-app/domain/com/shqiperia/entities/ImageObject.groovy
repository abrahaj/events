package com.shqiperia.entities

import java.nio.MappedByteBuffer;

/**
 * 
 * @author ajb
 *
 */
class ImageObject extends MediaObject {
    String additionalType="http://schema.org/ImageObject"

    String caption     //Text    The caption for this object.
    String exifData    //Text    exif data for this object.
    Boolean representativeOfPage=false    //Boolean     Indicates whether this image is representative of the content of the page.
    //ImageObject thumbnail   //ImageObject     Thumbnail image for an image or video.
    
    static hasMany = [thumbnail:ImageObject]
    static mappedBy = [thumbnail:"thumbnail"]
    static constraints = {
    }
}
