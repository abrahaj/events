package com.shqiperia.entities

import java.util.Formatter.DateTime

class Event {

    String name 	//Text 	The name of the item.
    String alternateName	//Text 	An alias for the item.
    String image 	//URL 	URL of an image of the item.
    String sameAs 	//URL 	URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
    String url		//URL 	URL of the item.
    String description 	//Text 	A short description of the item.
    String additionalType="http://schema.org/Event" 	//URL 	An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.

    //Person attendee 	//Organization or Person 	A person or organization attending the event.
    Date	doorTime	 //SHOULD NORMALLY BE DATETIME, SEE http://stackoverflow.com/questions/11977386/grails-scaffold-generated-mysql-column-type-for-jodatime-datetime-field type, casted to Date! Time admission will commence
    Date duration 	//Duration 	The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format.
    Date endDate 	//Date 	The end date and time of the event (in ISO 8601 date format).
    EventStatusType eventStatus //EventCancelled  EventPostponed     EventRescheduled     EventScheduled
    Postaladdress location 	//Place or PostalAddress 	The location of the event, organization or action.
    //NOT ADDED YET offers 	Offer 	An offer to sell this item—for example, an offer to sell a product, the DVD of a movie, or tickets to an event.
    //Person performer 	//Organization or Person 	A performer at the event—for example, a presenter, musician, musical group or actor.
    Date previousStartDate	//Used in conjunction with eventStatus for rescheduled or cancelled events. This property contains the previously scheduled start date. For rescheduled events, the startDate property should be used for the newly scheduled start date. In the (rare) case of an event that has been postponed and rescheduled multiple times, this field may be repeated.

    Date startDate 	//Date 	The start date and time of the event (in ISO 8601 date format).
    //Event subEvent 	//Event 	An Event that is part of this event. For example, a conference event includes many presentations, each are a subEvent of the conference.
    //Event superEvent 	//Event 	An event that this event is a part of. For example, a collection of individual music performances might each have a music festival as their superEvent.
    String typicalAgeRange //The typical expected age range

    static hasMany = [attendee:Person, performer:Person, subEvent:Event,superEvent:Event,offer:Offer]
    //static mappedBy = [subEvent:"subEvent",superEvent: "superEvent",attendee:"attendee",performer:"performer"]

    static constraints = { name	nullable:false }

    static mapping = {
        name column: "name", comment:"The name of the event."
        //doorTime type: PersistenQtDateTime

    }

    enum EventStatusType {

        EVENTCANCELLED('EventCancelled'),
        EVENTPOSTPONED('EventPostponed'),
        EVENTRESCHEDULED('EventRescheduled'),
        EVENTSCHEDULED('EventScheduled')
        String id

        EventStatusType(String id) {
            this.id = id
        }
    }
}
