package com.shqiperia.entities

class Place {
	
	String name 	//Text 	The name of the item.
	String alternateName	//Text 	An alias for the item.
	String image 	//URL 	URL of an image of the item.
	String sameAs 	//URL 	URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
	String url		//URL 	URL of the item.
	String description 	//Text 	A short description of the item.
	String additionalType="http://schema.org/Place" 	//URL 	An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.

	
	Postaladdress address 	//PostalAddress 	Physical address of the item.
	AggregateRating aggregateRating 	//AggregateRating 	The overall rating, based on a collection of reviews or ratings, of the item.
	Place containedIn 	//Place 	The basic containment relation between places.
	//event 	Event 	Upcoming or past event associated with this place or organization.
	String faxNumber 	//Text 	The fax number.
	GeoCoordinates geo 	//GeoCoordinates or GeoShape 	The geo coordinates of the place.
	String globalLocationNumber 	//Text 	The Global Location Number (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
	String interactionCount 	//Text 	A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
	String isicV4 	//Text 	The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
	String logo 	//ImageObject or URL 	A logo associated with an organization.
	String map 	//URL 	A URL to a map of the place.
	OpeningHoursSpecification openingHoursSpecification 	//OpeningHoursSpecification 	The opening hours of a certain place.
	ImageObject photo 	//ImageObject or Photograph 	A photograph of this place.	
	//review 	Review 	A review of the item.
	
	String telephone //	Text
	
	static hasMany =[event:Event,map:String,review:Review]
    
    static constraints = {
    }
}
