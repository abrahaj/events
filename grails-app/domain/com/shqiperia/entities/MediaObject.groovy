package com.shqiperia.entities

/**
 * In this class, the Distance and Duration are considered as String
 * @author ajb
 *
 */
class MediaObject extends CreativeWork{
    String additionalType="http://schema.org/MediaObject"     //URL   An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
    
    //associatedArticle   NewsArticle     A NewsArticle associated with the Media Object.
    String bitrate     //Text    The bitrate of the media object.
    String contentSize     //Text    File size in (mega/kilo) bytes.
    String contentUrl  //URL     Actual bytes of the media object, for example the image file or video file. (previous spelling: contentURL)
    String duration    //Duration    The duration of the item (movie, audio recording, event, etc.) in ISO 8601 date format.
    String embedUrl    //URL     A URL pointing to a player for a specific video. In general, this is the information in the src element of an embed tag and should not be the same as the content of the loc tag. (previous spelling: embedURL)
    CreativeWork encodesCreativeWork     //CreativeWork    The creative work encoded by this media object
    String encodingFormat  //Text    mp3, mpeg4, etc.
    Date expires  //   Date    Date the content expires and is no longer useful or available. Useful for videos.
    String height  //Distance or QuantitativeValue   The height of the item.
    String playerType  //Text    Player type required—for example, Flash or Silverlight.
    Organization productionCompany   //Organization    The production company or studio that made the movie, tv/radio series, season, or episode, or media object.
    PublicationEvent publication     //PublicationEvent    A publication event associated with the episode, clip or media object.
    Place regionsAllowed  //Place   The regions where the media is allowed. If not specified, then it's assumed to be allowed everywhere. Specify the countries in ISO 3166 format.
    boolean requiresSubscription    //Boolean     Indicates if use of the media require a subscription (either paid or free). Allowed values are true or false (note that an earlier version had 'yes', 'no').
    Date uploadDate  //Date    Date when this media object was uploaded to this site.
    String width   //Distance or QuantitativeValue   The width of the item.
    static constraints = {
    }
}
