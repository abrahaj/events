package com.shqiperia.entities

import java.util.Date;

class TypeAndQuantityNode extends StructuredValue {
    String additionalType="http://schema.org/TypeAndQuantityNode"
    Float amountOfThisGood    //Number  The quantity of the goods included in the offer.
    BusinessFunction businessFunction    //BusinessFunction    The business function (e.g. sell, lease, repair, dispose) of the offer or component of a bundle (TypeAndQuantityNode). The default is http://purl.org/goodrelations/v1#Sell.
    Product typeOfGood  //Product     The product that this structured value is referring to.
    String unitCode    //Text
    static constraints = {
    }
    
    enum BusinessFunction{
        CONSTRUCTIONINSTALLATION('http://purl.org/goodrelations/v1#ConstructionInstallation'),
        LEASEOUT('http://purl.org/goodrelations/v1#Dispose http://purl.org/goodrelations/v1#LeaseOut'),
        PROVIDESERVICE('http://purl.org/goodrelations/v1#ProvideService'),
        MAINTAIN('http://purl.org/goodrelations/v1#Maintain'),
        REPAIR('http://purl.org/goodrelations/v1#Repair'),
        SELL('http://purl.org/goodrelations/v1#Sell'),
        BUY('http://purl.org/goodrelations/v1#Buy')
        String id
        BusinessFunction(String id) {
            this.id = id
        }
    }
}