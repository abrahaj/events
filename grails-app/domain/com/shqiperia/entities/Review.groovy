package com.shqiperia.entities
/**
 * The itemReviewed Property is changed to a String to be addressed by a URI
 * @author ajb
 *
 */
class Review extends CreativeWork {
    
    String additionalType="http://schema.org/Review"
    
    String itemReviewed    //Thing   The item that is being reviewed/rated.
    String reviewBody  //Text    The actual body of the review
    Rating reviewRating    //Rating  The rating given in this review. Note that reviews can themselves be rated. The reviewRating applies to rating given by the review. The aggregateRating property applies to the review itself, as a creative work.

    static constraints = {
    }
}
