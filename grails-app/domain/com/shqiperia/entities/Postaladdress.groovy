package com.shqiperia.entities

class Postaladdress extends Contactpoint{
	//String addressCountry 	//Country 	The country. For example, USA. You can also provide the two-letter ISO 3166-1 alpha-2 country code.
	String addressLocality 	//Text 	The locality. For example, Mountain View.
	String addressRegion 	//Text 	The region. For example, CA.
	String postalCode 	//Text 	The postal code. For example, 94043.
	String postOfficeBoxNumber 	//Text 	The post offce box number for PO box addresses.
	String streetAddress 	//Text 	The street address. For example, 1600 Amphitheatre Pkwy.
    static constraints = {
    }
}
