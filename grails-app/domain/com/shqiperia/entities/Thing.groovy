package com.shqiperia.entities

abstract class Thing {
	String name 	//Text 	The name of the item.
	String alternateName	//Text 	An alias for the item.
	String image 	//URL 	URL of an image of the item.
	String sameAs 	//URL 	URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
	String url		//URL 	URL of the item.
	String description 	//Text 	A short description of the item.
	String additionalType 	//URL 	An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
	
    static constraints = {
    }
}
