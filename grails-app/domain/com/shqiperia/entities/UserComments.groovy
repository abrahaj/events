package com.shqiperia.entities

/**
 * 
 * @author ajb
 *
 */
class UserComments extends Event{
    String additionalType="http://schema.org/UserComments"
    String commentText     //Text    The text of the UserComment.
    Date commentTime     //Date    The time at which the UserComment was made.
    Person creator     //Organization or Person  The creator/author of this CreativeWork or UserComments. This is the same as the Author property for CreativeWork.
    CreativeWork discusses   //CreativeWork    Specifies the CreativeWork associated with the UserComment.
    String replyToUrl  //URL

    static constraints = {
    }
}
