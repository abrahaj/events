package com.shqiperia.entities
/**
 * 
 * @author ajb
 *
 */
class AudioObject extends MediaObject{
    String additionalType="http://schema.org/AudioObject"
    String transcript  //Text    If this MediaObject is an AudioObject or VideoObject, the transcript of that object.
    
    static constraints = {
    }
}
