package com.shqiperia.entities

class Person{
	String name 	//Text 	The name of the item.
	String alternateName	//Text 	An alias for the item.
	String image 	//URL 	URL of an image of the item.
	String sameAs 	//URL 	URL of a reference Web page that unambiguously indicates the item's identity. E.g. the URL of the item's Wikipedia page, Freebase page, or official website.
	String url		//URL 	URL of the item.
	String description 	//Text 	A short description of the item.
	String additionalType="http://www.schema.org/Person" 	//URL 	An additional type for the item, typically used for adding more specific types from external vocabularies in microdata syntax. This is a relationship between something and a class that the thing is in. In RDFa syntax, it is better to use the native RDFa syntax - the 'typeof' attribute - for multiple types. Schema.org tools may have only weaker understanding of extra types, in particular those defined externally.
	
	String familyName 	//Text 	Family name. In the U.S., the last name of an Person. This can be used along with givenName instead of the Name property.
	String givenName 	//Text 	Given name. In the U.S., the first name of a Person. This can be used along with familyName instead of the Name property.
	String gender 	//Text 	Gender of the person.
	String email 	//Text 	Email address.
	
	String additionalName 	//Text 	An additional name for a Person, can be used for a middle name.
	Postaladdress address 	//PostalAddress 	//Physical address of the item.
	//Organization affiliation 	//Organization 	An organization that this person is affiliated with. For example, a school/university, a club, or a team.
	//EducationalOrganization alumniOf 	//EducationalOrganization 	An educational organizations that the person is an alumni of.
	String award 	//Text 	An award won by this person or for this creative work.
	Date birthDate 	//Date 	Date of birth.
	//brand 	Brand or Organization 	The brand(s) associated with a product or service, or the brand(s) maintained by an organization or business person.
	Date deathDate 	//Date 	Date of death.
	String duns 	//Text 	The Dun & Bradstreet DUNS number for identifying an organization or business person.
	String globalLocationNumber 	//Text 	The Global Location Number (GLN, sometimes also referred to as International Location Number or ILN) of the respective organization, person, or place. The GLN is a 13-digit number used to identify parties and physical locations.
	//hasPOS 	Place 	Points-of-Sales operated by the organization or person.
	String	homeLocation 	//ContactPoint or Place 	A contact location for a person's residence.
	String honorificPrefix 	//Text 	An honorific prefix preceding a Person's name such as Dr/Mrs/Mr.
	String honorificSuffix 	//Text 	An honorific suffix preceding a Person's name such as M.D. /PhD/MSCSW.
	String interactionCount 	//Text 	A count of a specific user interactions with this item—for example, 20 UserLikes, 5 UserComments, or 300 UserDownloads. The user interaction type should be one of the sub types of UserInteraction.
	String isicV4 	//Text 	The International Standard of Industrial Classification of All Economic Activities (ISIC), Revision 4 code for a particular organization, business person, or place.
	String jobTitle 	//Text 	The job title of the person (for example, Financial Manager).
	//makesOffer 	Offer 	A pointer to products or services offered by the organization or person.
	Organization memberOf 	//Organization 	An organization to which the person belongs.
	String naics 	//Text 	The North American Industry Classification System (NAICS) code for a particular organization or business person.
	String nationality 	//Country 	Nationality of the person.
	//owns 	OwnershipInfo or Product 	Products owned by the organization or person.
	//performerIn 	Event 	Event that this person is a performer or participant in.
	//seeks 	Demand 	A pointer to products or services sought by the organization or person (demand).
	String taxID 	//Text 	The Tax / Fiscal ID of the organization or person, e.g. the TIN in the US or the CIF/NIF in Spain.
	String telephone 	//Text 	The telephone number.
	String vatID 	//Text 	The Value-added Tax ID of the organisation or person.
	String	workLocation 	//ContactPoint or Place 	A contact location for a person's place of work.
	Organization worksFor 	//Organization

	static hasMany = [colleague:Person,follows:Person,knows:Person,parent:Person,sibling:Person,relatedTo:Person,spouse:Person,children:Person,affiliation:Organization,alumniOf:EducationalOrganization]
	static mappedBy = [colleague: "colleague",follows: "follows",knows:"knows",parent:"parent",sibling:"sibling",relatedTo:"relatedTo",spouse:"spouse",children:"children"]

    static constraints = {
		name	nullable:false
		additionalName nullable: false
        alternateName   nullable: false
		email	email:true
        image   image:false
		colleague	nullable:true
		workLocation widget: 'textarea'
		address widget: 'textarea'
		homeLocation widget: 'textarea'
		//gender	name(inList: ["M", "F", "NA"])
/*		spouse(validator: {value, obj, errors->
		      if(value.contains(obj) {
		        errors.rejectValue('childNodes', 'Cannot contain self')
		      }
		  }*/
    }
}
