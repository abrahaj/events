package com.shqiperia.entities

import java.sql.Date;
import java.sql.Time;
/**
 * In this file DateTime is exchanged for Date
 * @author ajb
 *
 */
class OpeningHoursSpecification extends StructuredValue{

    String additionalType="http://schema.org/OpeningHoursSpecification"
    
    Time closes  //Time    The closing hour of the place or service on the given day(s) of the week.
    DayOfWeek dayOfWeek   //DayOfWeek   The day of the week for which these opening hours are valid.
    Time opens   //Time    The opening hour of the place or service on the given day(s) of the week.
    Date validFrom   //DateTime    The date when the item becomes valid.
    Date validThrough    //DateTime    The end of the validity of offer, price specification, or opening hours data.
    
    static constraints = {
    }
}
