package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class OpeningHoursSpecificationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond OpeningHoursSpecification.list(params), model:[openingHoursSpecificationInstanceCount: OpeningHoursSpecification.count()]
    }

    def show(OpeningHoursSpecification openingHoursSpecificationInstance) {
        respond openingHoursSpecificationInstance
    }

    def create() {
        respond new OpeningHoursSpecification(params)
    }

    @Transactional
    def save(OpeningHoursSpecification openingHoursSpecificationInstance) {
        if (openingHoursSpecificationInstance == null) {
            notFound()
            return
        }

        if (openingHoursSpecificationInstance.hasErrors()) {
            respond openingHoursSpecificationInstance.errors, view:'create'
            return
        }

        openingHoursSpecificationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'openingHoursSpecificationInstance.label', default: 'OpeningHoursSpecification'), openingHoursSpecificationInstance.id])
                redirect openingHoursSpecificationInstance
            }
            '*' { respond openingHoursSpecificationInstance, [status: CREATED] }
        }
    }

    def edit(OpeningHoursSpecification openingHoursSpecificationInstance) {
        respond openingHoursSpecificationInstance
    }

    @Transactional
    def update(OpeningHoursSpecification openingHoursSpecificationInstance) {
        if (openingHoursSpecificationInstance == null) {
            notFound()
            return
        }

        if (openingHoursSpecificationInstance.hasErrors()) {
            respond openingHoursSpecificationInstance.errors, view:'edit'
            return
        }

        openingHoursSpecificationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'OpeningHoursSpecification.label', default: 'OpeningHoursSpecification'), openingHoursSpecificationInstance.id])
                redirect openingHoursSpecificationInstance
            }
            '*'{ respond openingHoursSpecificationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(OpeningHoursSpecification openingHoursSpecificationInstance) {

        if (openingHoursSpecificationInstance == null) {
            notFound()
            return
        }

        openingHoursSpecificationInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'OpeningHoursSpecification.label', default: 'OpeningHoursSpecification'), openingHoursSpecificationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'openingHoursSpecificationInstance.label', default: 'OpeningHoursSpecification'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
