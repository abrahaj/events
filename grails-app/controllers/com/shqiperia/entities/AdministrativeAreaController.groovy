package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AdministrativeAreaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AdministrativeArea.list(params), model:[administrativeAreaInstanceCount: AdministrativeArea.count()]
    }

    def show(AdministrativeArea administrativeAreaInstance) {
        respond administrativeAreaInstance
    }

    def create() {
        respond new AdministrativeArea(params)
    }

    @Transactional
    def save(AdministrativeArea administrativeAreaInstance) {
        if (administrativeAreaInstance == null) {
            notFound()
            return
        }

        if (administrativeAreaInstance.hasErrors()) {
            respond administrativeAreaInstance.errors, view:'create'
            return
        }

        administrativeAreaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'administrativeAreaInstance.label', default: 'AdministrativeArea'), administrativeAreaInstance.id])
                redirect administrativeAreaInstance
            }
            '*' { respond administrativeAreaInstance, [status: CREATED] }
        }
    }

    def edit(AdministrativeArea administrativeAreaInstance) {
        respond administrativeAreaInstance
    }

    @Transactional
    def update(AdministrativeArea administrativeAreaInstance) {
        if (administrativeAreaInstance == null) {
            notFound()
            return
        }

        if (administrativeAreaInstance.hasErrors()) {
            respond administrativeAreaInstance.errors, view:'edit'
            return
        }

        administrativeAreaInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AdministrativeArea.label', default: 'AdministrativeArea'), administrativeAreaInstance.id])
                redirect administrativeAreaInstance
            }
            '*'{ respond administrativeAreaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AdministrativeArea administrativeAreaInstance) {

        if (administrativeAreaInstance == null) {
            notFound()
            return
        }

        administrativeAreaInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AdministrativeArea.label', default: 'AdministrativeArea'), administrativeAreaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'administrativeAreaInstance.label', default: 'AdministrativeArea'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
