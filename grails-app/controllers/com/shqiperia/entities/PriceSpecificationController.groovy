package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PriceSpecificationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PriceSpecification.list(params), model:[priceSpecificationInstanceCount: PriceSpecification.count()]
    }

    def show(PriceSpecification priceSpecificationInstance) {
        respond priceSpecificationInstance
    }

    def create() {
        respond new PriceSpecification(params)
    }

    @Transactional
    def save(PriceSpecification priceSpecificationInstance) {
        if (priceSpecificationInstance == null) {
            notFound()
            return
        }

        if (priceSpecificationInstance.hasErrors()) {
            respond priceSpecificationInstance.errors, view:'create'
            return
        }

        priceSpecificationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'priceSpecificationInstance.label', default: 'PriceSpecification'), priceSpecificationInstance.id])
                redirect priceSpecificationInstance
            }
            '*' { respond priceSpecificationInstance, [status: CREATED] }
        }
    }

    def edit(PriceSpecification priceSpecificationInstance) {
        respond priceSpecificationInstance
    }

    @Transactional
    def update(PriceSpecification priceSpecificationInstance) {
        if (priceSpecificationInstance == null) {
            notFound()
            return
        }

        if (priceSpecificationInstance.hasErrors()) {
            respond priceSpecificationInstance.errors, view:'edit'
            return
        }

        priceSpecificationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PriceSpecification.label', default: 'PriceSpecification'), priceSpecificationInstance.id])
                redirect priceSpecificationInstance
            }
            '*'{ respond priceSpecificationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PriceSpecification priceSpecificationInstance) {

        if (priceSpecificationInstance == null) {
            notFound()
            return
        }

        priceSpecificationInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PriceSpecification.label', default: 'PriceSpecification'), priceSpecificationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'priceSpecificationInstance.label', default: 'PriceSpecification'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
