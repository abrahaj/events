package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SocialEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SocialEvent.list(params), model:[socialEventInstanceCount: SocialEvent.count()]
    }

    def show(SocialEvent socialEventInstance) {
        respond socialEventInstance
    }

    def create() {
        respond new SocialEvent(params)
    }

    @Transactional
    def save(SocialEvent socialEventInstance) {
        if (socialEventInstance == null) {
            notFound()
            return
        }

        if (socialEventInstance.hasErrors()) {
            respond socialEventInstance.errors, view:'create'
            return
        }

        socialEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'socialEventInstance.label', default: 'SocialEvent'), socialEventInstance.id])
                redirect socialEventInstance
            }
            '*' { respond socialEventInstance, [status: CREATED] }
        }
    }

    def edit(SocialEvent socialEventInstance) {
        respond socialEventInstance
    }

    @Transactional
    def update(SocialEvent socialEventInstance) {
        if (socialEventInstance == null) {
            notFound()
            return
        }

        if (socialEventInstance.hasErrors()) {
            respond socialEventInstance.errors, view:'edit'
            return
        }

        socialEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SocialEvent.label', default: 'SocialEvent'), socialEventInstance.id])
                redirect socialEventInstance
            }
            '*'{ respond socialEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SocialEvent socialEventInstance) {

        if (socialEventInstance == null) {
            notFound()
            return
        }

        socialEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SocialEvent.label', default: 'SocialEvent'), socialEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'socialEventInstance.label', default: 'SocialEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
