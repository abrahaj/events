package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EducationalOrganizationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EducationalOrganization.list(params), model:[educationalOrganizationInstanceCount: EducationalOrganization.count()]
    }

    def show(EducationalOrganization educationalOrganizationInstance) {
        respond educationalOrganizationInstance
    }

    def create() {
        respond new EducationalOrganization(params)
    }

    @Transactional
    def save(EducationalOrganization educationalOrganizationInstance) {
        if (educationalOrganizationInstance == null) {
            notFound()
            return
        }

        if (educationalOrganizationInstance.hasErrors()) {
            respond educationalOrganizationInstance.errors, view:'create'
            return
        }

        educationalOrganizationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'educationalOrganizationInstance.label', default: 'EducationalOrganization'), educationalOrganizationInstance.id])
                redirect educationalOrganizationInstance
            }
            '*' { respond educationalOrganizationInstance, [status: CREATED] }
        }
    }

    def edit(EducationalOrganization educationalOrganizationInstance) {
        respond educationalOrganizationInstance
    }

    @Transactional
    def update(EducationalOrganization educationalOrganizationInstance) {
        if (educationalOrganizationInstance == null) {
            notFound()
            return
        }

        if (educationalOrganizationInstance.hasErrors()) {
            respond educationalOrganizationInstance.errors, view:'edit'
            return
        }

        educationalOrganizationInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EducationalOrganization.label', default: 'EducationalOrganization'), educationalOrganizationInstance.id])
                redirect educationalOrganizationInstance
            }
            '*'{ respond educationalOrganizationInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EducationalOrganization educationalOrganizationInstance) {

        if (educationalOrganizationInstance == null) {
            notFound()
            return
        }

        educationalOrganizationInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EducationalOrganization.label', default: 'EducationalOrganization'), educationalOrganizationInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'educationalOrganizationInstance.label', default: 'EducationalOrganization'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
