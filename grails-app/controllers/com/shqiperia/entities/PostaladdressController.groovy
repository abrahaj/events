package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PostaladdressController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Postaladdress.list(params), model:[postaladdressInstanceCount: Postaladdress.count()]
    }

    def show(Postaladdress postaladdressInstance) {
        respond postaladdressInstance
    }

    def create() {
        respond new Postaladdress(params)
    }

    @Transactional
    def save(Postaladdress postaladdressInstance) {
        if (postaladdressInstance == null) {
            notFound()
            return
        }

        if (postaladdressInstance.hasErrors()) {
            respond postaladdressInstance.errors, view:'create'
            return
        }

        postaladdressInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'postaladdressInstance.label', default: 'Postaladdress'), postaladdressInstance.id])
                redirect postaladdressInstance
            }
            '*' { respond postaladdressInstance, [status: CREATED] }
        }
    }

    def edit(Postaladdress postaladdressInstance) {
        respond postaladdressInstance
    }

    @Transactional
    def update(Postaladdress postaladdressInstance) {
        if (postaladdressInstance == null) {
            notFound()
            return
        }

        if (postaladdressInstance.hasErrors()) {
            respond postaladdressInstance.errors, view:'edit'
            return
        }

        postaladdressInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Postaladdress.label', default: 'Postaladdress'), postaladdressInstance.id])
                redirect postaladdressInstance
            }
            '*'{ respond postaladdressInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Postaladdress postaladdressInstance) {

        if (postaladdressInstance == null) {
            notFound()
            return
        }

        postaladdressInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Postaladdress.label', default: 'Postaladdress'), postaladdressInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'postaladdressInstance.label', default: 'Postaladdress'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
