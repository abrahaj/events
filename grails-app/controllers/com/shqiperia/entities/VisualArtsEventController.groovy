package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VisualArtsEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond VisualArtsEvent.list(params), model:[visualArtsEventInstanceCount: VisualArtsEvent.count()]
    }

    def show(VisualArtsEvent visualArtsEventInstance) {
        respond visualArtsEventInstance
    }

    def create() {
        respond new VisualArtsEvent(params)
    }

    @Transactional
    def save(VisualArtsEvent visualArtsEventInstance) {
        if (visualArtsEventInstance == null) {
            notFound()
            return
        }

        if (visualArtsEventInstance.hasErrors()) {
            respond visualArtsEventInstance.errors, view:'create'
            return
        }

        visualArtsEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'visualArtsEventInstance.label', default: 'VisualArtsEvent'), visualArtsEventInstance.id])
                redirect visualArtsEventInstance
            }
            '*' { respond visualArtsEventInstance, [status: CREATED] }
        }
    }

    def edit(VisualArtsEvent visualArtsEventInstance) {
        respond visualArtsEventInstance
    }

    @Transactional
    def update(VisualArtsEvent visualArtsEventInstance) {
        if (visualArtsEventInstance == null) {
            notFound()
            return
        }

        if (visualArtsEventInstance.hasErrors()) {
            respond visualArtsEventInstance.errors, view:'edit'
            return
        }

        visualArtsEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'VisualArtsEvent.label', default: 'VisualArtsEvent'), visualArtsEventInstance.id])
                redirect visualArtsEventInstance
            }
            '*'{ respond visualArtsEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(VisualArtsEvent visualArtsEventInstance) {

        if (visualArtsEventInstance == null) {
            notFound()
            return
        }

        visualArtsEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'VisualArtsEvent.label', default: 'VisualArtsEvent'), visualArtsEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'visualArtsEventInstance.label', default: 'VisualArtsEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
