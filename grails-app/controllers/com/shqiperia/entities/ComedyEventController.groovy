package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ComedyEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ComedyEvent.list(params), model:[comedyEventInstanceCount: ComedyEvent.count()]
    }

    def show(ComedyEvent comedyEventInstance) {
        respond comedyEventInstance
    }

    def create() {
        respond new ComedyEvent(params)
    }

    @Transactional
    def save(ComedyEvent comedyEventInstance) {
        if (comedyEventInstance == null) {
            notFound()
            return
        }

        if (comedyEventInstance.hasErrors()) {
            respond comedyEventInstance.errors, view:'create'
            return
        }

        comedyEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'comedyEventInstance.label', default: 'ComedyEvent'), comedyEventInstance.id])
                redirect comedyEventInstance
            }
            '*' { respond comedyEventInstance, [status: CREATED] }
        }
    }

    def edit(ComedyEvent comedyEventInstance) {
        respond comedyEventInstance
    }

    @Transactional
    def update(ComedyEvent comedyEventInstance) {
        if (comedyEventInstance == null) {
            notFound()
            return
        }

        if (comedyEventInstance.hasErrors()) {
            respond comedyEventInstance.errors, view:'edit'
            return
        }

        comedyEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ComedyEvent.label', default: 'ComedyEvent'), comedyEventInstance.id])
                redirect comedyEventInstance
            }
            '*'{ respond comedyEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ComedyEvent comedyEventInstance) {

        if (comedyEventInstance == null) {
            notFound()
            return
        }

        comedyEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ComedyEvent.label', default: 'ComedyEvent'), comedyEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'comedyEventInstance.label', default: 'ComedyEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
