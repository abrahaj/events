package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DeliveryEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DeliveryEvent.list(params), model:[deliveryEventInstanceCount: DeliveryEvent.count()]
    }

    def show(DeliveryEvent deliveryEventInstance) {
        respond deliveryEventInstance
    }

    def create() {
        respond new DeliveryEvent(params)
    }

    @Transactional
    def save(DeliveryEvent deliveryEventInstance) {
        if (deliveryEventInstance == null) {
            notFound()
            return
        }

        if (deliveryEventInstance.hasErrors()) {
            respond deliveryEventInstance.errors, view:'create'
            return
        }

        deliveryEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'deliveryEventInstance.label', default: 'DeliveryEvent'), deliveryEventInstance.id])
                redirect deliveryEventInstance
            }
            '*' { respond deliveryEventInstance, [status: CREATED] }
        }
    }

    def edit(DeliveryEvent deliveryEventInstance) {
        respond deliveryEventInstance
    }

    @Transactional
    def update(DeliveryEvent deliveryEventInstance) {
        if (deliveryEventInstance == null) {
            notFound()
            return
        }

        if (deliveryEventInstance.hasErrors()) {
            respond deliveryEventInstance.errors, view:'edit'
            return
        }

        deliveryEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DeliveryEvent.label', default: 'DeliveryEvent'), deliveryEventInstance.id])
                redirect deliveryEventInstance
            }
            '*'{ respond deliveryEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DeliveryEvent deliveryEventInstance) {

        if (deliveryEventInstance == null) {
            notFound()
            return
        }

        deliveryEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DeliveryEvent.label', default: 'DeliveryEvent'), deliveryEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'deliveryEventInstance.label', default: 'DeliveryEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
