package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AudienceController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Audience.list(params), model:[audienceInstanceCount: Audience.count()]
    }

    def show(Audience audienceInstance) {
        respond audienceInstance
    }

    def create() {
        respond new Audience(params)
    }

    @Transactional
    def save(Audience audienceInstance) {
        if (audienceInstance == null) {
            notFound()
            return
        }

        if (audienceInstance.hasErrors()) {
            respond audienceInstance.errors, view:'create'
            return
        }

        audienceInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'audienceInstance.label', default: 'Audience'), audienceInstance.id])
                redirect audienceInstance
            }
            '*' { respond audienceInstance, [status: CREATED] }
        }
    }

    def edit(Audience audienceInstance) {
        respond audienceInstance
    }

    @Transactional
    def update(Audience audienceInstance) {
        if (audienceInstance == null) {
            notFound()
            return
        }

        if (audienceInstance.hasErrors()) {
            respond audienceInstance.errors, view:'edit'
            return
        }

        audienceInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Audience.label', default: 'Audience'), audienceInstance.id])
                redirect audienceInstance
            }
            '*'{ respond audienceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Audience audienceInstance) {

        if (audienceInstance == null) {
            notFound()
            return
        }

        audienceInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Audience.label', default: 'Audience'), audienceInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'audienceInstance.label', default: 'Audience'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
