package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FestivalController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Festival.list(params), model:[festivalInstanceCount: Festival.count()]
    }

    def show(Festival festivalInstance) {
        respond festivalInstance
    }

    def create() {
        respond new Festival(params)
    }

    @Transactional
    def save(Festival festivalInstance) {
        if (festivalInstance == null) {
            notFound()
            return
        }

        if (festivalInstance.hasErrors()) {
            respond festivalInstance.errors, view:'create'
            return
        }

        festivalInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'festivalInstance.label', default: 'Festival'), festivalInstance.id])
                redirect festivalInstance
            }
            '*' { respond festivalInstance, [status: CREATED] }
        }
    }

    def edit(Festival festivalInstance) {
        respond festivalInstance
    }

    @Transactional
    def update(Festival festivalInstance) {
        if (festivalInstance == null) {
            notFound()
            return
        }

        if (festivalInstance.hasErrors()) {
            respond festivalInstance.errors, view:'edit'
            return
        }

        festivalInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Festival.label', default: 'Festival'), festivalInstance.id])
                redirect festivalInstance
            }
            '*'{ respond festivalInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Festival festivalInstance) {

        if (festivalInstance == null) {
            notFound()
            return
        }

        festivalInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Festival.label', default: 'Festival'), festivalInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'festivalInstance.label', default: 'Festival'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
