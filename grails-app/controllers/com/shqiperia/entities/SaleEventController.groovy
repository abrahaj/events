package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SaleEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SaleEvent.list(params), model:[saleEventInstanceCount: SaleEvent.count()]
    }

    def show(SaleEvent saleEventInstance) {
        respond saleEventInstance
    }

    def create() {
        respond new SaleEvent(params)
    }

    @Transactional
    def save(SaleEvent saleEventInstance) {
        if (saleEventInstance == null) {
            notFound()
            return
        }

        if (saleEventInstance.hasErrors()) {
            respond saleEventInstance.errors, view:'create'
            return
        }

        saleEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'saleEventInstance.label', default: 'SaleEvent'), saleEventInstance.id])
                redirect saleEventInstance
            }
            '*' { respond saleEventInstance, [status: CREATED] }
        }
    }

    def edit(SaleEvent saleEventInstance) {
        respond saleEventInstance
    }

    @Transactional
    def update(SaleEvent saleEventInstance) {
        if (saleEventInstance == null) {
            notFound()
            return
        }

        if (saleEventInstance.hasErrors()) {
            respond saleEventInstance.errors, view:'edit'
            return
        }

        saleEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SaleEvent.label', default: 'SaleEvent'), saleEventInstance.id])
                redirect saleEventInstance
            }
            '*'{ respond saleEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SaleEvent saleEventInstance) {

        if (saleEventInstance == null) {
            notFound()
            return
        }

        saleEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SaleEvent.label', default: 'SaleEvent'), saleEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'saleEventInstance.label', default: 'SaleEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
