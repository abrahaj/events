package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MusicEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond MusicEvent.list(params), model:[musicEventInstanceCount: MusicEvent.count()]
    }

    def show(MusicEvent musicEventInstance) {
        respond musicEventInstance
    }

    def create() {
        respond new MusicEvent(params)
    }

    @Transactional
    def save(MusicEvent musicEventInstance) {
        if (musicEventInstance == null) {
            notFound()
            return
        }

        if (musicEventInstance.hasErrors()) {
            respond musicEventInstance.errors, view:'create'
            return
        }

        musicEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'musicEventInstance.label', default: 'MusicEvent'), musicEventInstance.id])
                redirect musicEventInstance
            }
            '*' { respond musicEventInstance, [status: CREATED] }
        }
    }

    def edit(MusicEvent musicEventInstance) {
        respond musicEventInstance
    }

    @Transactional
    def update(MusicEvent musicEventInstance) {
        if (musicEventInstance == null) {
            notFound()
            return
        }

        if (musicEventInstance.hasErrors()) {
            respond musicEventInstance.errors, view:'edit'
            return
        }

        musicEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MusicEvent.label', default: 'MusicEvent'), musicEventInstance.id])
                redirect musicEventInstance
            }
            '*'{ respond musicEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MusicEvent musicEventInstance) {

        if (musicEventInstance == null) {
            notFound()
            return
        }

        musicEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MusicEvent.label', default: 'MusicEvent'), musicEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'musicEventInstance.label', default: 'MusicEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
