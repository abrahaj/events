package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserCommentsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond UserComments.list(params), model:[userCommentsInstanceCount: UserComments.count()]
    }

    def show(UserComments userCommentsInstance) {
        respond userCommentsInstance
    }

    def create() {
        respond new UserComments(params)
    }

    @Transactional
    def save(UserComments userCommentsInstance) {
        if (userCommentsInstance == null) {
            notFound()
            return
        }

        if (userCommentsInstance.hasErrors()) {
            respond userCommentsInstance.errors, view:'create'
            return
        }

        userCommentsInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userCommentsInstance.label', default: 'UserComments'), userCommentsInstance.id])
                redirect userCommentsInstance
            }
            '*' { respond userCommentsInstance, [status: CREATED] }
        }
    }

    def edit(UserComments userCommentsInstance) {
        respond userCommentsInstance
    }

    @Transactional
    def update(UserComments userCommentsInstance) {
        if (userCommentsInstance == null) {
            notFound()
            return
        }

        if (userCommentsInstance.hasErrors()) {
            respond userCommentsInstance.errors, view:'edit'
            return
        }

        userCommentsInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserComments.label', default: 'UserComments'), userCommentsInstance.id])
                redirect userCommentsInstance
            }
            '*'{ respond userCommentsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserComments userCommentsInstance) {

        if (userCommentsInstance == null) {
            notFound()
            return
        }

        userCommentsInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserComments.label', default: 'UserComments'), userCommentsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userCommentsInstance.label', default: 'UserComments'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
