package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LiteraryEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond LiteraryEvent.list(params), model:[literaryEventInstanceCount: LiteraryEvent.count()]
    }

    def show(LiteraryEvent literaryEventInstance) {
        respond literaryEventInstance
    }

    def create() {
        respond new LiteraryEvent(params)
    }

    @Transactional
    def save(LiteraryEvent literaryEventInstance) {
        if (literaryEventInstance == null) {
            notFound()
            return
        }

        if (literaryEventInstance.hasErrors()) {
            respond literaryEventInstance.errors, view:'create'
            return
        }

        literaryEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'literaryEventInstance.label', default: 'LiteraryEvent'), literaryEventInstance.id])
                redirect literaryEventInstance
            }
            '*' { respond literaryEventInstance, [status: CREATED] }
        }
    }

    def edit(LiteraryEvent literaryEventInstance) {
        respond literaryEventInstance
    }

    @Transactional
    def update(LiteraryEvent literaryEventInstance) {
        if (literaryEventInstance == null) {
            notFound()
            return
        }

        if (literaryEventInstance.hasErrors()) {
            respond literaryEventInstance.errors, view:'edit'
            return
        }

        literaryEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LiteraryEvent.label', default: 'LiteraryEvent'), literaryEventInstance.id])
                redirect literaryEventInstance
            }
            '*'{ respond literaryEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LiteraryEvent literaryEventInstance) {

        if (literaryEventInstance == null) {
            notFound()
            return
        }

        literaryEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LiteraryEvent.label', default: 'LiteraryEvent'), literaryEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'literaryEventInstance.label', default: 'LiteraryEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
