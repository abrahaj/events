package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AudioObjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AudioObject.list(params), model:[audioObjectInstanceCount: AudioObject.count()]
    }

    def show(AudioObject audioObjectInstance) {
        respond audioObjectInstance
    }

    def create() {
        respond new AudioObject(params)
    }

    @Transactional
    def save(AudioObject audioObjectInstance) {
        if (audioObjectInstance == null) {
            notFound()
            return
        }

        if (audioObjectInstance.hasErrors()) {
            respond audioObjectInstance.errors, view:'create'
            return
        }

        audioObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'audioObjectInstance.label', default: 'AudioObject'), audioObjectInstance.id])
                redirect audioObjectInstance
            }
            '*' { respond audioObjectInstance, [status: CREATED] }
        }
    }

    def edit(AudioObject audioObjectInstance) {
        respond audioObjectInstance
    }

    @Transactional
    def update(AudioObject audioObjectInstance) {
        if (audioObjectInstance == null) {
            notFound()
            return
        }

        if (audioObjectInstance.hasErrors()) {
            respond audioObjectInstance.errors, view:'edit'
            return
        }

        audioObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AudioObject.label', default: 'AudioObject'), audioObjectInstance.id])
                redirect audioObjectInstance
            }
            '*'{ respond audioObjectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AudioObject audioObjectInstance) {

        if (audioObjectInstance == null) {
            notFound()
            return
        }

        audioObjectInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AudioObject.label', default: 'AudioObject'), audioObjectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'audioObjectInstance.label', default: 'AudioObject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
