package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ContactpointController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Contactpoint.list(params), model:[contactpointInstanceCount: Contactpoint.count()]
    }

    def show(Contactpoint contactpointInstance) {
        respond contactpointInstance
    }

    def create() {
        respond new Contactpoint(params)
    }

    @Transactional
    def save(Contactpoint contactpointInstance) {
        if (contactpointInstance == null) {
            notFound()
            return
        }

        if (contactpointInstance.hasErrors()) {
            respond contactpointInstance.errors, view:'create'
            return
        }

        contactpointInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contactpointInstance.label', default: 'Contactpoint'), contactpointInstance.id])
                redirect contactpointInstance
            }
            '*' { respond contactpointInstance, [status: CREATED] }
        }
    }

    def edit(Contactpoint contactpointInstance) {
        respond contactpointInstance
    }

    @Transactional
    def update(Contactpoint contactpointInstance) {
        if (contactpointInstance == null) {
            notFound()
            return
        }

        if (contactpointInstance.hasErrors()) {
            respond contactpointInstance.errors, view:'edit'
            return
        }

        contactpointInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Contactpoint.label', default: 'Contactpoint'), contactpointInstance.id])
                redirect contactpointInstance
            }
            '*'{ respond contactpointInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Contactpoint contactpointInstance) {

        if (contactpointInstance == null) {
            notFound()
            return
        }

        contactpointInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Contactpoint.label', default: 'Contactpoint'), contactpointInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contactpointInstance.label', default: 'Contactpoint'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
