package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AlignmentObjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AlignmentObject.list(params), model:[alignmentObjectInstanceCount: AlignmentObject.count()]
    }

    def show(AlignmentObject alignmentObjectInstance) {
        respond alignmentObjectInstance
    }

    def create() {
        respond new AlignmentObject(params)
    }

    @Transactional
    def save(AlignmentObject alignmentObjectInstance) {
        if (alignmentObjectInstance == null) {
            notFound()
            return
        }

        if (alignmentObjectInstance.hasErrors()) {
            respond alignmentObjectInstance.errors, view:'create'
            return
        }

        alignmentObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'alignmentObjectInstance.label', default: 'AlignmentObject'), alignmentObjectInstance.id])
                redirect alignmentObjectInstance
            }
            '*' { respond alignmentObjectInstance, [status: CREATED] }
        }
    }

    def edit(AlignmentObject alignmentObjectInstance) {
        respond alignmentObjectInstance
    }

    @Transactional
    def update(AlignmentObject alignmentObjectInstance) {
        if (alignmentObjectInstance == null) {
            notFound()
            return
        }

        if (alignmentObjectInstance.hasErrors()) {
            respond alignmentObjectInstance.errors, view:'edit'
            return
        }

        alignmentObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AlignmentObject.label', default: 'AlignmentObject'), alignmentObjectInstance.id])
                redirect alignmentObjectInstance
            }
            '*'{ respond alignmentObjectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AlignmentObject alignmentObjectInstance) {

        if (alignmentObjectInstance == null) {
            notFound()
            return
        }

        alignmentObjectInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AlignmentObject.label', default: 'AlignmentObject'), alignmentObjectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'alignmentObjectInstance.label', default: 'AlignmentObject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
