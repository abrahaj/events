package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TheaterEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TheaterEvent.list(params), model:[theaterEventInstanceCount: TheaterEvent.count()]
    }

    def show(TheaterEvent theaterEventInstance) {
        respond theaterEventInstance
    }

    def create() {
        respond new TheaterEvent(params)
    }

    @Transactional
    def save(TheaterEvent theaterEventInstance) {
        if (theaterEventInstance == null) {
            notFound()
            return
        }

        if (theaterEventInstance.hasErrors()) {
            respond theaterEventInstance.errors, view:'create'
            return
        }

        theaterEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'theaterEventInstance.label', default: 'TheaterEvent'), theaterEventInstance.id])
                redirect theaterEventInstance
            }
            '*' { respond theaterEventInstance, [status: CREATED] }
        }
    }

    def edit(TheaterEvent theaterEventInstance) {
        respond theaterEventInstance
    }

    @Transactional
    def update(TheaterEvent theaterEventInstance) {
        if (theaterEventInstance == null) {
            notFound()
            return
        }

        if (theaterEventInstance.hasErrors()) {
            respond theaterEventInstance.errors, view:'edit'
            return
        }

        theaterEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TheaterEvent.label', default: 'TheaterEvent'), theaterEventInstance.id])
                redirect theaterEventInstance
            }
            '*'{ respond theaterEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TheaterEvent theaterEventInstance) {

        if (theaterEventInstance == null) {
            notFound()
            return
        }

        theaterEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TheaterEvent.label', default: 'TheaterEvent'), theaterEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'theaterEventInstance.label', default: 'TheaterEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
