package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FoodEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond FoodEvent.list(params), model:[foodEventInstanceCount: FoodEvent.count()]
    }

    def show(FoodEvent foodEventInstance) {
        respond foodEventInstance
    }

    def create() {
        respond new FoodEvent(params)
    }

    @Transactional
    def save(FoodEvent foodEventInstance) {
        if (foodEventInstance == null) {
            notFound()
            return
        }

        if (foodEventInstance.hasErrors()) {
            respond foodEventInstance.errors, view:'create'
            return
        }

        foodEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'foodEventInstance.label', default: 'FoodEvent'), foodEventInstance.id])
                redirect foodEventInstance
            }
            '*' { respond foodEventInstance, [status: CREATED] }
        }
    }

    def edit(FoodEvent foodEventInstance) {
        respond foodEventInstance
    }

    @Transactional
    def update(FoodEvent foodEventInstance) {
        if (foodEventInstance == null) {
            notFound()
            return
        }

        if (foodEventInstance.hasErrors()) {
            respond foodEventInstance.errors, view:'edit'
            return
        }

        foodEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FoodEvent.label', default: 'FoodEvent'), foodEventInstance.id])
                redirect foodEventInstance
            }
            '*'{ respond foodEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(FoodEvent foodEventInstance) {

        if (foodEventInstance == null) {
            notFound()
            return
        }

        foodEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FoodEvent.label', default: 'FoodEvent'), foodEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodEventInstance.label', default: 'FoodEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
