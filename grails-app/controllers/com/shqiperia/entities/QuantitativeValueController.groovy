package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuantitativeValueController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond QuantitativeValue.list(params), model:[quantitativeValueInstanceCount: QuantitativeValue.count()]
    }

    def show(QuantitativeValue quantitativeValueInstance) {
        respond quantitativeValueInstance
    }

    def create() {
        respond new QuantitativeValue(params)
    }

    @Transactional
    def save(QuantitativeValue quantitativeValueInstance) {
        if (quantitativeValueInstance == null) {
            notFound()
            return
        }

        if (quantitativeValueInstance.hasErrors()) {
            respond quantitativeValueInstance.errors, view:'create'
            return
        }

        quantitativeValueInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'quantitativeValueInstance.label', default: 'QuantitativeValue'), quantitativeValueInstance.id])
                redirect quantitativeValueInstance
            }
            '*' { respond quantitativeValueInstance, [status: CREATED] }
        }
    }

    def edit(QuantitativeValue quantitativeValueInstance) {
        respond quantitativeValueInstance
    }

    @Transactional
    def update(QuantitativeValue quantitativeValueInstance) {
        if (quantitativeValueInstance == null) {
            notFound()
            return
        }

        if (quantitativeValueInstance.hasErrors()) {
            respond quantitativeValueInstance.errors, view:'edit'
            return
        }

        quantitativeValueInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'QuantitativeValue.label', default: 'QuantitativeValue'), quantitativeValueInstance.id])
                redirect quantitativeValueInstance
            }
            '*'{ respond quantitativeValueInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(QuantitativeValue quantitativeValueInstance) {

        if (quantitativeValueInstance == null) {
            notFound()
            return
        }

        quantitativeValueInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'QuantitativeValue.label', default: 'QuantitativeValue'), quantitativeValueInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'quantitativeValueInstance.label', default: 'QuantitativeValue'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
