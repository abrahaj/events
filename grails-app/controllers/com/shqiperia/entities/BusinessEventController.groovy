package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BusinessEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BusinessEvent.list(params), model:[businessEventInstanceCount: BusinessEvent.count()]
    }

    def show(BusinessEvent businessEventInstance) {
        respond businessEventInstance
    }

    def create() {
        respond new BusinessEvent(params)
    }

    @Transactional
    def save(BusinessEvent businessEventInstance) {
        if (businessEventInstance == null) {
            notFound()
            return
        }

        if (businessEventInstance.hasErrors()) {
            respond businessEventInstance.errors, view:'create'
            return
        }

        businessEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'businessEventInstance.label', default: 'BusinessEvent'), businessEventInstance.id])
                redirect businessEventInstance
            }
            '*' { respond businessEventInstance, [status: CREATED] }
        }
    }

    def edit(BusinessEvent businessEventInstance) {
        respond businessEventInstance
    }

    @Transactional
    def update(BusinessEvent businessEventInstance) {
        if (businessEventInstance == null) {
            notFound()
            return
        }

        if (businessEventInstance.hasErrors()) {
            respond businessEventInstance.errors, view:'edit'
            return
        }

        businessEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BusinessEvent.label', default: 'BusinessEvent'), businessEventInstance.id])
                redirect businessEventInstance
            }
            '*'{ respond businessEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BusinessEvent businessEventInstance) {

        if (businessEventInstance == null) {
            notFound()
            return
        }

        businessEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BusinessEvent.label', default: 'BusinessEvent'), businessEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'businessEventInstance.label', default: 'BusinessEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
