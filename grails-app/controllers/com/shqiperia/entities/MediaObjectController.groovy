package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MediaObjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond MediaObject.list(params), model:[mediaObjectInstanceCount: MediaObject.count()]
    }

    def show(MediaObject mediaObjectInstance) {
        respond mediaObjectInstance
    }

    def create() {
        respond new MediaObject(params)
    }

    @Transactional
    def save(MediaObject mediaObjectInstance) {
        if (mediaObjectInstance == null) {
            notFound()
            return
        }

        if (mediaObjectInstance.hasErrors()) {
            respond mediaObjectInstance.errors, view:'create'
            return
        }

        mediaObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mediaObjectInstance.label', default: 'MediaObject'), mediaObjectInstance.id])
                redirect mediaObjectInstance
            }
            '*' { respond mediaObjectInstance, [status: CREATED] }
        }
    }

    def edit(MediaObject mediaObjectInstance) {
        respond mediaObjectInstance
    }

    @Transactional
    def update(MediaObject mediaObjectInstance) {
        if (mediaObjectInstance == null) {
            notFound()
            return
        }

        if (mediaObjectInstance.hasErrors()) {
            respond mediaObjectInstance.errors, view:'edit'
            return
        }

        mediaObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MediaObject.label', default: 'MediaObject'), mediaObjectInstance.id])
                redirect mediaObjectInstance
            }
            '*'{ respond mediaObjectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MediaObject mediaObjectInstance) {

        if (mediaObjectInstance == null) {
            notFound()
            return
        }

        mediaObjectInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MediaObject.label', default: 'MediaObject'), mediaObjectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mediaObjectInstance.label', default: 'MediaObject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
