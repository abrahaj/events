package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SportsEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SportsEvent.list(params), model:[sportsEventInstanceCount: SportsEvent.count()]
    }

    def show(SportsEvent sportsEventInstance) {
        respond sportsEventInstance
    }

    def create() {
        respond new SportsEvent(params)
    }

    @Transactional
    def save(SportsEvent sportsEventInstance) {
        if (sportsEventInstance == null) {
            notFound()
            return
        }

        if (sportsEventInstance.hasErrors()) {
            respond sportsEventInstance.errors, view:'create'
            return
        }

        sportsEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'sportsEventInstance.label', default: 'SportsEvent'), sportsEventInstance.id])
                redirect sportsEventInstance
            }
            '*' { respond sportsEventInstance, [status: CREATED] }
        }
    }

    def edit(SportsEvent sportsEventInstance) {
        respond sportsEventInstance
    }

    @Transactional
    def update(SportsEvent sportsEventInstance) {
        if (sportsEventInstance == null) {
            notFound()
            return
        }

        if (sportsEventInstance.hasErrors()) {
            respond sportsEventInstance.errors, view:'edit'
            return
        }

        sportsEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SportsEvent.label', default: 'SportsEvent'), sportsEventInstance.id])
                redirect sportsEventInstance
            }
            '*'{ respond sportsEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SportsEvent sportsEventInstance) {

        if (sportsEventInstance == null) {
            notFound()
            return
        }

        sportsEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SportsEvent.label', default: 'SportsEvent'), sportsEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'sportsEventInstance.label', default: 'SportsEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
