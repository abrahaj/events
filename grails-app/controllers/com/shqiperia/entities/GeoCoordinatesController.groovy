package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class GeoCoordinatesController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond GeoCoordinates.list(params), model:[geoCoordinatesInstanceCount: GeoCoordinates.count()]
    }

    def show(GeoCoordinates geoCoordinatesInstance) {
        respond geoCoordinatesInstance
    }

    def create() {
        respond new GeoCoordinates(params)
    }

    @Transactional
    def save(GeoCoordinates geoCoordinatesInstance) {
        if (geoCoordinatesInstance == null) {
            notFound()
            return
        }

        if (geoCoordinatesInstance.hasErrors()) {
            respond geoCoordinatesInstance.errors, view:'create'
            return
        }

        geoCoordinatesInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'geoCoordinatesInstance.label', default: 'GeoCoordinates'), geoCoordinatesInstance.id])
                redirect geoCoordinatesInstance
            }
            '*' { respond geoCoordinatesInstance, [status: CREATED] }
        }
    }

    def edit(GeoCoordinates geoCoordinatesInstance) {
        respond geoCoordinatesInstance
    }

    @Transactional
    def update(GeoCoordinates geoCoordinatesInstance) {
        if (geoCoordinatesInstance == null) {
            notFound()
            return
        }

        if (geoCoordinatesInstance.hasErrors()) {
            respond geoCoordinatesInstance.errors, view:'edit'
            return
        }

        geoCoordinatesInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'GeoCoordinates.label', default: 'GeoCoordinates'), geoCoordinatesInstance.id])
                redirect geoCoordinatesInstance
            }
            '*'{ respond geoCoordinatesInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(GeoCoordinates geoCoordinatesInstance) {

        if (geoCoordinatesInstance == null) {
            notFound()
            return
        }

        geoCoordinatesInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'GeoCoordinates.label', default: 'GeoCoordinates'), geoCoordinatesInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'geoCoordinatesInstance.label', default: 'GeoCoordinates'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
