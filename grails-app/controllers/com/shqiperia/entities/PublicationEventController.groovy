package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PublicationEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond PublicationEvent.list(params), model:[publicationEventInstanceCount: PublicationEvent.count()]
    }

    def show(PublicationEvent publicationEventInstance) {
        respond publicationEventInstance
    }

    def create() {
        respond new PublicationEvent(params)
    }

    @Transactional
    def save(PublicationEvent publicationEventInstance) {
        if (publicationEventInstance == null) {
            notFound()
            return
        }

        if (publicationEventInstance.hasErrors()) {
            respond publicationEventInstance.errors, view:'create'
            return
        }

        publicationEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'publicationEventInstance.label', default: 'PublicationEvent'), publicationEventInstance.id])
                redirect publicationEventInstance
            }
            '*' { respond publicationEventInstance, [status: CREATED] }
        }
    }

    def edit(PublicationEvent publicationEventInstance) {
        respond publicationEventInstance
    }

    @Transactional
    def update(PublicationEvent publicationEventInstance) {
        if (publicationEventInstance == null) {
            notFound()
            return
        }

        if (publicationEventInstance.hasErrors()) {
            respond publicationEventInstance.errors, view:'edit'
            return
        }

        publicationEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'PublicationEvent.label', default: 'PublicationEvent'), publicationEventInstance.id])
                redirect publicationEventInstance
            }
            '*'{ respond publicationEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(PublicationEvent publicationEventInstance) {

        if (publicationEventInstance == null) {
            notFound()
            return
        }

        publicationEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'PublicationEvent.label', default: 'PublicationEvent'), publicationEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'publicationEventInstance.label', default: 'PublicationEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
