package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ChildrensEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ChildrensEvent.list(params), model:[childrensEventInstanceCount: ChildrensEvent.count()]
    }

    def show(ChildrensEvent childrensEventInstance) {
        respond childrensEventInstance
    }

    def create() {
        respond new ChildrensEvent(params)
    }

    @Transactional
    def save(ChildrensEvent childrensEventInstance) {
        if (childrensEventInstance == null) {
            notFound()
            return
        }

        if (childrensEventInstance.hasErrors()) {
            respond childrensEventInstance.errors, view:'create'
            return
        }

        childrensEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'childrensEventInstance.label', default: 'ChildrensEvent'), childrensEventInstance.id])
                redirect childrensEventInstance
            }
            '*' { respond childrensEventInstance, [status: CREATED] }
        }
    }

    def edit(ChildrensEvent childrensEventInstance) {
        respond childrensEventInstance
    }

    @Transactional
    def update(ChildrensEvent childrensEventInstance) {
        if (childrensEventInstance == null) {
            notFound()
            return
        }

        if (childrensEventInstance.hasErrors()) {
            respond childrensEventInstance.errors, view:'edit'
            return
        }

        childrensEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ChildrensEvent.label', default: 'ChildrensEvent'), childrensEventInstance.id])
                redirect childrensEventInstance
            }
            '*'{ respond childrensEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ChildrensEvent childrensEventInstance) {

        if (childrensEventInstance == null) {
            notFound()
            return
        }

        childrensEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ChildrensEvent.label', default: 'ChildrensEvent'), childrensEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'childrensEventInstance.label', default: 'ChildrensEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
