package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserInteractionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond UserInteraction.list(params), model:[userInteractionInstanceCount: UserInteraction.count()]
    }

    def show(UserInteraction userInteractionInstance) {
        respond userInteractionInstance
    }

    def create() {
        respond new UserInteraction(params)
    }

    @Transactional
    def save(UserInteraction userInteractionInstance) {
        if (userInteractionInstance == null) {
            notFound()
            return
        }

        if (userInteractionInstance.hasErrors()) {
            respond userInteractionInstance.errors, view:'create'
            return
        }

        userInteractionInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userInteractionInstance.label', default: 'UserInteraction'), userInteractionInstance.id])
                redirect userInteractionInstance
            }
            '*' { respond userInteractionInstance, [status: CREATED] }
        }
    }

    def edit(UserInteraction userInteractionInstance) {
        respond userInteractionInstance
    }

    @Transactional
    def update(UserInteraction userInteractionInstance) {
        if (userInteractionInstance == null) {
            notFound()
            return
        }

        if (userInteractionInstance.hasErrors()) {
            respond userInteractionInstance.errors, view:'edit'
            return
        }

        userInteractionInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserInteraction.label', default: 'UserInteraction'), userInteractionInstance.id])
                redirect userInteractionInstance
            }
            '*'{ respond userInteractionInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserInteraction userInteractionInstance) {

        if (userInteractionInstance == null) {
            notFound()
            return
        }

        userInteractionInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserInteraction.label', default: 'UserInteraction'), userInteractionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInteractionInstance.label', default: 'UserInteraction'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
