package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class StructuredValueController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond StructuredValue.list(params), model:[structuredValueInstanceCount: StructuredValue.count()]
    }

    def show(StructuredValue structuredValueInstance) {
        respond structuredValueInstance
    }

    def create() {
        respond new StructuredValue(params)
    }

    @Transactional
    def save(StructuredValue structuredValueInstance) {
        if (structuredValueInstance == null) {
            notFound()
            return
        }

        if (structuredValueInstance.hasErrors()) {
            respond structuredValueInstance.errors, view:'create'
            return
        }

        structuredValueInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'structuredValueInstance.label', default: 'StructuredValue'), structuredValueInstance.id])
                redirect structuredValueInstance
            }
            '*' { respond structuredValueInstance, [status: CREATED] }
        }
    }

    def edit(StructuredValue structuredValueInstance) {
        respond structuredValueInstance
    }

    @Transactional
    def update(StructuredValue structuredValueInstance) {
        if (structuredValueInstance == null) {
            notFound()
            return
        }

        if (structuredValueInstance.hasErrors()) {
            respond structuredValueInstance.errors, view:'edit'
            return
        }

        structuredValueInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'StructuredValue.label', default: 'StructuredValue'), structuredValueInstance.id])
                redirect structuredValueInstance
            }
            '*'{ respond structuredValueInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(StructuredValue structuredValueInstance) {

        if (structuredValueInstance == null) {
            notFound()
            return
        }

        structuredValueInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'StructuredValue.label', default: 'StructuredValue'), structuredValueInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'structuredValueInstance.label', default: 'StructuredValue'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
