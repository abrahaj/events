package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ImageObjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ImageObject.list(params), model:[imageObjectInstanceCount: ImageObject.count()]
    }

    def show(ImageObject imageObjectInstance) {
        respond imageObjectInstance
    }

    def create() {
        respond new ImageObject(params)
    }

    @Transactional
    def save(ImageObject imageObjectInstance) {
        if (imageObjectInstance == null) {
            notFound()
            return
        }

        if (imageObjectInstance.hasErrors()) {
            respond imageObjectInstance.errors, view:'create'
            return
        }

        imageObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'imageObjectInstance.label', default: 'ImageObject'), imageObjectInstance.id])
                redirect imageObjectInstance
            }
            '*' { respond imageObjectInstance, [status: CREATED] }
        }
    }

    def edit(ImageObject imageObjectInstance) {
        respond imageObjectInstance
    }

    @Transactional
    def update(ImageObject imageObjectInstance) {
        if (imageObjectInstance == null) {
            notFound()
            return
        }

        if (imageObjectInstance.hasErrors()) {
            respond imageObjectInstance.errors, view:'edit'
            return
        }

        imageObjectInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ImageObject.label', default: 'ImageObject'), imageObjectInstance.id])
                redirect imageObjectInstance
            }
            '*'{ respond imageObjectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ImageObject imageObjectInstance) {

        if (imageObjectInstance == null) {
            notFound()
            return
        }

        imageObjectInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ImageObject.label', default: 'ImageObject'), imageObjectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'imageObjectInstance.label', default: 'ImageObject'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
