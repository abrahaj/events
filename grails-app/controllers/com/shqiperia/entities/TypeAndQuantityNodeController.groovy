package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TypeAndQuantityNodeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TypeAndQuantityNode.list(params), model:[typeAndQuantityNodeInstanceCount: TypeAndQuantityNode.count()]
    }

    def show(TypeAndQuantityNode typeAndQuantityNodeInstance) {
        respond typeAndQuantityNodeInstance
    }

    def create() {
        respond new TypeAndQuantityNode(params)
    }

    @Transactional
    def save(TypeAndQuantityNode typeAndQuantityNodeInstance) {
        if (typeAndQuantityNodeInstance == null) {
            notFound()
            return
        }

        if (typeAndQuantityNodeInstance.hasErrors()) {
            respond typeAndQuantityNodeInstance.errors, view:'create'
            return
        }

        typeAndQuantityNodeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'typeAndQuantityNodeInstance.label', default: 'TypeAndQuantityNode'), typeAndQuantityNodeInstance.id])
                redirect typeAndQuantityNodeInstance
            }
            '*' { respond typeAndQuantityNodeInstance, [status: CREATED] }
        }
    }

    def edit(TypeAndQuantityNode typeAndQuantityNodeInstance) {
        respond typeAndQuantityNodeInstance
    }

    @Transactional
    def update(TypeAndQuantityNode typeAndQuantityNodeInstance) {
        if (typeAndQuantityNodeInstance == null) {
            notFound()
            return
        }

        if (typeAndQuantityNodeInstance.hasErrors()) {
            respond typeAndQuantityNodeInstance.errors, view:'edit'
            return
        }

        typeAndQuantityNodeInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TypeAndQuantityNode.label', default: 'TypeAndQuantityNode'), typeAndQuantityNodeInstance.id])
                redirect typeAndQuantityNodeInstance
            }
            '*'{ respond typeAndQuantityNodeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(TypeAndQuantityNode typeAndQuantityNodeInstance) {

        if (typeAndQuantityNodeInstance == null) {
            notFound()
            return
        }

        typeAndQuantityNodeInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TypeAndQuantityNode.label', default: 'TypeAndQuantityNode'), typeAndQuantityNodeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeAndQuantityNodeInstance.label', default: 'TypeAndQuantityNode'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
