package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EducationEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EducationEvent.list(params), model:[educationEventInstanceCount: EducationEvent.count()]
    }

    def show(EducationEvent educationEventInstance) {
        respond educationEventInstance
    }

    def create() {
        respond new EducationEvent(params)
    }

    @Transactional
    def save(EducationEvent educationEventInstance) {
        if (educationEventInstance == null) {
            notFound()
            return
        }

        if (educationEventInstance.hasErrors()) {
            respond educationEventInstance.errors, view:'create'
            return
        }

        educationEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'educationEventInstance.label', default: 'EducationEvent'), educationEventInstance.id])
                redirect educationEventInstance
            }
            '*' { respond educationEventInstance, [status: CREATED] }
        }
    }

    def edit(EducationEvent educationEventInstance) {
        respond educationEventInstance
    }

    @Transactional
    def update(EducationEvent educationEventInstance) {
        if (educationEventInstance == null) {
            notFound()
            return
        }

        if (educationEventInstance.hasErrors()) {
            respond educationEventInstance.errors, view:'edit'
            return
        }

        educationEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EducationEvent.label', default: 'EducationEvent'), educationEventInstance.id])
                redirect educationEventInstance
            }
            '*'{ respond educationEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EducationEvent educationEventInstance) {

        if (educationEventInstance == null) {
            notFound()
            return
        }

        educationEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EducationEvent.label', default: 'EducationEvent'), educationEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'educationEventInstance.label', default: 'EducationEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
