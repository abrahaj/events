package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class WarrantyPromiseController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond WarrantyPromise.list(params), model:[warrantyPromiseInstanceCount: WarrantyPromise.count()]
    }

    def show(WarrantyPromise warrantyPromiseInstance) {
        respond warrantyPromiseInstance
    }

    def create() {
        respond new WarrantyPromise(params)
    }

    @Transactional
    def save(WarrantyPromise warrantyPromiseInstance) {
        if (warrantyPromiseInstance == null) {
            notFound()
            return
        }

        if (warrantyPromiseInstance.hasErrors()) {
            respond warrantyPromiseInstance.errors, view:'create'
            return
        }

        warrantyPromiseInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'warrantyPromiseInstance.label', default: 'WarrantyPromise'), warrantyPromiseInstance.id])
                redirect warrantyPromiseInstance
            }
            '*' { respond warrantyPromiseInstance, [status: CREATED] }
        }
    }

    def edit(WarrantyPromise warrantyPromiseInstance) {
        respond warrantyPromiseInstance
    }

    @Transactional
    def update(WarrantyPromise warrantyPromiseInstance) {
        if (warrantyPromiseInstance == null) {
            notFound()
            return
        }

        if (warrantyPromiseInstance.hasErrors()) {
            respond warrantyPromiseInstance.errors, view:'edit'
            return
        }

        warrantyPromiseInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'WarrantyPromise.label', default: 'WarrantyPromise'), warrantyPromiseInstance.id])
                redirect warrantyPromiseInstance
            }
            '*'{ respond warrantyPromiseInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(WarrantyPromise warrantyPromiseInstance) {

        if (warrantyPromiseInstance == null) {
            notFound()
            return
        }

        warrantyPromiseInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'WarrantyPromise.label', default: 'WarrantyPromise'), warrantyPromiseInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'warrantyPromiseInstance.label', default: 'WarrantyPromise'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
