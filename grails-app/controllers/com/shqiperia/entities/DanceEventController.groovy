package com.shqiperia.entities



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class DanceEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond DanceEvent.list(params), model:[danceEventInstanceCount: DanceEvent.count()]
    }

    def show(DanceEvent danceEventInstance) {
        respond danceEventInstance
    }

    def create() {
        respond new DanceEvent(params)
    }

    @Transactional
    def save(DanceEvent danceEventInstance) {
        if (danceEventInstance == null) {
            notFound()
            return
        }

        if (danceEventInstance.hasErrors()) {
            respond danceEventInstance.errors, view:'create'
            return
        }

        danceEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'danceEventInstance.label', default: 'DanceEvent'), danceEventInstance.id])
                redirect danceEventInstance
            }
            '*' { respond danceEventInstance, [status: CREATED] }
        }
    }

    def edit(DanceEvent danceEventInstance) {
        respond danceEventInstance
    }

    @Transactional
    def update(DanceEvent danceEventInstance) {
        if (danceEventInstance == null) {
            notFound()
            return
        }

        if (danceEventInstance.hasErrors()) {
            respond danceEventInstance.errors, view:'edit'
            return
        }

        danceEventInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'DanceEvent.label', default: 'DanceEvent'), danceEventInstance.id])
                redirect danceEventInstance
            }
            '*'{ respond danceEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(DanceEvent danceEventInstance) {

        if (danceEventInstance == null) {
            notFound()
            return
        }

        danceEventInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'DanceEvent.label', default: 'DanceEvent'), danceEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'danceEventInstance.label', default: 'DanceEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
