-- MySQL dump 10.13  Distrib 5.6.12, for Linux (x86_64)
--
-- Host: localhost    Database: entities
-- ------------------------------------------------------
-- Server version	5.6.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contactpoint`
--

DROP TABLE IF EXISTS `contactpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contactpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `additional_type` varchar(250) DEFAULT NULL,
  `contact_type` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `fax_number` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `same_as` varchar(250) DEFAULT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  `address_locality` varchar(250) DEFAULT NULL,
  `address_region` varchar(250) DEFAULT NULL,
  `post_office_box_number` varchar(250) DEFAULT NULL,
  `postal_code` varchar(250) DEFAULT NULL,
  `street_address` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `additional_type` varchar(250) DEFAULT NULL,
  `alternate_name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `door_time` datetime DEFAULT NULL,
  `duration` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  `name` varchar(250) NOT NULL COMMENT 'The name of the event.',
  `previous_start_date` datetime DEFAULT NULL,
  `same_as` varchar(250) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `typical_age_range` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5C6729A2556D6B4` (`location_id`),
  CONSTRAINT `FK5C6729A2556D6B4` FOREIGN KEY (`location_id`) REFERENCES `contactpoint` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_event`
--

DROP TABLE IF EXISTS `event_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_event` (
  `event_sub_event_id` bigint(20) DEFAULT NULL,
  `event_id` bigint(20) DEFAULT NULL,
  `event_super_event_id` bigint(20) DEFAULT NULL,
  KEY `FK1A0B6CB5EBF70180` (`event_id`),
  KEY `FK1A0B6CB52899FE24` (`event_sub_event_id`),
  KEY `FK1A0B6CB542FA3209` (`event_super_event_id`),
  CONSTRAINT `FK1A0B6CB542FA3209` FOREIGN KEY (`event_super_event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK1A0B6CB52899FE24` FOREIGN KEY (`event_sub_event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK1A0B6CB5EBF70180` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_person`
--

DROP TABLE IF EXISTS `event_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_person` (
  `event_attendee_id` bigint(20) DEFAULT NULL,
  `person_id` bigint(20) DEFAULT NULL,
  `event_performer_id` bigint(20) DEFAULT NULL,
  KEY `FK393DE49AC8E5AD3B` (`event_attendee_id`),
  KEY `FK393DE49AD5BB2854` (`person_id`),
  KEY `FK393DE49A5E09D951` (`event_performer_id`),
  CONSTRAINT `FK393DE49A5E09D951` FOREIGN KEY (`event_performer_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK393DE49AC8E5AD3B` FOREIGN KEY (`event_attendee_id`) REFERENCES `event` (`id`),
  CONSTRAINT `FK393DE49AD5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `additional_type` varchar(250) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  `contact_point_id` bigint(20) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `duns` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `fax_number` varchar(250) DEFAULT NULL,
  `founding_date` datetime DEFAULT NULL,
  `global_location_number` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `interaction_count` varchar(250) DEFAULT NULL,
  `isicv4` varchar(250) DEFAULT NULL,
  `legal_name` varchar(250) NOT NULL,
  `logo` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `same_as` varchar(250) DEFAULT NULL,
  `taxid` varchar(250) DEFAULT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `vatid` varchar(250) DEFAULT NULL,
  `class` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK4644ED3374CC6813` (`contact_point_id`),
  KEY `FK4644ED335459FE15` (`address_id`),
  CONSTRAINT `FK4644ED335459FE15` FOREIGN KEY (`address_id`) REFERENCES `contactpoint` (`id`),
  CONSTRAINT `FK4644ED3374CC6813` FOREIGN KEY (`contact_point_id`) REFERENCES `contactpoint` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `additional_name` varchar(250) NOT NULL,
  `additional_type` varchar(250) DEFAULT NULL,
  `address_id` bigint(20) DEFAULT NULL,
  `alternate_name` varchar(250) DEFAULT NULL,
  `award` varchar(250) DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `death_date` datetime DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `duns` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `family_name` varchar(250) DEFAULT NULL,
  `gender` varchar(250) DEFAULT NULL,
  `given_name` varchar(250) DEFAULT NULL,
  `global_location_number` varchar(250) DEFAULT NULL,
  `home_location` varchar(250) DEFAULT NULL,
  `honorific_prefix` varchar(250) DEFAULT NULL,
  `honorific_suffix` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `interaction_count` varchar(250) DEFAULT NULL,
  `isicv4` varchar(250) DEFAULT NULL,
  `job_title` varchar(250) DEFAULT NULL,
  `member_of_id` bigint(20) DEFAULT NULL,
  `naics` varchar(250) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `nationality` varchar(250) DEFAULT NULL,
  `same_as` varchar(250) DEFAULT NULL,
  `taxid` varchar(250) DEFAULT NULL,
  `telephone` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  `vatid` varchar(250) DEFAULT NULL,
  `work_location` varchar(250) DEFAULT NULL,
  `works_for_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKC4E39B55AE8E7C1B` (`works_for_id`),
  KEY `FKC4E39B555459FE15` (`address_id`),
  KEY `FKC4E39B55C7A67F0B` (`member_of_id`),
  CONSTRAINT `FKC4E39B55C7A67F0B` FOREIGN KEY (`member_of_id`) REFERENCES `organization` (`id`),
  CONSTRAINT `FKC4E39B555459FE15` FOREIGN KEY (`address_id`) REFERENCES `contactpoint` (`id`),
  CONSTRAINT `FKC4E39B55AE8E7C1B` FOREIGN KEY (`works_for_id`) REFERENCES `organization` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_children`
--

DROP TABLE IF EXISTS `person_children`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_children` (
  `person_id` bigint(20) NOT NULL,
  `children__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`children__id`),
  KEY `FK6ACE3A9D5BB2854` (`person_id`),
  KEY `FK6ACE3A9AD8A7589` (`children__id`),
  CONSTRAINT `FK6ACE3A9AD8A7589` FOREIGN KEY (`children__id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK6ACE3A9D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_colleague`
--

DROP TABLE IF EXISTS `person_colleague`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_colleague` (
  `person_id` bigint(20) NOT NULL,
  `colleague__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`colleague__id`),
  KEY `FK44D0E3456607339` (`colleague__id`),
  KEY `FK44D0E345D5BB2854` (`person_id`),
  CONSTRAINT `FK44D0E345D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK44D0E3456607339` FOREIGN KEY (`colleague__id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_educational_organization`
--

DROP TABLE IF EXISTS `person_educational_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_educational_organization` (
  `person_alumni_of_id` bigint(20) DEFAULT NULL,
  `educational_organization_id` bigint(20) DEFAULT NULL,
  KEY `FKD71EDC9704B30BB` (`person_alumni_of_id`),
  KEY `FKD71EDC94FEEC67` (`educational_organization_id`),
  CONSTRAINT `FKD71EDC94FEEC67` FOREIGN KEY (`educational_organization_id`) REFERENCES `organization` (`id`),
  CONSTRAINT `FKD71EDC9704B30BB` FOREIGN KEY (`person_alumni_of_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_follows`
--

DROP TABLE IF EXISTS `person_follows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_follows` (
  `person_id` bigint(20) NOT NULL,
  `follows__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`follows__id`),
  KEY `FK3FAB57F8D5BB2854` (`person_id`),
  KEY `FK3FAB57F831B28EC` (`follows__id`),
  CONSTRAINT `FK3FAB57F831B28EC` FOREIGN KEY (`follows__id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK3FAB57F8D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_knows`
--

DROP TABLE IF EXISTS `person_knows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_knows` (
  `person_id` bigint(20) NOT NULL,
  `knows__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`knows__id`),
  KEY `FKED28EDDEFFA1CE52` (`knows__id`),
  KEY `FKED28EDDED5BB2854` (`person_id`),
  CONSTRAINT `FKED28EDDED5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKED28EDDEFFA1CE52` FOREIGN KEY (`knows__id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_organization`
--

DROP TABLE IF EXISTS `person_organization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_organization` (
  `person_affiliation_id` bigint(20) DEFAULT NULL,
  `organization_id` bigint(20) DEFAULT NULL,
  KEY `FK94988DDD199C0014` (`organization_id`),
  KEY `FK94988DDD57D07503` (`person_affiliation_id`),
  CONSTRAINT `FK94988DDD57D07503` FOREIGN KEY (`person_affiliation_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FK94988DDD199C0014` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_parent`
--

DROP TABLE IF EXISTS `person_parent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_parent` (
  `person_id` bigint(20) NOT NULL,
  `parent__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`parent__id`),
  KEY `FKBFC6F0D4D5BB2854` (`person_id`),
  KEY `FKBFC6F0D4944FD2B4` (`parent__id`),
  CONSTRAINT `FKBFC6F0D4944FD2B4` FOREIGN KEY (`parent__id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKBFC6F0D4D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_related_to`
--

DROP TABLE IF EXISTS `person_related_to`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_related_to` (
  `person_id` bigint(20) NOT NULL,
  `related_to__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`related_to__id`),
  KEY `FKAED7A6392CD6F299` (`related_to__id`),
  KEY `FKAED7A639D5BB2854` (`person_id`),
  CONSTRAINT `FKAED7A639D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKAED7A6392CD6F299` FOREIGN KEY (`related_to__id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_sibling`
--

DROP TABLE IF EXISTS `person_sibling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_sibling` (
  `person_id` bigint(20) NOT NULL,
  `sibling__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`sibling__id`),
  KEY `FKE49243D8C0E924CC` (`sibling__id`),
  KEY `FKE49243D8D5BB2854` (`person_id`),
  CONSTRAINT `FKE49243D8D5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKE49243D8C0E924CC` FOREIGN KEY (`sibling__id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person_spouse`
--

DROP TABLE IF EXISTS `person_spouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_spouse` (
  `person_id` bigint(20) NOT NULL,
  `spouse__id` bigint(20) NOT NULL,
  PRIMARY KEY (`person_id`,`spouse__id`),
  KEY `FKC5B7BABFD5BB2854` (`person_id`),
  KEY `FKC5B7BABF3FA3AF1F` (`spouse__id`),
  CONSTRAINT `FKC5B7BABF3FA3AF1F` FOREIGN KEY (`spouse__id`) REFERENCES `person` (`id`),
  CONSTRAINT `FKC5B7BABFD5BB2854` FOREIGN KEY (`person_id`) REFERENCES `person` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thing`
--

DROP TABLE IF EXISTS `thing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thing` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL,
  `additional_type` varchar(250) DEFAULT NULL,
  `alternate_name` varchar(250) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `same_as` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-10 15:40:15
